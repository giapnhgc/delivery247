import 'package:delivery247/components/loading.dart';
import 'package:delivery247/models/UserModel.dart';
import 'package:delivery247/models/cart_info.dart';
import 'package:delivery247/models/user.dart';
import 'package:delivery247/services/constant.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:delivery247/services/share_preferent_module.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class AuthenticationService extends GetxService
    with SingleGetTickerProviderMixin {
  GetXHttpProvider httpProvider = Get.find();

  @override
  onInit() {
    super.onInit();
    // httpProvider = new GetXHttpProvider();
  }

  Future<User> signIn(String phone, String password) async {
    showLoadingX(this);
    // var fcmToken = await FirebaseMessaging.instance.getToken();
    var _body = {
      "Phone": phone,
      "Password": password,
    };
    var _res = await httpProvider.login(_body);
    if (_res != null && _res['Code'] == '00') {
      Provider.of<UserModel>(Get.overlayContext, listen: false)
          .updateUserInfo(_res['Result']);
      SPreferentModule().setItem(StorageKey.LOGIN_INFO, _res['Result']);

      User user = User();
      user.setUserData(_res['Result']);

      CartInformation cartInfo = CartInformation();
      cartInfo.setUserIdCart(_res['Result']['Id']);
      return user;
    } else {
      return null;
    }
  }

  Future<void> signOut(String phone) async {
    // var _body = {"user_name": phone};
    // var _res = await httpProvider.logout(_body);
    // if (_res != null) {
    SPreferentModule().clearAll();
    // SqliteModule.instance.dropTable();
    Provider.of<UserModel>(Get.overlayContext, listen: false).removeUserInfo();
    // }
  }
}
