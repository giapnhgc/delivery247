import 'package:get/get.dart';

import 'authentication_service.dart';
import 'authentication_state.dart';

class AuthenticationController extends GetxController {
  final AuthenticationService _authenticationService;
  final _authenticationStateStream = AuthenticationState().obs;

  AuthenticationState get state => _authenticationStateStream.value;

  void setState(data) =>
      _authenticationStateStream.value = Authenticated(user: data);

  AuthenticationController(this._authenticationService);

  @override
  void onInit() {
    super.onInit();
  }

  Future<void> signIn(String phone, String password) async {
    final user = await _authenticationService.signIn(phone, password);
    if (user != null) {
      _authenticationStateStream.value = Authenticated(user: user);
    } else {
      _authenticationStateStream.value = UnAuthenticated();
    }
  }

  Future<void> signOut(String phone) async {
    await _authenticationService.signOut(phone);
    _authenticationStateStream.value = UnAuthenticated();
  }
}
