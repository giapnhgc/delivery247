import 'package:flutter/material.dart';

class UserObjInherited extends InheritedWidget {
  UserObjInherited({Widget child, this.obj}) : super(child: child);

  final dynamic obj;

  @override
  bool updateShouldNotify(UserObjInherited oldWidget) {
    return obj != oldWidget.obj;
  }

  static UserObjInherited of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<UserObjInherited>();
  }
}
