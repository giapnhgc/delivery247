import 'package:flutter/material.dart';
import 'package:delivery247/services/constant.dart';

class UserModel with ChangeNotifier {
  Map userProfile = {};

  void updateUserInfo(Map data) {
    userProfile = convertData(data);
    notifyListeners();
  }

  void upsertUserInfo(Map data) {
    Map newData = convertData(data);
    userProfile = {...userProfile, ...newData};
    notifyListeners();
  }

  Map convertData(Map data) {
    Map convertData = data;
    data.forEach((k, v) {
      if (v is String) {
        String value = v.toLowerCase();
        if (value == NULL_STRING) {
          convertData[k] = null;
        }
      }
    });
    return convertData;
  }

  void removeUserInfo() {
    userProfile = {};
    notifyListeners();
  }
}
