class CartInformation {
  //"AccountUseAppId": 388250855377211392,
  // "PaymentMethod": 0, //0 tm, 1 ck
  // "DeliveryAddressId": 389326145545310208,
  // "DiscountCode": "",
  // "TransportAmount": 0,
  // "DiscountAmount": 0,
  // "DiscountMemberAmount": 0,
  // "TotalAmount": 100000,
  // "Remark": "Ghi chú",
  // "Products": [
  //     {
  //         "ProductId": 384247827502469120,
  //         "ProductName": "Bơ Booth túi 1kg (1-3 trái)",
  //         "Quantity": 2,
  //         "Price": 62000
  //     }
  // ]

  int accountUseAppId = 0;
  int paymentMethod = 0; //0 tm, 1 ck
  int deliveryAddressId = 0;
  String discountCode = '';
  double transportAmount = 0;
  double discountAmount = 0;
  double discountMemberAmount = 0;
  double totalAmount = 0;
  String remark = '';
  List products = [];

  static CartInformation _instance = new CartInformation.internal();

  CartInformation.internal();
  factory CartInformation() => _instance;

  void setUserIdCart(userId) {
    accountUseAppId = userId ?? 0;
  }

  void setCartData(data) {}

  void setCartProduct(data) {
    products.add(data);
    print(products);
  }
}
