import 'package:flutter/material.dart';

class NotificationModel with ChangeNotifier {
  bool haveNotification = false;
  Map payload;

  static NotificationModel _instance = new NotificationModel.internal();

  NotificationModel.internal();
  factory NotificationModel() => _instance;

  void setNotification(bool data, {Map payloadData}) {
    haveNotification = data;
    payload = payloadData;
    notifyListeners();
  }
}
