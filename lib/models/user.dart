class User {
  String userName = '';
  String fullName = '';
  String fullAddress = '';
  String phoneNumber = '';
  String receName = '';
  int receId = 0;
  int id = 0;
  String token = '';

  static User _instance = new User.internal();

  User.internal();
  factory User() => _instance;

  void setUserData(data) {
    userName = data['UserName'] ?? '';
    fullName = data['FullName'] ?? '';
    fullAddress = data['FullAddress'] ?? '';
    phoneNumber = data['PhoneNumber'] ?? '';
    receName = data['ReceName'] ?? '';
    receId = data['ReceId'] ?? 0;
    id = data['Id'] ?? 0;
    token = data['Token'] ?? '';
  }
}
