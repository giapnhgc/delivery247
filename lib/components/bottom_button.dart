import 'package:delivery247/components/text_custom.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';

import '_text_custom.dart';

class BottomButton extends StatelessWidget {
  final Function onPress;
  final String title;
  final bool disable;

  final bool secondButton;
  final Function secondButtonOnPress;
  final String secondButtonTitle;
  final Color secondButtonBgColor;
  final Color secondButtonTextColor;
  BottomButton({
    @required this.onPress,
    @required this.title,
    this.disable = false,
    this.secondButton = false,
    this.secondButtonOnPress,
    this.secondButtonTitle,
    this.secondButtonBgColor,
    this.secondButtonTextColor,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: 64,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.16),
              blurRadius: 4,
              offset: Offset(0, -2),
            ),
          ],
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 8,
        ),
        child: Row(
          children: [
            secondButton
                ? Expanded(
                    child: Container(
                      height: 48,
                      child: TextButton(
                        style: AppTheme.buttonPrimary.copyWith(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              AppTheme.colorRedWhite),
                        ),
                        onPressed: secondButtonOnPress,
                        child: MyText(
                          secondButtonTitle,
                          style: AppTheme.textStyle16.medium().primary(),
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
            SizedBox(
              width: secondButton ? 15 : 0,
            ),
            Expanded(
              child: Container(
                height: 48,
                child: TextButton(
                  style: AppTheme.buttonPrimary.copyWith(
                    backgroundColor: MaterialStateProperty.all<Color>(disable
                        ? AppTheme.colorDisable
                        : AppTheme.colorPrimary),
                  ),
                  child: TextCustom(
                    title: title,
                    style: AppTheme.textStyle16.medium().copyWith(
                        color: disable
                            ? AppTheme.colorBorder
                            : AppTheme.colorWhite),
                  ),
                  onPressed: onPress,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
