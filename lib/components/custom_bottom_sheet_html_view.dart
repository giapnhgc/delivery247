import 'package:delivery247/components/text_custom.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_svg/flutter_svg.dart';

// ignore: must_be_immutable
class CustomBottomSheetHtmlView extends StatelessWidget {
  final String label;
  final String html;

  final double maxHeight;
  final bool isShowBtn;
  CustomBottomSheetHtmlView({
    @required this.label,
    @required this.html,
    this.maxHeight = 360,
    this.isShowBtn = true,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: maxHeight,
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
        ),
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              Container(
                height: 52,
                width: double.infinity,
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 1,
                      color: AppTheme.colorDisable,
                    ),
                  ),
                ),
                alignment: Alignment.center,
                child: Container(
                  // padding: EdgeInsets.only(top: 0),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 24,
                      ),
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          child: TextCustom(
                            title: label,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: AppTheme.colorText,
                          ),
                        ),
                      ),
                      InkWell(
                        child: Container(
                          width: 32,
                          height: 32,
                          margin: EdgeInsets.only(right: 12),
                          padding: EdgeInsets.all(10),
                          child: SvgPicture.asset(
                            'assets/svgs/icon_close.svg',
                            fit: BoxFit.contain,
                            color: AppTheme.darkText,
                            height: 12,
                            width: 12,
                          ),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    color: AppTheme.white,
                    child: Html(data: html),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
