import 'package:delivery247/components/loading/loading_state.dart';
import 'package:get/get.dart';

class LoadingController extends GetxController {
  final _loadingStateStream = LoadingState().obs;

  LoadingState get state => _loadingStateStream.value;

  void setState(data) => _loadingStateStream.value = UnLoading();

  Future<void> showLoading() async {
    _loadingStateStream.value = Loading();
  }

  Future<void> hideLoading() async {
    _loadingStateStream.value = UnLoading();
  }
}
