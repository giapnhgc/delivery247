import 'package:delivery247/components/text_custom.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HeaderOnlyUI extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Widget wTitle;
  final bool titleCenter;
  final Function clickBack;
  final Widget actionWidget;
  final bool noBack;
  final Color backgroundColor;
  final bool showBorderBottom;
  final Color contentColor;
  final Brightness brightness;
  HeaderOnlyUI(
      {this.title,
      this.wTitle,
      this.titleCenter = true,
      this.clickBack,
      this.actionWidget,
      this.backgroundColor,
      this.showBorderBottom = true,
      this.contentColor,
      this.brightness = Brightness.dark,
      this.noBack = true});

  @override
  Size get preferredSize => Size.fromHeight(60);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      brightness: brightness,
      backgroundColor: backgroundColor ?? Colors.white,
      flexibleSpace: backgroundColor != null
          ? SizedBox()
          : Container(color: AppTheme.colorYellow),
      centerTitle: titleCenter,
      titleSpacing: 0,
      elevation: 0,
      bottom: PreferredSize(
        child: showBorderBottom
            ? Container(
                color: AppTheme.borderLineColor,
                height: 1.0,
              )
            : SizedBox(),
        preferredSize: Size.fromHeight(1.0),
      ),
      title: wTitle ??
          TextCustom(
            title: title,
            style: contentColor != null
                ? AppTheme.textStyle16
                    .white()
                    .merge(TextStyle(color: contentColor))
                : AppTheme.textStyle16.white(),
            textOverflow: TextOverflow.ellipsis,
          ),
      leading: noBack
          ? Container(
              // ignore: deprecated_member_use
              child: FlatButton(
                child: SvgPicture.asset(
                  'assets/svgs/icon_back.svg',
                  color: contentColor ?? Colors.white,
                ),
                onPressed: () => {
                  if (clickBack != null) clickBack() else Navigator.pop(context)
                },
              ),
            )
          : SizedBox(),
      actions: [
        actionWidget != null
            ? Container(
                child: actionWidget,
              )
            : SizedBox(),
      ],
    );
  }
}
