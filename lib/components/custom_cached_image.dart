import 'package:cached_network_image/cached_network_image.dart';
import 'package:delivery247/models/hex_color.dart';
import 'package:flutter/material.dart';

class CustomCachedImage extends StatelessWidget {
  final String imageUrl;
  final double height;
  CustomCachedImage({@required this.imageUrl, @required this.height});
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      height: height,
      fit: BoxFit.cover,
      imageBuilder: (context, imgProvider) {
        return Container(
          alignment: Alignment.center,
          height: double.infinity,
          decoration: BoxDecoration(
            color: HexColor('#e6e6e6'),
            borderRadius: BorderRadius.circular(8),
            image: DecorationImage(
                image: NetworkImage(imageUrl), fit: BoxFit.cover),
          ),
        );
      },
      placeholder: (context, url) {
        return Container(
          alignment: Alignment.center,
          height: height,
          decoration: BoxDecoration(
            color: HexColor('#e6e6e6'),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Container(
            height: 48,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/icon_image.png'),
                  fit: BoxFit.contain),
            ),
          ),
        );
      },
      errorWidget: (context, url, error) {
        return Container(
          alignment: Alignment.center,
          height: double.infinity,
          decoration: BoxDecoration(
            color: HexColor('#e6e6e6'),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Container(
            height: 48,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/icon_image_error.png'),
                  fit: BoxFit.contain),
            ),
          ),
        );
      },
    );
  }
}

class ContainerImageCached extends StatelessWidget {
  final String imageUrl;
  final double height;
  final double width;
  final Widget child;
  final BorderRadiusGeometry borderRadius;
  ContainerImageCached({
    @required this.imageUrl,
    @required this.height,
    this.width = double.infinity,
    this.child,
    this.borderRadius = BorderRadius.zero,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: ClipRRect(
        borderRadius: borderRadius,
        child: CachedNetworkImage(
          imageUrl: imageUrl,
          imageBuilder: (context, imageProvider) => Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
              ),
            ),
            child: child ?? SizedBox(),
          ),
          placeholder: (context, url) {
            return Container(
              height: double.infinity,
              width: double.infinity,
              alignment: Alignment.center,
              color: HexColor('#e6e6e6'),
              child: Image(
                width: 30,
                height: 30,
                color: HexColor('#919191'),
                image: AssetImage('assets/images/icon_image.png'),
              ),
            );
          },
          errorWidget: (context, url, error) {
            return Container(
              height: double.infinity,
              width: double.infinity,
              alignment: Alignment.center,
              color: HexColor('#e6e6e6'),
              child: Image(
                width: 30,
                height: 30,
                color: HexColor('#919191'),
                image: AssetImage('assets/images/icon_image_error.png'),
              ),
            );
          },
        ),
      ),
    );
  }
}
