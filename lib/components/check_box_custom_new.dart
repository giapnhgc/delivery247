import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/mfg_labs_icons.dart';

class MyCheckbox extends StatelessWidget {
  final bool isChecked;
  final double size;
  final double iconSize;
  final Color selectedColor;
  final Color selectedIconColor;
  final Function onChange;

  MyCheckbox(
      {this.isChecked = false,
      this.size,
      this.iconSize,
      this.selectedColor,
      this.selectedIconColor,
      this.onChange});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 20,
      height: 20,
      child: Container(
        // style: AppTheme.buttonNoPadding,
        // onPressed: () {
        //   if (onChange != null) onChange(!isChecked);
        // },
        child: AnimatedContainer(
          duration: Duration(milliseconds: 500),
          curve: Curves.fastLinearToSlowEaseIn,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              border: Border.all(
                color: isChecked ? AppTheme.colorPrimary : AppTheme.colorBorder,
                width: 2.0,
              )),
          width: size ?? 20,
          height: size ?? 20,
          child: isChecked
              ? Icon(
                  MfgLabs.ok,
                  color: selectedIconColor ?? AppTheme.colorPrimary,
                  size: iconSize ?? 16,
                )
              : null,
        ),
      ),
    );
  }
}
