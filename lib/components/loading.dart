import 'package:delivery247/components/loading/loading_controller.dart';
import 'package:delivery247/components/pain_canvas.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'loading/loading_state.dart';

void showLoading(context, ticker, {Function onCallBackContext}) {
  AnimationController rotationController = AnimationController(
    duration: Duration(milliseconds: 1500),
    vsync: ticker,
  )..repeat();
  Animation<double> _animation = CurvedAnimation(
    parent: rotationController,
    curve: Curves.linear,
  );

  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (_loadingContext) {
      if (onCallBackContext != null) onCallBackContext(_loadingContext);
      return Center(
        child: Stack(
          children: [
            RotationTransition(
              turns: _animation,
              child: CustomPaint(
                size: Size(60, 60),
                painter: MyPainter(),
              ),
            ),
            Positioned(
              left: 1,
              top: 1,
              right: 1,
              child: Container(
                height: 58,
                width: 58,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(39),
                  image: DecorationImage(
                    image: AssetImage("assets/images/logo/logo.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            )
          ],
        ),
      );
    },
  ).then((value) {
    rotationController.dispose();
  });
}

void hideLoading(context) {
  Navigator.pop(context, null);
}

void showLoadingX(TickerProvider tickerProvider) async {
  final LoadingController _loadingController = Get.find();

  // await Future.delayed(Duration(milliseconds: 10));
  if (!(_loadingController.state is Loading)) {
    await _loadingController.showLoading();

    AnimationController rotationController = AnimationController(
      duration: Duration(milliseconds: 1500),
      vsync: tickerProvider,
    )..repeat();
    Animation<double> _animation = CurvedAnimation(
      parent: rotationController,
      curve: Curves.linearToEaseOut,
    );
    Get.dialog(
      Center(
        child: Stack(
          children: [
            RotationTransition(
              turns: _animation,
              child: CustomPaint(
                size: Size(60, 60),
                painter: MyPainter(),
              ),
            ),
            Positioned(
              left: 1,
              top: 1,
              right: 1,
              child: Container(
                height: 58,
                width: 58,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(39),
                  image: DecorationImage(
                    image: AssetImage("assets/images/logo/logo.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      barrierDismissible: false,
    );
  }
}

hideLoadingX() async {
  final LoadingController _loadingController = Get.find();
  // await Future.delayed(Duration(milliseconds: 10));

  if (_loadingController.state is Loading) {
    await _loadingController.hideLoading();
    Get.back();
  }
}
