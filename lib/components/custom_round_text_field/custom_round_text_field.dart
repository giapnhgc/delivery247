import 'package:flutter/material.dart';

class CustomRoundTextField extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final TextInputType keyboardType;
  final bool obscureText;
  final FocusNode focusNode;
  final TextInputAction textInputAction;

  CustomRoundTextField({
    this.controller,
    this.hintText,
    this.prefixIcon,
    this.suffixIcon,
    this.keyboardType,
    this.focusNode,
    this.obscureText = false,
    this.textInputAction: TextInputAction.done,
  });

  @override
  Widget build(BuildContext context) {
    final mqData = MediaQuery.of(context);
    final mqDataNew = mqData.copyWith(textScaleFactor: 1);
    return MediaQuery(
      data: mqDataNew,
      child: TextFormField(
        controller: controller,
        decoration: new InputDecoration(
          hintText: hintText,
          fillColor: Colors.white,
          prefixIcon: prefixIcon,
          suffixIcon: suffixIcon,
          border: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(8),
            // borderSide: new BorderSide(),
            borderSide: const BorderSide(color: Colors.yellow, width: 2.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: const BorderSide(color: Colors.yellow, width: 2.0),
          ),
          contentPadding: EdgeInsets.symmetric(vertical: 16, horizontal: 12),
        ),
        keyboardType: keyboardType,
        obscureText: obscureText,
        focusNode: focusNode,
        textInputAction: textInputAction,
      ),
    );
  }
}
