import 'package:delivery247/components/text_custom.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomConfirmDialog extends StatelessWidget {
  final Function onTapYes;
  final Function onTapNo;
  final String confirmText;
  final String declineText;
  final double width;
  final bool barrierDismissible;
  final bool showNoButton;
  final Widget content;
  final double alertHeight;
  final double alertPaddingTop;
  final String imageUrl;
  final bool isSvg;

  CustomConfirmDialog({
    @required this.content,
    @required this.onTapYes,
    this.onTapNo,
    this.confirmText,
    this.declineText,
    this.width,
    this.barrierDismissible = true,
    this.showNoButton = true,
    this.alertHeight,
    this.alertPaddingTop,
    this.imageUrl,
    this.isSvg = false,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext dialogContext) {
    bool isImageUrl = false;
    if (!checkNullOrUndifine([imageUrl])) {
      isImageUrl = false;
    } else {
      isImageUrl = true;
    }
    return Stack(
      clipBehavior: Clip.none,
      alignment: Alignment.topCenter,
      children: [
        Container(
          padding: EdgeInsets.only(top: alertPaddingTop ?? 30),
          height: alertHeight ?? 330,
          child: WillPopScope(
            onWillPop: () async => false,
            child: Scaffold(
              backgroundColor: Colors.transparent,
              body: Center(
                child: SingleChildScrollView(
                  child: Container(
                    width: width ?? 296,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.all(16),
                    child: Column(
                      children: [
                        content,
                        SizedBox(
                          height: 16,
                        ),
                        Container(
                          height: 48,
                          child: TextButton(
                              style: AppTheme.textButtonPrimary,
                              child: TextCustom(
                                title: confirmText ?? 'Có',
                                style: AppTheme.textBtnLight,
                              ),
                              onPressed: () {
                                Navigator.pop(dialogContext);
                                if (onTapYes != null) onTapYes();
                              }),
                          width: double.infinity,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        showNoButton
                            ? Container(
                                height: 48,
                                child: TextButton(
                                  style: AppTheme.textButtonSecondary,
                                  child: TextCustom(
                                    title: declineText ?? 'Không',
                                    style: AppTheme.textBtnYellow,
                                  ),
                                  onPressed: () {
                                    Navigator.pop(dialogContext);
                                    if (onTapNo != null) onTapNo();
                                  },
                                ),
                                width: double.infinity,
                              )
                            : SizedBox()
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          top: -40,
          child: CircleAvatar(
            backgroundColor: Colors.white,
            radius: 50,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50.0),
              child: isImageUrl
                  ? (isSvg
                      ? SvgPicture.asset(
                          imageUrl,
                          height: 75,
                          width: 75,
                        )
                      : Image(
                          image: AssetImage(imageUrl),
                          fit: BoxFit.contain,
                          height: 75,
                          width: 75,
                        ))
                  : Image(
                      image: AssetImage('assets/images/logo/logo.png'),
                      fit: BoxFit.contain,
                      height: 75,
                      width: 75,
                    ),
            ),
          ),
        ),
      ],
    );
  }
}
