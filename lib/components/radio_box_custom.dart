import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';

class CustomRadiobox extends StatelessWidget {
  final bool isChecked;
  final double size;
  final double iconSize;
  final Color selectedColor;
  final Color selectedIconColor;

  CustomRadiobox({
    this.isChecked = false,
    this.size,
    this.iconSize,
    this.selectedColor,
    this.selectedIconColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AnimatedContainer(
        duration: Duration(milliseconds: 500),
        curve: Curves.fastLinearToSlowEaseIn,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: isChecked
                ? Border.all(
                    color: AppTheme.colorPrimary,
                    width: 2.0,
                  )
                : Border.all(
                    color: AppTheme.colorBorder,
                    width: 2.0,
                  )),
        width: size ?? 20,
        height: size ?? 20,
        child: isChecked
            ? Container(
                margin: EdgeInsets.all(3),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: AppTheme.colorPrimary,
                ),
              )
            : null,
      ),
    );
  }
}
