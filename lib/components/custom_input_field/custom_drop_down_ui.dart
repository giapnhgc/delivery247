import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:rxdart/subjects.dart';

import '../m_input_border.dart';

class DropDownCtrl extends TextEditingController {
  final String placeholder;
  final List listDataInit;
  List listData;
  dynamic itemData;
  dynamic itemValue;

  set setListData(_listData) {
    listData = _listData;
    setValue = itemValue;
  }

  //value của item được chọn trong list
  set setValue(_valueData) {
    if (_valueData != null && _valueData != '') {
      var _listDataInit = [];
      if (listDataInit != null) _listDataInit = listDataInit;
      if (listData == null) listData = _listDataInit;

      itemData = listData.firstWhere(
          (element) => element['value'] == _valueData,
          orElse: () => null);
      itemValue = _valueData;
      if (itemData != null) {
        text = itemData['title'];
      } else {
        text = _valueData;
        listData.add({'value': _valueData, 'title': _valueData});
      }
    } else {
      itemData = null;
      itemValue = null;
    }
  }

  set setValue2(_valueData) {
    if (_valueData != null && _valueData != '') {
      text = _valueData;
      listData.add({'value': _valueData, 'title': _valueData});
      itemData = listData.firstWhere(
          (element) => element['value'] == _valueData,
          orElse: () => null);
      itemValue = _valueData;
    } else {
      itemData = null;
      itemValue = null;
    }
  }

  set setValueByTitle(_title) {
    if (_title != null && _title != '') {
      var _listDataInit = [];
      if (listDataInit != null) _listDataInit = listDataInit;
      if (listData == null) listData = _listDataInit;

      itemData = listData.firstWhere((element) => element['title'] == _title,
          orElse: () => null);

      if (itemData != null) {
        itemValue = itemData['value'];
        text = itemData['title'];
      } else {
        text = _title;
        listData.add({'value': _title, 'title': _title});
      }
    } else {
      text = '';
      itemData = null;
      itemValue = null;
    }
  }

  DropDownCtrl({
    this.placeholder,
    this.listDataInit,
  });
}

class CustomDropdownComponent extends StatefulWidget {
  final DropDownCtrl controller;
  final bool disable;
  final bool validate;
  final bool isValidate;
  final Function onChange;
  final Function onDismiss;
  final AutovalidateMode autovalidateMode;
  final bool showDoneButton;

  CustomDropdownComponent({
    this.controller,
    this.disable = false,
    this.validate = false,
    this.isValidate,
    this.onChange,
    this.onDismiss,
    this.autovalidateMode,
    this.showDoneButton = false,
  });
  @override
  _CustomDropdownComponentState createState() =>
      _CustomDropdownComponentState();
}

class _CustomDropdownComponentState extends State<CustomDropdownComponent> {
  final pLoadList = PublishSubject<dynamic>();
  Stream<dynamic> get sLoadList => pLoadList.stream;

  final pStateTextField = PublishSubject<dynamic>();
  Stream<dynamic> get sStateTextField => pStateTextField.stream;

  DropDownCtrl textCtrl;

  bool isFocus = false;

  TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    textCtrl = widget.controller != null ? widget.controller : DropDownCtrl();
  }

  @override
  void dispose() {
    pLoadList.close();
    pStateTextField.close();
    searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String searchKey = '';
    bool showSearch = false;

    int _itemOfList = 0;
    if (widget.controller.listData == null)
      widget.controller.setListData = widget.controller.listDataInit;
    else
      _itemOfList = widget.controller.listData.length;

    double _height = (_itemOfList.toDouble() * 52) + 50;
    if (_height > (MediaQuery.of(context).size.height - 60) ||
        widget.showDoneButton) {
      _height = MediaQuery.of(context).size.height - 60;
      showSearch = true;
    }

    return Container(
      child: Stack(
        children: [
          StreamBuilder(
              stream: sStateTextField,
              builder: (context, AsyncSnapshot<dynamic> snapshot) {
                return TextFormField(
                  controller: textCtrl,
                  autovalidateMode: widget.validate
                      ? AutovalidateMode.onUserInteraction
                      : AutovalidateMode.disabled,
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 16, horizontal: 12),
                    suffixIcon: Container(
                      padding: EdgeInsets.all(17),
                      child: SvgPicture.asset(
                        'assets/svgs/icon_drop_down.svg',
                        color: AppTheme.colorBorder,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide:
                          BorderSide(color: AppTheme.colorBlue, width: 2.0),
                    ),
                    enabledBorder: MyInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: isFocus
                          ? BorderSide(color: AppTheme.colorBlue, width: 2.0)
                          : BorderSide(color: AppTheme.borderLineColor),
                    ),
                    errorBorder: MyInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: isFocus
                          ? BorderSide(color: AppTheme.colorBlue, width: 2.0)
                          : BorderSide(color: AppTheme.colorDanger),
                    ),
                    border: MyInputBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                    labelText: textCtrl.placeholder +
                        (widget.validate == true ? ' *' : ''),
                    labelStyle: AppTheme.textStyle16.copyWith(
                        color: isFocus
                            ? AppTheme.colorBlue
                            : AppTheme.colorBorder),
                    fillColor: AppTheme.backgroundColor,
                    filled: widget.disable,
                  ),
                  readOnly: widget.disable,
                  validator: (String value) {
                    if (widget.validate == false) return null;
                    if (value.isEmpty)
                      return textCtrl.placeholder + ' ' + 'khongBoTrong'.tr;
                    else {
                      return null;
                    }
                  },
                );
              }),
          !widget.disable
              ? Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  child: InkWell(
                    child: Container(height: 52),
                    onTap: () {
                      isFocus = true;
                      pStateTextField.sink.add(1);

                      showModalBottomSheet<void>(
                        isScrollControlled: true,
                        isDismissible: true,
                        context: context,
                        builder: (BuildContext barContext) {
                          return Container(
                            height: _height,
                            child: ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(16),
                                topRight: Radius.circular(16),
                              ),
                              child: Container(
                                color: Colors.white,
                                child: Column(
                                  children: [
                                    Container(
                                      height: 50,
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        border: Border(
                                          bottom: BorderSide(
                                            width: 1,
                                            color: AppTheme.colorDisable,
                                          ),
                                        ),
                                      ),
                                      alignment: Alignment.center,
                                      child: Row(
                                        children: [
                                          widget.showDoneButton
                                              ? Container(
                                                  width: 100,
                                                )
                                              : SizedBox(),
                                          Expanded(
                                            child: MyText(
                                              widget.controller.placeholder,
                                              style:
                                                  AppTheme.textStyle16.copyWith(
                                                color: AppTheme.colorText,
                                                fontWeight: FontWeight.w500,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          widget.showDoneButton
                                              ? uiConfirmBtn(barContext)
                                              : SizedBox(),
                                        ],
                                      ),
                                    ),
                                    showSearch
                                        ? Container(
                                            height: 50,
                                            margin: EdgeInsets.symmetric(
                                                vertical: 8, horizontal: 16),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              color: AppTheme.backgroundColor,
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Container(
                                                  width: 50,
                                                  height: 50,
                                                  padding: EdgeInsets.all(16),
                                                  child: SvgPicture.asset(
                                                      'assets/svgs/icon_search.svg'),
                                                ),
                                                Expanded(
                                                  child: TextField(
                                                    controller:
                                                        searchController,
                                                    decoration: InputDecoration(
                                                      filled: true,
                                                      hintText: 'Tìm kiếm',
                                                      fillColor:
                                                          Colors.transparent,
                                                      isDense: true,
                                                      contentPadding:
                                                          const EdgeInsets
                                                                  .symmetric(
                                                              vertical: 0,
                                                              horizontal: 8),
                                                      focusedBorder:
                                                          InputBorder.none,
                                                      enabledBorder:
                                                          InputBorder.none,
                                                    ),
                                                    onChanged: (value) {
                                                      searchKey = value;
                                                      pLoadList.sink.add(1);
                                                    },
                                                  ),
                                                ),
                                                SizedBox(width: 50),
                                              ],
                                            ),
                                          )
                                        : SizedBox(),
                                    Expanded(
                                      child: SingleChildScrollView(
                                        child: StreamBuilder(
                                          stream: sLoadList,
                                          builder: (context,
                                              AsyncSnapshot<dynamic> snapshot) {
                                            var _listDataForSearch = [];
                                            if (widget.controller.listData !=
                                                null)
                                              _listDataForSearch = widget
                                                  .controller.listData
                                                  .where((element) =>
                                                      element['title']
                                                          .toLowerCase()
                                                          .contains(searchKey
                                                              .toLowerCase()))
                                                  .toList();
                                            return Column(
                                              children: List.generate(
                                                  _listDataForSearch.length,
                                                  (index) {
                                                return InkWell(
                                                  child: Container(
                                                    height: 52,
                                                    alignment: Alignment.center,
                                                    child: MyText(
                                                      _listDataForSearch[index]
                                                          ['title'],
                                                      style: AppTheme
                                                          .textStyle16
                                                          .copyWith(
                                                        color: _listDataForSearch[
                                                                        index]
                                                                    ['value'] ==
                                                                widget
                                                                    .controller
                                                                    .value
                                                            ? AppTheme.colorBlue
                                                            : AppTheme
                                                                .colorText,
                                                      ),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                  onTap: () {
                                                    textCtrl.text =
                                                        _listDataForSearch[
                                                            index]['title'];
                                                    widget.controller.setValue =
                                                        _listDataForSearch[
                                                            index]['value'];
                                                    Navigator.pop(barContext);
                                                    if (widget.onChange != null)
                                                      widget.onChange();
                                                  },
                                                );
                                              }),
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ).then((value) {
                        isFocus = false;
                        pStateTextField.sink.add(1);
                        if (widget.onDismiss != null) widget.onDismiss();
                      });
                    },
                  ),
                )
              : SizedBox()
        ],
      ),
    );
  }

  Widget uiConfirmBtn(barContext) {
    return Container(
      width: 100,
      child: TextButton(
        onPressed: () {
          textCtrl.text = searchController.text;
          widget.controller.setValue2 = searchController.text;
          Navigator.pop(barContext);
          if (widget.onChange != null) widget.onChange();
        },
        child: MyText(
          'confirm'.tr,
          style: AppTheme.textStyle16.medium().copyWith1(
                color: AppTheme.colorBlue,
              ),
          fontSize: 16,
        ),
      ),
    );
  }
}
