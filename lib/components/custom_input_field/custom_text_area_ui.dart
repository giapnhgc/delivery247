import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/constant.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../m_input_border.dart';

class TextAreaCtrl extends TextEditingController {
  final ValidateType type;
  final TextInputType keyboardType;
  String placeholder;
  final String labelText;
  final int minLines;
  final int maxLines;

  double get doubleValue {
    return parseCurrency(text);
  }

  set setText(String _text) {
    if (_text != null) text = _text;
  }

  TextAreaCtrl({
    this.type = ValidateType.normal,
    this.keyboardType = TextInputType.multiline,
    this.placeholder,
    this.labelText,
    this.minLines = 5,
    this.maxLines,
  });
}

// ignore: must_be_immutable
class CustomTextAreaComponent extends StatelessWidget {
  final Function onSubmit;
  final Function onChange;
  final bool disable;
  final bool validate;
  TextAreaCtrl controller;
  final List<TextInputFormatter> inputFormater;
  final TextCapitalization capitalization;
  CustomTextAreaComponent({
    this.onSubmit,
    this.onChange,
    this.disable = false,
    this.validate = false,
    this.inputFormater,
    @required this.controller,
    this.capitalization = TextCapitalization.none,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        // if (!checkNullOrUndifine([controller.text])) ...[
        Container(
          margin: EdgeInsets.only(top: 12),
          padding: EdgeInsets.symmetric(vertical: 16, horizontal: 12),
          child: MyText(
            controller.placeholder,
            style: AppTheme.textStyle16.grey(),
          ),
        ),
        // ] else ...[
        //   SizedBox(),
        // ],
        Container(
          padding: EdgeInsets.all(0),
          child: TextFormField(
            textInputAction: TextInputAction.done,
            minLines: controller.minLines,
            maxLines: controller.maxLines,
            textCapitalization: capitalization,
            controller: controller,
            autovalidateMode: validate
                ? AutovalidateMode.onUserInteraction
                : AutovalidateMode.disabled,
            inputFormatters: inputFormater ?? [],
            keyboardType: controller.keyboardType,
            cursorHeight: 14,
            decoration: InputDecoration(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 16, horizontal: 12),
              enabledBorder: MyInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(
                  color: AppTheme.borderLineColor,
                ),
              ),
              focusedBorder: MyInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide:
                    BorderSide(color: AppTheme.colorPrimary, width: 2.0),
              ),
              border: MyInputBorder(
                borderRadius: BorderRadius.circular(8),
              ),
              labelText: controller.labelText + (validate == true ? ' *' : ''),
              labelStyle: AppTheme.textStyle16.grey(),
              fillColor: AppTheme.backgroundColor,
              filled: disable,
              // hintText: controller.placeholder,
              floatingLabelBehavior: FloatingLabelBehavior.always,
            ),
            readOnly: disable,
            onChanged: (String value) {
              if (onChange != null) onChange();
              if (controller.text != '') {
                //Do something to hide placeholder
                controller.placeholder = '';
              }
            },
            onFieldSubmitted: (value) {
              if (onSubmit != null) onSubmit();
            },
            validator: (String value) {
              if (validate == false) return null;
              if (value.isEmpty)
                return controller.labelText + ' ' + 'khongBoTrong'.tr;
              else {
                switch (controller.type) {
                  case ValidateType.email:
                    if (checkValidateEmail(controller.text))
                      return controller.labelText + ' ' + 'khongHopLe'.tr;
                    return null;
                  case ValidateType.phone:
                    if (checkValidatePhone(controller.text))
                      return controller.labelText + ' ' + 'khongHopLe'.tr;
                    return null;
                  case ValidateType.id:
                    if (checkValidateCmt(controller.text))
                      return controller.labelText + ' ' + 'khongHopLe'.tr;
                    return null;
                  case ValidateType.maSoThue:
                    if (checkValidateMst(controller.text))
                      return controller.labelText + ' ' + 'khongHopLe'.tr;
                    return null;
                  default:
                    return null;
                }
              }
            },
          ),
        ),
      ],
    );
  }
}
