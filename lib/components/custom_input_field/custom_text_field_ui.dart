import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/constant.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../m_input_border.dart';

class TextEditingCtrl extends TextEditingController {
  final ValidateType type;
  final TextInputType keyboardType;
  final String placeholder;

  double get doubleValue {
    return parseCurrency(text);
  }

  set setText(String _text) {
    if (_text != null) text = _text;
  }

  TextEditingCtrl({
    this.type = ValidateType.normal,
    this.keyboardType = TextInputType.text,
    this.placeholder,
  });
}

class CustomTextFieldComponent extends StatelessWidget {
  final Function onSubmit;
  final Function onChange;
  final bool disable;
  final bool validate;
  final TextEditingCtrl controller;
  final List<TextInputFormatter> inputFormater;
  final TextCapitalization capitalization;
  CustomTextFieldComponent({
    this.onSubmit,
    this.onChange,
    this.disable = false,
    this.validate = false,
    this.inputFormater,
    @required this.controller,
    this.capitalization = TextCapitalization.none,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0),
      child: TextFormField(
        textCapitalization: capitalization,
        controller: controller,
        autovalidateMode: validate
            ? AutovalidateMode.onUserInteraction
            : AutovalidateMode.disabled,
        inputFormatters: inputFormater ?? [],
        keyboardType: controller.keyboardType,
        cursorHeight: 14,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 16, horizontal: 12),
            enabledBorder: MyInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(color: AppTheme.borderLineColor),
            ),
            focusedBorder: MyInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(color: AppTheme.colorPrimary, width: 2.0),
            ),
            border: MyInputBorder(
              borderRadius: BorderRadius.circular(8),
            ),
            labelText: controller.placeholder + (validate == true ? ' *' : ''),
            labelStyle: AppTheme.textStyle16.grey(),
            fillColor: AppTheme.backgroundColor,
            filled: disable),
        readOnly: disable,
        onChanged: (String value) {
          if (onChange != null) onChange();
        },
        onFieldSubmitted: (value) {
          if (onSubmit != null) onSubmit();
        },
        validator: (String value) {
          if (validate == false) return null;
          if (value.isEmpty)
            return controller.placeholder + ' ' + 'khongBoTrong'.tr;
          else {
            switch (controller.type) {
              case ValidateType.email:
                if (checkValidateEmail(controller.text))
                  return controller.placeholder + ' ' + 'khongHopLe'.tr;
                return null;
              case ValidateType.phone:
                if (checkValidatePhone(controller.text))
                  return controller.placeholder + ' ' + 'khongHopLe'.tr;
                return null;
              case ValidateType.id:
                if (checkValidateCmt(controller.text))
                  return controller.placeholder + ' ' + 'khongHopLe'.tr;
                return null;
              case ValidateType.maSoThue:
                if (checkValidateMst(controller.text))
                  return controller.placeholder + ' ' + 'khongHopLe'.tr;
                return null;
              default:
                return null;
            }
          }
        },
      ),
    );
  }
}
