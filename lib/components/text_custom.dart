import 'package:flutter/material.dart';

//** Component về text và các tuỳ chỉnh */
class TextCustom extends StatelessWidget {
  final String title;
  final Color color;
  final FontWeight fontWeight;
  final FontStyle fontStyle;
  final double fontSize;
  final TextOverflow textOverflow;
  final TextAlign textAlign;
  final int maxLine;
  final TextStyle style;
  TextCustom(
      {this.title,
      this.color = Colors.blueGrey,
      this.fontWeight = FontWeight.normal,
      this.fontSize = 14,
      this.textOverflow,
      this.textAlign = TextAlign.left,
      this.fontStyle,
      this.style,
      this.maxLine});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      textAlign: textAlign,
      style: style != null
          ? style
          : TextStyle(
              color: color,
              fontSize: fontSize,
              fontWeight: fontWeight,
              fontStyle: fontStyle,
              fontFamily: 'SFProDisplay'),
      overflow: textOverflow,
      maxLines: maxLine,
    );
  }
}
