import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';

class CustomScrollWrapper extends StatefulWidget {
  final Widget child;
  const CustomScrollWrapper({Key key, this.child}) : super(key: key);

  @override
  _CustomScrollWrapperState createState() => _CustomScrollWrapperState();
}

class _CustomScrollWrapperState extends State<CustomScrollWrapper> {
  ScrollMetrics metrics;

  bool handleScrollActivity(ScrollNotification notification) {
    final ScrollMetrics _metrics = notification.metrics;
    setState(() {
      metrics = _metrics;
    });
    // print(
    //     'Maximum scrolling distance for scrolling component:${_metrics.maxScrollExtent}');
    // print('Current Scroll Position:${_metrics.pixels}');
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final double _alignmentX = metrics != null
        ? (-1 + (metrics.pixels / metrics.maxScrollExtent) * 2)
        : -1.0;
    return NotificationListener<ScrollNotification>(
      onNotification: handleScrollActivity,
      child: Stack(
        children: <Widget>[
          widget.child,
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              alignment: Alignment.center,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(99),
                child: Container(
                  width: 30,
                  height: 2,
                  color: AppTheme.colorDisable,
                  child: Container(
                    alignment: Alignment(_alignmentX, 1),
                    child: Container(
                      width: 12,
                      color: Colors.yellow,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
