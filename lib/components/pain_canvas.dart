import 'dart:math';

import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//Vẽ đường cung
class MyPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = AppTheme.colorPrimary
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2.0;

    // Method to convert degree to radians
    num degToRad(num deg) => deg * (pi / 180.0);

    Path path = Path();
    // Adds a quarter arc
    path.addArc(Rect.fromLTWH(0, 0, size.width, size.height), degToRad(180),
        degToRad(90));
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter old) {
    return false;
  }
}

class CustomTooltip extends CustomPainter {
  CustomTooltip({this.color});
  final Color color;

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()..color = color ?? Colors.yellow;

    Path paintBubbleTail() {
      Path path;

      path = Path()
        ..moveTo(size.width - 50, 0)
        ..lineTo(size.width - 40, -12)
        ..lineTo(size.width - 30, 0);

      return path;
    }

    final RRect bubbleBody = RRect.fromRectAndRadius(
        Rect.fromLTWH(0, 0, size.width, size.height), Radius.circular(16));
    final Path bubbleTail = paintBubbleTail();

    canvas.drawRRect(bubbleBody, paint);
    canvas.drawPath(bubbleTail, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
