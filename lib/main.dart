import 'dart:convert';

import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/authentication/authentication_service.dart';
import 'package:delivery247/components/loading/loading_controller.dart';
import 'package:delivery247/language/language.dart';
import 'package:delivery247/models/NotificationModel.dart';
import 'package:delivery247/models/UserModel.dart';
import 'package:delivery247/pages/splash/splash_view.dart';
import 'package:delivery247/services/http/getx_http.dart';
// import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  initLocalNotification();
  // await initFirebase();
  initController();

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then(
    (value) => runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => UserModel()),
        ],
        child: HomePage(),
      ),
    ),
  );
}

/// Khởi tạo thông báo local
initLocalNotification() async {
  var initializationSettingsAndroid =
      AndroidInitializationSettings('mipmap/ic_launcher');
  var initializationSettingsIOS = IOSInitializationSettings(
      requestAlertPermission: true,
      requestBadgePermission: true,
      requestSoundPermission: true,
      onDidReceiveLocalNotification:
          (int id, String title, String body, String payload) async {});
  var initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid, iOS: initializationSettingsIOS);

  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String payload) async {
    if (payload != null) {
      handleNotificationTap(jsonDecode(payload));
    }
  });
}

/// Khởi tạo firebase message
// Future initFirebase() async {
//   await Firebase.initializeApp();

//   await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
//     alert: true,
//     badge: true,
//     sound: true,
//   );

//   FirebaseMessaging.instance.getInitialMessage().then((RemoteMessage message) {
//     if (message != null) {
//       handleNotificationTap(message.data);
//     }
//   });

//   FirebaseMessaging.onMessage.listen((RemoteMessage message) {
//     RemoteNotification notification = message.notification;
//     AndroidNotification android = message.notification?.android;

//     var androidPlatformChannelSpecifics = AndroidNotificationDetails(
//       'fcm_notif',
//       'fcm_notif',
//       'Channel for Fcm notification',
//       icon: 'logo',
//       sound: RawResourceAndroidNotificationSound('notification'),
//       largeIcon: DrawableResourceAndroidBitmap('mipmap/ic_launcher'),
//     );

//     var iOSPlatformChannelSpecifics = IOSNotificationDetails(
//         sound: 'notification.wav',
//         presentAlert: true,
//         presentBadge: true,
//         presentSound: true);
//     var platformChannelSpecifics = NotificationDetails(
//         android: androidPlatformChannelSpecifics,
//         iOS: iOSPlatformChannelSpecifics);

//     if (notification != null && android != null) {
//       flutterLocalNotificationsPlugin.show(
//         notification.hashCode,
//         notification.title,
//         notification.body,
//         platformChannelSpecifics,
//       );
//     }
//   });

//   FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
//     handleNotificationTap(message.data);
//   });

//   FirebaseMessaging.instance.subscribeToTopic('247Delivery');
// }

/// Khởi tạo authen controller
void initController() {
  Get.lazyPut(() => GetXHttpProvider());
  Get.lazyPut(() => LoadingController());
  Get.lazyPut(
    () => AuthenticationController(Get.put(AuthenticationService())),
  );
}

/// Xử lý khi tap vào thông báo
handleNotificationTap(Map payload) {
  NotificationModel().setNotification(true, payloadData: payload);
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      builder: (context, child) {
        return MediaQuery(
          child: ScrollConfiguration(
            behavior: MyBehavior(),
            child: child,
          ),
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        );
      },
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [const Locale('vi'), const Locale('en')],
      locale: LocalizationService.locale,
      fallbackLocale: LocalizationService.fallbackLocale,
      translations: LocalizationService(),
      navigatorObservers: [ClearFocusOnPush()],
      debugShowCheckedModeBanner: false,
      home: SplashView(),
      theme: ThemeData(
          fontFamily: 'SFProDisplay',
          bottomSheetTheme: BottomSheetThemeData(
              backgroundColor: Colors.black.withOpacity(0.001))),
    );
  }
}

/// disable hiệu ứng scroll của android và scroll quá của ios
class MyBehavior extends ScrollBehavior {
  //ios
  @override
  ScrollPhysics getScrollPhysics(BuildContext context) =>
      ClampingScrollPhysics();
  //android
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

//Ẩn bàn phím khi chuyển page
class ClearFocusOnPush extends NavigatorObserver {
  @override
  void didPush(Route route, Route previousRoute) {
    super.didPush(route, previousRoute);
    final focus = FocusManager.instance.primaryFocus;
    focus?.unfocus();
  }
}
