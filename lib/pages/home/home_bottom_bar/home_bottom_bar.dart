import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/authentication/authentication_state.dart';
import 'package:delivery247/models/user.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyTabController extends GetxController with SingleGetTickerProviderMixin {
  final AuthenticationController authenticationController = Get.find();
  User user;
  GetXHttpProvider httpProvider = Get.find();

  final myTab = [
    {'icon': 'assets/svgs/home.svg', 'name': 'Trang chủ'},
    {'icon': 'assets/svgs/chat.svg', 'name': 'Trợ giúp'},
    {'icon': 'assets/svgs/notification.svg', 'name': 'Thông báo'},
    {'icon': 'assets/svgs/user.svg', 'name': 'Tài khoản'},
  ];

  int tabIndex = 0;
  int previousTabIndex = 0;

  @override
  void onInit() {
    super.onInit();

    if (authenticationController.state is Authenticated) {
      user = (authenticationController.state as Authenticated).user;
    } else {
      debugPrint(
          'Not Authenticated: ' + authenticationController.state.toString());
    }
  }

  @override
  void onClose() {
    super.onClose();
  }

  void onTabClicked(index) async {
    previousTabIndex = tabIndex;
    tabIndex = index;
    update();

    // if (index == 1) {
    //   var _body = {
    //     "id": "",
    //     "pm": "",
    //     "dtac": "",
    //     "sdt": user.phone,
    //     "ma_tv": "",
    //     "trang_thai": ""
    //   };
    //   await httpProvider.readNotification(_body);
    //   user.setNotificationCount(0);
    //   update();
    // }
  }

  tabToHome() {
    tabIndex = 0;
    update();
  }
}
