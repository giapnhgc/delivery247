import 'package:badges/badges.dart';
import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/authentication/authentication_state.dart';
import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/pages/authenticate/login/login_view.dart';
import 'package:delivery247/pages/home/home_content/home_content_view.dart';
import 'package:delivery247/pages/home/home_notification/home_notification_view.dart';
import 'package:delivery247/pages/home/tab_personal/tab_personal_view.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import 'home_bottom_bar.dart';

class MyTabView extends GetView<MyTabController> {
  final AuthenticationController _authenticationController = Get.find();

  final MyTabController c = Get.put(MyTabController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: mBody(context),
      bottomNavigationBar: mTab(),
    );
  }

  Widget mBody(context) {
    return Container(
      // padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: GetBuilder<MyTabController>(builder: (_) {
        Widget _bodyContent = HomeContentView();
        if (c.tabIndex == 0) {
          _bodyContent = HomeContentView();
        } else if (c.tabIndex == 1) {
          _bodyContent = HomeNotificationView();
        } else if (c.tabIndex == 2) {
          _bodyContent = HomeNotificationView();
        } else if (c.tabIndex == 3) {
          if (_authenticationController.state is Authenticated) {
            _bodyContent = TabPersonalView();
          } else {
            Future.delayed(Duration(milliseconds: 100), () async {
              // await Get.dialog(LoginView());
              await Get.off(() => LoginView(), duration: Duration(seconds: 0));
              if (_authenticationController.state is Authenticated) {
                c.onTabClicked(3);
              } else
                c.onTabClicked(c.previousTabIndex);
            });
          }
        }

        return _bodyContent;
      }),
    );
  }

  Widget previousContent(index) {
    if (index == 1)
      return HomeNotificationView();
    else if (index == 3)
      return TabPersonalView();
    else
      return HomeContentView();
  }

  Widget mTab() {
    return Container(
      decoration: BoxDecoration(
        boxShadow: AppTheme.boxShadow,
        color: AppTheme.colorWhite,
      ),
      child: GetBuilder<MyTabController>(
        builder: (_) => SafeArea(
          child: Container(
            height: 80,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: List.generate(c.myTab.length, (index) {
                return Expanded(
                  child: TextButton(
                    onPressed: () => c.onTabClicked(index),
                    child: Column(
                      children: [
                        SizedBox(height: 12),
                        Badge(
                          badgeColor: AppTheme.colorRed,
                          showBadge: false,
                          // showBadge: index == 1 && c.user.notificationCount > 0,
                          badgeContent: Container(
                            height: 12,
                            width: 12,
                            alignment: Alignment.center,
                            child: MyText(
                              '0',
                              // c.user.notificationCount.toString(),
                              style: TextStyle(
                                fontSize: 10,
                                color: AppTheme.colorWhite,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          child: SvgPicture.asset(
                            c.myTab[index]['icon'],
                            color: index == c.tabIndex
                                ? AppTheme.colorPrimary
                                : AppTheme.colorText,
                          ),
                        ),
                        SizedBox(height: 4),
                        MyText(
                          c.myTab[index]['name'],
                          style: TextStyle(
                            fontSize: AppTheme.fontSize12,
                            fontWeight: FontWeight.w400,
                            color: index == c.tabIndex
                                ? AppTheme.colorPrimary
                                : AppTheme.colorText,
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }),
            ),
          ),
        ),
      ),
    );
  }
}
