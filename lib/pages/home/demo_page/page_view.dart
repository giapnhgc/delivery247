import 'package:delivery247/components/bottom_button.dart';
import 'package:delivery247/components/header/header.dart';
import 'package:delivery247/components/text_custom.dart';
import 'package:delivery247/pages/home/demo_page/page.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DemoView extends GetView<DemoController> {
  final DemoController c = Get.put(DemoController());
  final Map data;
  DemoView({this.data});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HeaderOnlyUI(
        clickBack: () {
          Get.back();
        },
        title: 'DemoPageTitle',
      ),
      backgroundColor: AppTheme.backgroundColor,
      body: mBody(),
      bottomNavigationBar: BottomButton(
        title: 'Tiếp tục',
        onPress: () => {},
      ),
    );
  }

  Widget mBody() {
    return SingleChildScrollView(
      child: GetBuilder<DemoController>(
        builder: (_) => Column(
          children: [
            SizedBox(height: 4),
          ],
        ),
      ),
    );
  }

  Widget rowData({title, content}) {
    return Container(
      padding: EdgeInsets.only(top: 6),
      child: Row(
        children: [
          Expanded(
            child: TextCustom(
              title: title,
              fontSize: 16,
              color: AppTheme.colorBorder,
            ),
          ),
          TextCustom(
            title: content,
            fontSize: 16,
            color: AppTheme.colorBorder,
          )
        ],
      ),
    );
  }
}
