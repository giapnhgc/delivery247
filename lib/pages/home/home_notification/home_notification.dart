import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/components/loading.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeNotificationController extends GetxController
    with SingleGetTickerProviderMixin {
  GetXHttpProvider httpProvider = Get.find();
  final AuthenticationController _authenticationController = Get.find();

  var listTab = [
    'Tất cả',
    'Khuyến mại',
    'Tin tức',
  ];

  final List tiles = [
    {"title": "Tất cả", "type": "ALL"},
    {'title': 'Bảo hiểm', 'type': 'KM'},
    {'title': 'Tin tức', 'type': 'NEW'}
  ];

  RxInt tabIndex = 0.obs;

  bool isLoading = false;

  ScrollController scrollControllerAll = new ScrollController();

  int itemNoti = 0;
  @override
  void onInit() {
    super.onInit();

    loadList(tiles[itemNoti]);

    scrollControllerAll.addListener(() {
      if (scrollControllerAll.position.maxScrollExtent ==
          scrollControllerAll.offset) {
        loadList(tiles[itemNoti]);
      }
    });
  }

  onClickTabItem(index) {
    tabIndex.value = index;

    page = 1;
    hasMore = true;
    loadList(tiles[index]);
  }

  dynamic listData = [];
  int page = 1;
  bool hasMore = true;

  String phone = '';
  String type;

  loadList(Map itemTypeNoti) async {
    showLoadingX(this);
    await Future.delayed(Duration(seconds: 3));
    hideLoadingX();

    // if (hasMore) {
    //   page = 1;
    //   String _phone =
    //       (_authenticationController.state as Authenticated).user.phone;

    //   var _body = {
    //     'pm': '',
    //     'sdt': _phone,
    //     'loai_tin': itemTypeNoti['type'],
    //     'page': page
    //   };
    //   var obj = await httpProvider.getListNotifi(_body);

    //   print(jsonEncode(_body));
    //   // for (var i in obj['response_data']['Table']) {
    //   //   print(i['LOAI_TIN']);
    //   // }

    //   if (obj != null &&
    //       obj['response_data'] != null &&
    //       obj['response_data'].length > 0) {
    //     if (page == 1) {
    //       listData = obj['response_data']['Table'];
    //     } else {
    //       listData.addAll(obj['response_data']['Table']);
    //     }
    //     hasMore = obj['response_data']['Table'].length >= 10;
    //     page += 1;
    //   } else {
    //     listData = [];
    //     showSnackBar('Không có dữ liệu');
    //   }
    //   update();
    // }
  }

  refreshTab(i) {
    page = 1;
    hasMore = true;

    loadList(tiles[i]);
  }

  onReadNotify(soID, BuildContext context, ticker) async {
    // var _body = {'id': soID, 'pm': '', 'sdt': phone};
    // var obj = await httpProvider.getListNotifi(_body);
    // if (obj != null &&
    //     obj['resultlist'] != null &&
    //     obj['resultlist'].length > 0) {
    //   for (var item in listData) {
    //     if (item['ID'].toInt() == soID) item['TRANG_THAI'] = 'D';
    //   }
    //   update();
    // }
  }
}
