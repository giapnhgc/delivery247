import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/header/header.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import 'home_notification.dart';

class HomeNotificationView extends GetView<HomeNotificationController> {
  final HomeNotificationController c = Get.put(HomeNotificationController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HeaderOnlyUI(
        title: 'Thông báo',
        noBack: false,
      ),
      // body: _tatCa(),
      body: mBody(),
    );
  }

  Widget mBody() {
    return Container(
      child: Column(
        children: [
          Container(
            width: Get.width,
            decoration: BoxDecoration(border: AppTheme.borderBottomLine),
            child: Obx(() => Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: List.generate(
                    c.listTab.length,
                    (index) => InkWell(
                      child: Container(
                        padding: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              width: 2,
                              color: c.tabIndex.value == index
                                  ? AppTheme.colorPrimary
                                  : Colors.transparent,
                            ),
                          ),
                        ),
                        child: MyText(
                          c.listTab[index],
                          style: AppTheme.textStyle16.copyWith(
                              color: c.tabIndex.value == index
                                  ? AppTheme.colorPrimary
                                  : AppTheme.colorText),
                        ),
                      ),
                      onTap: () => c.onClickTabItem(index),
                    ),
                  ),
                )),
          ),
          Expanded(
            child: Obx(() {
              if (c.tabIndex.value == 0)
                return _tatCa();
              else if (c.tabIndex.value == 1)
                return _khuyenMai();
              else if (c.tabIndex.value == 2)
                return _tinTuc();
              else
                return Container();
            }),
            // ),
          )
        ],
      ),
    );
  }

  Widget _tatCa() {
    c.itemNoti = 0;
    return GetBuilder<HomeNotificationController>(
      builder: (_) => Container(
        child: RefreshIndicator(
          onRefresh: () async => c.refreshTab(0),
          child: ListView.builder(
            controller: c.scrollControllerAll,
            itemCount: c.listData.length,
            itemBuilder: (BuildContext _context, int index) {
              return _item(c.listData[index]);
            },
          ),
        ),
      ),
    );
  }

  Widget _khuyenMai() {
    c.itemNoti = 1;
    return GetBuilder<HomeNotificationController>(
      builder: (_) => Container(
        child: RefreshIndicator(
          onRefresh: () async => c.refreshTab(1),
          child: ListView.builder(
            controller: c.scrollControllerAll,
            itemCount: c.listData.length,
            itemBuilder: (BuildContext _context, int index) {
              return _item(c.listData[index]);
            },
          ),
        ),
      ),
    );
  }

  Widget _tinTuc() {
    c.itemNoti = 2;
    return GetBuilder<HomeNotificationController>(
      builder: (_) => Container(
        child: RefreshIndicator(
          onRefresh: () async => c.refreshTab(2),
          child: ListView.builder(
            controller: c.scrollControllerAll,
            itemCount: c.listData.length,
            itemBuilder: (BuildContext _context, int index) {
              return _item(c.listData[index]);
            },
          ),
        ),
      ),
    );
  }

  Widget _item(item) {
    String _pathSvg = '';
    if (item['LOAI_TIN'] != null && item['LOAI_TIN'] == 'LOAI')
      _pathSvg = '';
    return Container(
      color: AppTheme.colorWhite,
      margin: EdgeInsets.only(bottom: 4),
      padding: EdgeInsets.only(bottom: 12, left: 16, right: 16, top: 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: SvgPicture.asset(
              _pathSvg,
              height: 24,
              width: 24,
            ),
          ),
          SizedBox(width: 8),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MyText(
                  item['TIEU_DE'] ?? '',
                  style: AppTheme.textStyle16.medium(),
                ),
                SizedBox(height: 8),
                MyText(
                  item['NOI_DUNG'] ?? '',
                  style: AppTheme.textStyle16.grey(),
                ),
                SizedBox(height: 8),
                Row(
                  children: [
                    MyText(
                      item['NGAY_GUI'] ?? '',
                      style: AppTheme.textStyle14.grey(),
                    ),
                    SizedBox(width: 12),
                    MyText(
                      item['GIO_GUI'] ?? '',
                      style: AppTheme.textStyle14.grey(),
                    )
                  ],
                )
              ],
            ),
          ),
          SizedBox(width: 16),
        ],
      ),
    );
  }

}
