import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/authentication/authentication_state.dart';
import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/models/user.dart';
import 'package:delivery247/pages/authenticate/login/login_view.dart';
import 'package:delivery247/pages/home/products/product_order_status/order_status_view.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:toast/toast.dart';

enum TypeData { trangThaiDon, matKhauBaoMat, dangXuat }

class Setting {
  String title;
  List<Tile> list;
  Setting({this.title, this.list});
}

class Tile {
  String image;
  String title;
  Function onPress;
  Widget actions;
  Tile({this.image, this.title, this.onPress, this.actions});
}

class TabPersonalController extends GetxController
    with SingleGetTickerProviderMixin {
  final AuthenticationController authenticationController = Get.find();
  User user;

  GetXHttpProvider httpProvider = Get.find();

  RxString tvv = 'Bạn chưa chọn tư vấn viên'.obs;

  RxBool xacThucTaiKhoan = false.obs;

  List<Setting> setting = [];

  RxInt voteRate = 4.obs;

  @override
  void onInit() {
    super.onInit();
    setting = [
      Setting(title: 'Đơn hàng', list: [
        Tile(
          title: 'Trạng thái đơn',
          image: 'assets/svgs/product.svg',
          onPress: () => onClickItem(TypeData.trangThaiDon),
        ),
      ]),
      Setting(title: 'Quản lý tài khoản', list: [
        Tile(
          title: 'Đăng xuất',
          image: 'assets/svgs/icon_logout.svg',
          onPress: () => onClickItem(TypeData.dangXuat),
        ),
      ]),
    ];

    onLoadUserData();
    onLoadData();
  }

  onLoadUserData() {
    if (authenticationController.state is Authenticated) {
      user = (authenticationController.state as Authenticated).user;
      update();
    }
  }

  onLoadData() async {}

  onClickItem(TypeData type) {
    switch (type) {
      case TypeData.trangThaiDon:
        return trangThaiDon();
      case TypeData.matKhauBaoMat:
        return matKhauVaBaoMat();

      case TypeData.dangXuat:
        return logout();

      default:
        return Toast.show('Tính năng sắp ra mắt', Get.context);
    }
  }

  matKhauVaBaoMat() async {}

  trangThaiDon() async {
    Get.to(() => OrderStatusView());
  }

  showUserProfile() {
    // Get.to(() => ThongTinCaNhanView());
  }

  logout() async {
    showYesNoDialog(
      Get.overlayContext,
      child: Container(
        child: Column(
          children: [
            MyText(
              'Đăng xuất',
              style: AppTheme.textStyle16.medium(),
              textAlign: TextAlign.center,
            ),
            // TextCustom(
            //   title: 'Đăng xuất',
            //   style: TextStyle(
            //     fontSize: 18,
            //     color: Colors.black,
            //     fontWeight: FontWeight.w500,
            //   ),
            // ),
            SizedBox(height: 8),
            MyText(
              'Bạn có muốn đăng xuất?',
              style: AppTheme.textStyle16,
              textAlign: TextAlign.center,
            ),

            // TextCustom(
            //   title: 'Bạn có muốn đăng xuất?',
            //   style: AppTheme.textStyle,
            // ),
          ],
        ),
      ),
      onSubmit: (res) async {
        if (res != null) {
          await authenticationController.signOut(user.phoneNumber);
          Get.offAll(() => LoginView());
        }
      },
    );
  }
}
