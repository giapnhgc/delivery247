import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/header/header.dart';
import 'package:delivery247/models/hex_color.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import 'tab_personal.dart';

class TabPersonalView extends GetView<TabPersonalController> {
  final bool isBack;
  TabPersonalView({this.isBack = false});
  final TabPersonalController c = Get.put(TabPersonalController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.backgroundColor,
      appBar: HeaderOnlyUI(
        title: 'Tài khoản',
        noBack: isBack,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                thongTinCaNhan(),
                ...List.generate(
                  c.setting.length,
                  (index) => section(
                    c.setting[index],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget sectionHeader(String title) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      alignment: Alignment.centerLeft,
      child: MyText(
        title,
        style: AppTheme.textStyle16.lightGrey(),
        textAlign: TextAlign.left,
      ),
    );
  }

  Widget thongTinCaNhan() {
    return GetBuilder<TabPersonalController>(
      builder: (_) => Container(
        color: Colors.white,
        child: InkWell(
          onTap: () => c.showUserProfile(),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
            child: Row(
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Container(
                        width: 48,
                        height: 48,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(99),
                        ),
                        child: SvgPicture.asset('assets/svgs/avatar.svg'),
                      ),
                      SizedBox(width: 8),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            MyText(
                              c.user.fullName == ''
                                  ? c.user.phoneNumber
                                  : c.user.fullName,
                              style: AppTheme.textStyle16,
                            ),
                            SizedBox(height: 4),
                            // MyText(
                            //   'Xem chi tiết',
                            //   style: AppTheme.textStyle14.grey(),
                            // ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget itemTile(
      {Function onPress, String title, String image, Widget actionWidget}) {
    return GestureDetector(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        color: Colors.white,
        child: Row(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: AppTheme.tileBorderColor,
                    ),
                  ),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: SvgPicture.asset(
                        image,
                        height: 20,
                        width: 20,
                        color: HexColor('#2E3A5B'),
                      ),
                    ),
                    SizedBox(width: 8),
                    Expanded(
                      child: MyText(
                        title,
                        style: AppTheme.textStyle16,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            actionWidget ?? SizedBox(),
          ],
        ),
      ),
      onTap: () {
        return onPress != null ? onPress() : null;
      },
    );
  }

  Widget section(Setting data) {
    return Column(
      children: [
        sectionHeader(data.title),
        ...List.generate(
          data.list.length,
          (index) => itemTile(
            title: data.list[index].title,
            onPress: data.list[index].onPress,
            image: data.list[index].image,
            actionWidget: data.list[index].actions,
          ),
        ),
      ],
    );
  }
}
