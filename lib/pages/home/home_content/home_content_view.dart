import 'package:badges/badges.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/custom_cached_image.dart';
import 'package:delivery247/components/custom_scroll_wrapper.dart';
import 'package:delivery247/models/hex_color.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/http/api_url.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:delivery247/services/modules/delivery_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:url_launcher/url_launcher.dart';

import 'home_content.dart';

class HomeContentView extends GetView<HomeContentController> {
  final HomeContentController c = Get.put(HomeContentController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: mBody(context),
      floatingActionButton: _buildFAB(),
    );
  }

  Widget _buildFAB() {
    return FloatingActionButton(
      onPressed: () {
        launch('tel://0585669888');
      },
      backgroundColor: AppTheme.colorPrimary,
      child: Icon(Delivery247Icon.phone),
    );
    // return FloatingActionButton.extended(
    //   onPressed: () {
    //     launch('tel://0585669888');
    //   },
    //   label: Text('0585.669.888'),
    //   icon: Icon(Delivery247Icon.phone),
    //   backgroundColor: AppTheme.colorPrimary,
    // );
  }

  Widget mBody(context) {
    double _statusBarHeight = MediaQuery.of(context).padding.top;
    c.loadUser(statusBarHeight: _statusBarHeight);

    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Stack(
        children: [
          Container(
            height: c.headerHeight,
            width: double.infinity,
            color: AppTheme.colorPrimary,
            child: Container(
              padding: EdgeInsets.only(right: Get.width * 0.25),
            ),
          ),
          Positioned(
            top: _statusBarHeight + c.headerScrollHeight,
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              child: NotificationListener<ScrollNotification>(
                // ignore: missing_return
                onNotification: (scrollNotification) {
                  if (scrollNotification is ScrollEndNotification &&
                      scrollNotification.depth == 0) {
                    c.handleScrollEnd();
                  } else if (scrollNotification is ScrollUpdateNotification &&
                      scrollNotification.depth == 0) {
                    c.handleScrollUpdate();
                  }
                },
                child: SingleChildScrollView(
                  controller: c.scrollController,
                  child: Column(
                    children: [
                      // SizedBox(height: 16),
                      _mainMenu(),
                      _productCategory(),
                      _banner(),
                      _productGroupCategory(),
                      _promotion(),
                      // SizedBox(height: 16),
                    ],
                  ),
                ),
              ),
            ),
          ),
          _header(context),
          SizedBox(
            height: 48,
          )
        ],
      ),
    );
  }

  Widget _header(context) {
    double _statusBarHeight = MediaQuery.of(context).padding.top;
    return Container(
      height: c.headerHeight,
      width: double.infinity,
      child: Container(
        padding: EdgeInsets.only(top: _statusBarHeight),
        child: Column(
          children: [
            Container(
              height: c.headerHeight -
                  c.headerMainServiceHeight -
                  _statusBarHeight -
                  40,
              child: Stack(children: [
                Obx(() => Opacity(
                      opacity: c.logoOpacity.value,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [],
                      ),
                    )),
                Obx(() => Positioned(
                      top: 8 + c.heightUserTitle.value,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: 40,
                        padding: EdgeInsets.only(left: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: InkWell(
                                child: Container(
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                          'assets/svgs/avatar.svg'),
                                      SizedBox(width: 8),
                                      Expanded(
                                          child:
                                              //  c.logoOpacity.value == 1
                                              //     ?
                                              Obx(() => MyText(
                                                    c.fullName.value,
                                                    style: AppTheme.textStyle16
                                                        .white(),
                                                    maxLine: 1,
                                                  ))
                                          // : SizedBox(),
                                          )
                                    ],
                                  ),
                                ),
                                onTap: () => c.onClickUser(),
                              ),
                            ),
                            Row(
                              children: [
                                // c.logoOpacity.value == 0
                                //     ? Container(
                                //         child: Opacity(
                                //           opacity: -(c.logoOpacity.value - 1),
                                //           child: Row(
                                //             children: List.generate(
                                //               c.listMainMenu.length,
                                //               (index) => TextButton(
                                //                 style: AppTheme.buttonNoPadding,
                                //                 child: Container(
                                //                   padding: EdgeInsets.symmetric(
                                //                       horizontal: 10),
                                //                   // SvgPicture.asset(
                                //                   child: Image.asset(
                                //                     c.listMainMenu[index]
                                //                         ['icon'],
                                //                     height: 32,
                                //                     width: 32,
                                //                     // color: AppTheme.colorWhite,
                                //                   ),
                                //                 ),
                                //                 onPressed: () =>
                                //                     c.onClickMainItem(
                                //                         c.listMainMenu[index]),
                                //               ),
                                //             ),
                                //           ),
                                //         ),
                                //       )
                                //     : SizedBox(),
                                // c.logoOpacity.value == 1
                                //     ?
                                Row(
                                  children: [
                                    searchProduct(c),
                                    cartView(c),
                                  ],
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ))
              ]),
            ),
            SizedBox(height: c.headerMainServiceHeight),
          ],
        ),
      ),
    );
  }

  Widget searchProduct(c) {
    return InkWell(
      child: Container(
        width: 40,
        height: 40,
        padding: EdgeInsets.only(right: 4, top: 8, bottom: 8),
        child: SvgPicture.asset(
          'assets/svgs/search.svg',
          color: AppTheme.colorWhite,
        ),
      ),
      onTap: () {
        c.goSearchProductView();
      },
    );
  }

  Widget cartView(c) {
    return InkWell(
      child: Container(
        height: 40,
        width: 60,
        child: Container(
          padding: EdgeInsets.only(right: 16, top: 2),
          child: Badge(
            badgeColor: AppTheme.colorRed,
            showBadge: true,
            badgeContent: Container(
              height: 12,
              width: 12,
              alignment: Alignment.center,
              child: Obx(
                () => MyText(
                  c.cartNumber.value <= 9
                      ? c.cartNumber.value.toString()
                      : '9+',
                  style: TextStyle(
                    fontSize: 9,
                    color: AppTheme.colorWhite,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            child: Icon(
              Delivery247Icon.shopping_cart_1,
              color: AppTheme.white,
            ),
          ),
        ),
      ),
      onTap: () => c.goToCartView(),
    );
  }

  Widget _mainMenu() {
    return Container(
      height: c.headerMainServiceHeight,
      width: double.infinity,
      margin: EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        boxShadow: AppTheme.boxShadow,
        color: AppTheme.colorWhite,
      ),
      child: Row(
        children: List.generate(
          c.listMainMenu.length,
          (index) => Expanded(
            child: TextButton(
              style: AppTheme.buttonNoPadding,
              child: Container(
                padding: EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 8),
                    // SvgPicture.asset(
                    Image.asset(c.listMainMenu[index]['icon'],
                        width: 42, height: 42),
                    SizedBox(height: 8),
                    Expanded(
                      child: MyText(
                        c.listMainMenu[index]['name'],
                        style: AppTheme.textStyle14,
                        textAlign: TextAlign.center,
                        maxLine: 2,
                      ),
                    )
                  ],
                ),
              ),
              onPressed: () => c.onClickMainItem(c.listMainMenu[index]),
            ),
          ),
        ),
      ),
    );
  }

  Widget _productCategory() {
    return GetBuilder<HomeContentController>(builder: (_) {
      return Container(
        color: AppTheme.colorWhite,
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(left: 4),
              child: MyText(
                'Danh mục sản phẩm',
                style: AppTheme.textStyle20,
              ),
            ),
            SizedBox(height: 8),
            ResponsiveGridRow(
              children: List.generate(
                c.listProductCategory.length,
                (index) => ResponsiveGridCol(
                  xs: 4,
                  child: InkWell(
                      child: Container(
                        padding: EdgeInsets.all(4),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.all(14),
                              decoration: BoxDecoration(
                                border:
                                    Border.all(color: AppTheme.colorBorder1),
                                borderRadius: BorderRadius.circular(14),
                              ),
                              child: Image.network(
                                url + c.listProductCategory[index]['ImagePath'],
                                width: 40,
                                height: 40,
                              ),
                            ),
                            SizedBox(height: 8),
                            MyText(
                              c.listProductCategory[index]['Name'],
                              style: AppTheme.textStyle14,
                              maxLine: 2,
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                      onTap: () {
                        c.onClickServiceItem(c.listProductCategory[index]);
                      }),
                ),
              ),
            )
          ],
        ),
      );
    });
  }

  Widget _banner() {
    return Container(
      color: AppTheme.colorWhite,
      child: GetBuilder<HomeContentController>(
          builder: (_) => Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 4, bottom: 4),
                    child: CarouselSlider(
                      options: CarouselOptions(
                          disableCenter: true,
                          aspectRatio: 21 / 9,
                          initialPage: 1,
                          viewportFraction: 1,
                          enlargeCenterPage: true,
                          scrollDirection: Axis.horizontal,
                          autoPlay: true,
                          autoPlayInterval: Duration(seconds: 5),
                          onPageChanged: (index, reson) =>
                              c.onIndexCarouselChange(index)),
                      items: List.generate(c.listBanner.length, (index) {
                        return Container(
                          // padding: EdgeInsets.symmetric(horizontal: 16),
                          child: InkWell(
                            child: Image.network(
                              url + c.listBanner[index]['ImagePath'],
                              width: Get.width,
                              height: (Get.width / (21 / 9)) - 32,
                            ),

                            //url banner
                            // ContainerImageCached(
                            // width: Get.width,
                            // height: (Get.width / 2.2) - 32,
                            // borderRadius: BorderRadius.circular(8),
                            //     imageUrl: c.listBanner[index]['URL']),
                            onTap: () =>
                                c.onClickBannerItem(c.listBanner[index]),
                          ),
                        );
                      }),
                      carouselController: c.carouselController,
                    ),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(c.listBanner.length, (index) {
                        return Obx(
                          () => Container(
                            margin: EdgeInsets.all(2),
                            height: 4,
                            width: 4,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: c.indexCarousel.value == index
                                  ? HexColor('#8592AB')
                                  : HexColor('#DBE0E6'),
                            ),
                          ),
                        );
                      }),
                    ),
                  ),
                ],
              )),
    );
  }

  Widget _productGroupCategory() {
    return GetBuilder<HomeContentController>(builder: (_) {
      return Column(
        children: List.generate(c.listGroupCategory.length, (index) {
          return _productGroupItem(index);
        }),
      );
    });
  }

  Widget _productGroupItem(i) {
    return Container(
      height: 350,
      color: AppTheme.colorWhite,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 8),
                child: MyText(
                  c.listGroupCategory[i]['ProductCategoryName'],
                  style: AppTheme.textStyle20,
                ),
              ),
              InkWell(
                  child: Container(
                    padding: EdgeInsets.only(left: 20, top: 8, bottom: 8),
                    child: MyText(
                      'Xem tất cả',
                      style: AppTheme.textStyle14.grey(),
                    ),
                  ),
                  onTap: () {
                    c.onClickServiceItem({
                      'Id': c.listGroupCategory[i]['ProductCategoryId']
                          .toString(),
                      'Name': c.listGroupCategory[i]['ProductCategoryName']
                    });
                  })
            ],
          ),
          SizedBox(height: 12),
          GetBuilder<HomeContentController>(
            builder: (_) => Container(
              height: 289,
              margin: EdgeInsets.only(bottom: 16),
              child: CustomScrollWrapper(
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    List listDataProduct = c.listGroupCategory[i]['Products'];
                    return InkWell(
                      child: Container(
                        width: (Get.width / 2) - 24,
                        margin: EdgeInsets.only(
                          right: index % 2 == 0 ? 8 : 0,
                          left: index % 2 == 1 ? 8 : 0,
                          bottom: 12,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: AppTheme.colorWhite,
                          boxShadow: AppTheme.boxShadow,
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Column(
                            children: [
                              ContainerImageCached(
                                  width: (Get.width / 2) - 24,
                                  height: (Get.width / 2) - 24,
                                  imageUrl:
                                      url + listDataProduct[index]['Avatar']),
                              Container(
                                padding: EdgeInsets.all(12),
                                child: Column(
                                  children: [
                                    MyText(
                                      listDataProduct[index]['Name'],
                                      style: AppTheme.textStyle16,
                                      maxLine: 2,
                                    ),
                                    SizedBox(height: 8),
                                    Row(
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(height: 8),
                                            MyText(
                                              formatCurrency(
                                                  listDataProduct[index]
                                                      ['Price']),
                                              style:
                                                  AppTheme.textStyle12.grey(),
                                            ),
                                          ],
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              InkWell(
                                                child: Container(
                                                  alignment:
                                                      Alignment.centerRight,
                                                  child: Icon(
                                                    Delivery247Icon.cart_plus,
                                                    color:
                                                        AppTheme.colorPrimary,
                                                  ),
                                                ),
                                                onTap: () async {
                                                  //them gio hang
                                                  var res = await addToCart(
                                                      listDataProduct[index]);
                                                  c.cartNumber.value = res;
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      onTap: () {
                        //chi tiet san pham
                        c.productDetails(
                            listDataProduct[index],
                            c.listGroupCategory[i]['ProductCategoryId']
                                .toString());
                      },
                    );
                  },
                  separatorBuilder: (context, index) => SizedBox(
                    width: 8,
                  ),
                  itemCount: c.listGroupCategory[i]['Products'].length,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _promotion() {
    return GetBuilder<HomeContentController>(builder: (_) {
      return Container(
        color: AppTheme.colorWhite,
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 8),
                  child: MyText(
                    'Thông tin khuyến mãi',
                    style: AppTheme.textStyle20,
                  ),
                ),
                InkWell(
                  child: Container(
                    padding: EdgeInsets.only(left: 20, top: 8, bottom: 8),
                    child: MyText(
                      'Tất cả',
                      style: AppTheme.textStyle14.grey(),
                    ),
                  ),
                  onTap: () => c.goToSaleView(),
                )
              ],
            ),
            SizedBox(height: 12),
            GetBuilder<HomeContentController>(
              builder: (_) => Container(
                height: 280,
                margin: EdgeInsets.only(bottom: 16),
                child: CustomScrollWrapper(
                  child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return InkWell(
                        child: Container(
                          width: (Get.width / 2) - 24,
                          margin: EdgeInsets.only(
                            right: index % 2 == 0 ? 8 : 0,
                            left: index % 2 == 1 ? 8 : 0,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: AppTheme.colorWhite,
                            boxShadow: AppTheme.boxShadow,
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Column(
                              children: [
                                ContainerImageCached(
                                    width: (Get.width / 2) - 24,
                                    height: (Get.width / 2) - 24,
                                    imageUrl: url +
                                        c.listPromotion[index]['ImagePath']),
                                Container(
                                  width: 1,
                                  color: AppTheme.colorBorder1,
                                ),
                                Container(
                                  padding: EdgeInsets.all(12),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      MyText(
                                        c.listPromotion[index]['Name'],
                                        style: AppTheme.textStyle16,
                                        maxLine: 2,
                                      ),
                                      SizedBox(height: 8),
                                      MyText(
                                        'Hạn đến ' +
                                            c.listPromotion[index]
                                                ['ExpireDate'],
                                        // 'Hạn đến ${dateConvertFromStringYYYYMMDD(c.listPromotion[index]['time'])}',
                                        style: AppTheme.textStyle12.grey(),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        onTap: () {
                          c.goToSaleDetailView(c.listPromotion[index]);
                        },
                      );
                    },
                    separatorBuilder: (context, index) => SizedBox(
                      width: 8,
                    ),
                    itemCount: c.listPromotion.length,
                  ),
                ),
              ),
            )
          ],
        ),
      );
    });
  }
}
