import 'package:carousel_slider/carousel_controller.dart';
import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/authentication/authentication_state.dart';
import 'package:delivery247/models/user.dart';
import 'package:delivery247/pages/authenticate/login/login_view.dart';
import 'package:delivery247/pages/home/home_notification/home_notification_view.dart';
import 'package:delivery247/pages/home/products/product_details/product_detail_view.dart';
import 'package:delivery247/pages/home/products/product_list/product_list_view.dart';
import 'package:delivery247/pages/home/products/product_search/product_search_view.dart';
import 'package:delivery247/pages/home/products/product_type/product_type_view.dart';
import 'package:delivery247/pages/home/products/products_cart/products_cart_view.dart';
import 'package:delivery247/pages/home/tab_personal/tab_personal_view.dart';
import 'package:delivery247/pages/sales/sales_ui.dart';
import 'package:delivery247/pages/sales_details/sales_details_ui.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeContentController extends GetxController
    with SingleGetTickerProviderMixin {
  final AuthenticationController authenticationController = Get.find();
  User user;
  RxString fullName = ''.obs;

  GetXHttpProvider httpProvider = Get.find();

  var listMainMenu = [
    {
      'index': 1,
      'icon': 'assets/images/icon_san_pham.png',
      'name': 'Sản phẩm',
    },
    {
      'index': 2,
      'icon': 'assets/images/icon_dat_hang.png',
      'name': 'Đặt hàng',
    },
    {
      'index': 3,
      'icon': 'assets/images/icon_uu_dai.png',
      'name': 'Ưu đãi',
    },
    {
      'index': 4,
      'icon': 'assets/images/icon_tin_tuc.png',
      'name': 'Tin tức',
    },
  ];

  var listProductCategory = [];

  //Group Category
  List listGroupCategory = [];

  ScrollController scrollController = ScrollController();

  final CarouselController carouselController = CarouselController();

  var listBanner = [];

  var listPromotion = [];

  RxInt indexCarousel = 0.obs;

  RxDouble logoOpacity = 1.0.obs;
  RxDouble heightUserTitle = 12.0.obs;
  // RxDouble heightPadding = 20.0.obs;

  final double headerHeight = 280.0;
  final double headerScrollHeight = 64.0;
  final double headerMainServiceHeight = 110.0;

  double _statusBarHeight = 0.0;

  RxInt shartCount = 0.obs;
  final AuthenticationController _authenticationController = Get.find();
  RxInt cartNumber = 0.obs;

  @override
  void onInit() {
    super.onInit();

    loadUser();
    // onLoadShart();
    onloadProductCategory();
    onLoadBanner();
    onLoadProductsCategory();
    onLoadPromotion();
    setCartNumber();
  }

  goSearchProductView() async {
    var _res = await Get.to(() => ProductSearchView());
    // if (_res) {}
  }

  goToCartView() async {
    var _res = await Get.to(() => CartView());
    if (_res != null && _res) {
      setCartNumber();
    }
  }

  goToSaleView() async {
    var _res = await Get.to(() => SalesView());
    if (_res != null && _res) {
      setCartNumber();
    }
  }

  goToSaleDetailView(data) async {
    var _res = await Get.to(() => SalesDetailsView(data: data));
    if (_res != null && _res) {
      setCartNumber();
    }
  }

  setCartNumber() async {
    cartNumber.value = await countCartNumber();
    // print(cartNumber);
  }

  onloadProductCategory() async {
    var _res = await callApiProductCategory();
    listProductCategory = _res['data'];
    update();
  }

  Future<dynamic> callApiProductCategory() async {
    var result = await httpProvider.productCategory();
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  onLoadBanner() async {
    var _res = await callApiLoadBanner();
    listBanner = _res['data'];
    update();
  }

  Future<dynamic> callApiLoadBanner() async {
    var result = await httpProvider.bannerList();
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  onLoadPromotion() async {
    var _res = await callApiPromotion();
    listPromotion = _res['data'];
    update();
  }

  Future<dynamic> callApiPromotion() async {
    var result = await httpProvider.promotion(1, 10);
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  onIndexCarouselChange(index) {
    indexCarousel.value = index;
  }

  handleScrollEnd() async {
    double _offset = scrollController.offset;
    if (_offset < 60) {
      await Future.delayed(Duration(milliseconds: 10));
      scrollController.animateTo(0.0,
          duration: Duration(milliseconds: 200), curve: Curves.easeOut);
    } else if (_offset >= 60 && _offset < headerHeight - headerScrollHeight) {
      await Future.delayed(Duration(milliseconds: 10));

      scrollController.animateTo(
          headerHeight - headerScrollHeight - _statusBarHeight - 16,
          duration: Duration(milliseconds: 200),
          curve: Curves.easeOut);
    }
  }

  handleScrollUpdate() async {
    double _offset = scrollController.offset;
    double _scrollSize =
        headerHeight - headerScrollHeight - _statusBarHeight - 16;
    if (_offset < _scrollSize) {
      logoOpacity.value = (_scrollSize - _offset) / _scrollSize;
      if (_offset < 50) {
        heightUserTitle.value = ((50 - _offset) / 50) * 12;
      } else {
        heightUserTitle.value = 0;
      }
    } else {
      logoOpacity.value = 0;
      heightUserTitle.value = 0;
    }
  }

  onClickMainItem(item) async {
    switch (item['index']) {
      case 1:
        var _res = await Get.to(() => DanhMucSanPhamView());
        if (_res) {
          await setCartNumber();
        }
        return _res;
        break;
      case 2:
        return Get.to(() => HomeNotificationView());
        break;
      case 3:
        break;
      // return Get.to(() => Page());
      case 4:
        break;
      // return Get.to(() => Page());
      default:
        break;
    }
  }

  onClickBannerItem(item) async {
    // showLoadingX(this);
    // var _res = await httpProvider.getNewBanner(id: item['ID']);
    // if (_res != null && _res['resultlist']['Table'].length > 0) {
    //   Get.to(() => BannerDetailView(),
    //       arguments: _res['resultlist']['Table'][0]);
    // }
  }

  onClickServiceItem(item) async {
    var _res = await Get.to(() => ProductListView(data: item));
    if (_res != null && _res) {
      setCartNumber();
    }
  }

  onClickUser() async {
    if (user == null) {
      await Get.dialog(LoginView());
      // Get.off(() => LoginView(), duration: Duration(seconds: 0));
    } else {
      await Get.to(() => TabPersonalView(
            isBack: true,
          ));
      fullName.value = user.fullName;
    }
  }

  loadUser({statusBarHeight}) {
    _statusBarHeight = statusBarHeight;

    if (authenticationController.state is Authenticated) {
      user = (authenticationController.state as Authenticated).user;
      if (user.fullName != '')
        fullName.value = user.fullName;
      else
        fullName.value = user.phoneNumber;
    } else {
      user = null;
      fullName.value = 'Đăng nhập';
    }
  }

  onLoadProductsCategory() async {
    var _res = await callApiProductsCategory();
    listGroupCategory = _res['data'];
    update();
  }

  Future<dynamic> callApiProductsCategory() async {
    var result = await httpProvider.productListByGroupCategory();
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  productDetails(Map data, String catId) async {
    var _res = await Get.to(() => ProductDetailsView(data: data, idCat: catId));
    if (_res != null && _res) {
      setCartNumber();
    }
  }
}
