import 'package:badges/badges.dart';
import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/bottom_button.dart';
import 'package:delivery247/components/custom_cached_image.dart';
import 'package:delivery247/components/header/header.dart';
import 'package:delivery247/pages/home/products/product_list/products_list.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/http/api_url.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:delivery247/services/modules/delivery_icons.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:url_launcher/url_launcher.dart';

class ProductListView extends GetView<ProductListController> {
  final Map data;
  ProductListView({this.data});
  @override
  Widget build(BuildContext context) {
    final ProductListController c = Get.put(ProductListController(data));
    return WillPopScope(
      onWillPop: () => c.onClickBack(),
      child: Scaffold(
        appBar: HeaderOnlyUI(
          clickBack: () {
            c.onClickBack();
          },
          title:
              checkNullOrUndifine([data['Name']]) ? data['Name'] : 'Sản phẩm',
          actionWidget: Container(
            padding: EdgeInsets.only(right: 12, top: 2),
            child: cartView(c),
          ),
        ),
        backgroundColor: AppTheme.backgroundColor,
        body: mBody(c),
        bottomNavigationBar: BottomButton(
          title: 'Xem giỏ hàng',
          onPress: () {
            c.goToCartView();
          },
        ),
        floatingActionButton: _buildFAB(),
      ),
    );
  }

  Widget mBody(c) {
    return SingleChildScrollView(
      child: GetBuilder<ProductListController>(
        builder: (_) => Container(
          color: AppTheme.white,
          child: Column(
            children: [
              _productsList(c),
              SizedBox(
                height: 48,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _productsList(c) {
    return Container(
      color: AppTheme.colorWhite,
      padding: EdgeInsets.only(top: 16, left: 16, right: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GetBuilder<ProductListController>(
            builder: (_) {
              if (checkNullOrUndifine([c.listProducts['Products']]) &&
                  c.listProducts['Products'].length > 0) {
                return ResponsiveGridRow(
                  children:
                      List.generate(c.listProducts['Products'].length, (index) {
                    return ResponsiveGridCol(
                      xs: 6,
                      // sm: 4,
                      // md: 3,
                      // lg: 2,
                      child: InkWell(
                        child: Container(
                          width: (Get.width / 2) - 24,
                          margin: EdgeInsets.only(
                            right: index % 2 == 0 ? 8 : 0,
                            left: index % 2 == 1 ? 8 : 0,
                            bottom: 12,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: AppTheme.colorWhite,
                            boxShadow: AppTheme.boxShadow,
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Column(
                              children: [
                                ContainerImageCached(
                                    width: (Get.width / 2) - 24,
                                    height: (Get.width / 2) - 24,
                                    imageUrl: url +
                                        c.listProducts['Products'][index]
                                            ['Avatar']),
                                // Container(
                                //   width: 1,
                                //   color: AppTheme.colorBorder1,
                                // ),
                                Container(
                                  padding: EdgeInsets.all(12),
                                  child: Column(
                                    children: [
                                      MyText(
                                        c.listProducts['Products'][index]
                                            ['Name'],
                                        style: AppTheme.textStyle16,
                                        maxLine: 2,
                                      ),
                                      SizedBox(height: 8),
                                      Row(
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(height: 8),
                                              MyText(
                                                formatCurrency(
                                                    c.listProducts['Products']
                                                        [index]['ImportPrice']),
                                                style:
                                                    AppTheme.textStyle12.grey(),
                                              ),
                                            ],
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                InkWell(
                                                  child: Container(
                                                    alignment:
                                                        Alignment.centerRight,
                                                    child: Icon(
                                                      Delivery247Icon.cart_plus,
                                                      color:
                                                          AppTheme.colorPrimary,
                                                      size: 18,
                                                    ),
                                                  ),
                                                  onTap: () async {
                                                    //them gio hang
                                                    var res = await addToCart(
                                                        c.listProducts[
                                                            'Products'][index]);
                                                    c.cartNumber.value = res;
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        onTap: () {
                          //chi tiet san pham
                          c.productDetails(c.listProducts['Products'][index]);
                        },
                      ),
                    );
                  }),
                );
              } else {
                return SizedBox();
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _buildFAB() {
    return FloatingActionButton(
      onPressed: () {
        launch('tel://0585669888');
      },
      backgroundColor: AppTheme.colorPrimary,
      child: Icon(Delivery247Icon.phone),
    );
    // return FloatingActionButton.extended(
    //   onPressed: () {
    //     launch('tel://0585669888');
    //   },
    //   label: Text('0585.669.888'),
    //   icon: Icon(Delivery247Icon.phone),
    //   backgroundColor: AppTheme.colorPrimary,
    // );
  }

  Widget cartView(c) {
    return InkWell(
      child: Container(
        height: 40,
        width: 60,
        child: Container(
          padding: EdgeInsets.only(right: 12, top: 12),
          child: Badge(
            badgeColor: AppTheme.colorRed,
            showBadge: true,
            badgeContent: Container(
              height: 12,
              width: 12,
              alignment: Alignment.center,
              child: Obx(
                () => MyText(
                  c.cartNumber.value <= 9
                      ? c.cartNumber.value.toString()
                      : '9+',
                  style: TextStyle(
                    fontSize: 9,
                    color: AppTheme.colorWhite,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            child: Icon(
              Delivery247Icon.shopping_cart_1,
              color: AppTheme.white,
            ),
          ),
        ),
      ),
      onTap: () => c.goToCartView(),
    );
  }
}
