import 'package:delivery247/components/loading.dart';
import 'package:delivery247/pages/home/products/product_details/product_detail_view.dart';
import 'package:delivery247/pages/home/products/products_cart/products_cart_view.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:get/get.dart';

class ProductListController extends GetxController
    with SingleGetTickerProviderMixin {
  GetXHttpProvider httpProvider = Get.find();
  final Map data;
  String idCat;
  ProductListController(this.data);

  Map listProducts = {};

  // var listProducts =
  // [
  //   {
  //     "id": 1,
  //     "name": "Rau cải",
  //     "title":
  //         "Khuyến mãi lên đến 50% cho khách hàng mua sản phẩm Tinh tế Beauty",
  //     "discount": 0.0,
  //     "image": "http://via.placeholder.com/150x150",
  //     "price": "100,000 VNĐ"
  //   },
  //   {
  //     "id": 2,
  //     "name": "Táo",
  //     "title":
  //         "Khuyến mãi lên đến 30% cho khách hàng mua sản phẩm Tinh tế Beauty",
  //     "discount": 0.0,
  //     "image": "http://via.placeholder.com/150x150",
  //     "price": "100,000 VNĐ"
  //   },
  //   {
  //     "id": 3,
  //     "name": "Bưởi diễn",
  //     "title":
  //         "Khuyến mãi lên đến 30% cho khách hàng mua sản phẩm Tinh tế Beauty",
  //     "discount": 0.0,
  //     "image": "http://via.placeholder.com/150x150",
  //     "price": "100,000 VNĐ"
  //   },
  //   {
  //     "id": 4,
  //     "name": "Bưởi diễn",
  //     "title":
  //         "Khuyến mãi lên đến 30% cho khách hàng mua sản phẩm Tinh tế Beauty",
  //     "discount": 0.0,
  //     "image": "http://via.placeholder.com/150x150",
  //     "price": "100,000 VNĐ"
  //   },
  //   {
  //     "id": 5,
  //     "name": "Bưởi diễn",
  //     "title":
  //         "Khuyến mãi lên đến 30% cho khách hàng mua sản phẩm Tinh tế Beauty",
  //     "discount": 0.0,
  //     "image": "http://via.placeholder.com/150x150",
  //     "price": "100,000 VNĐ"
  //   },
  //   {
  //     "id": 6,
  //     "name": "Bưởi diễn",
  //     "title":
  //         "Khuyến mãi lên đến 30% cho khách hàng mua sản phẩm Tinh tế Beauty",
  //     "discount": 0.0,
  //     "image": "http://via.placeholder.com/150x150",
  //     "price": "100,000 VNĐ"
  //   },
  // ];

  RxInt cartNumber = 0.obs;

  @override
  void onInit() {
    super.onInit();
    onLoad();
  }

  onLoad() async {
    loadData();
  }

  loadData() async {
    showLoadingX(this);
    var _res = await callApiProductListbyCategory();
    hideLoadingX();
    listProducts = _res['data'];
    idCat = data['Id'];
    setCartNumber();
    update();
  }

  setCartNumber() async {
    cartNumber.value = await countCartNumber();
  }

  Future<dynamic> callApiProductListbyCategory() async {
    var result = await httpProvider.productListByCategory(data['Id'], 1, 10);
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  goToCartView() async {
    var _res = await Get.to(() => CartView());
    if (_res != null && _res) {
      setCartNumber();
    }
  }

  productDetails(Map data) {
    Get.to(() => ProductDetailsView(data: data, idCat: idCat));
  }

  onClickBack() {
    Get.back(result: true);
  }
}
