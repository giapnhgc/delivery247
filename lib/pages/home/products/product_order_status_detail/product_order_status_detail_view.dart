import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/custom_cached_image.dart';
import 'package:delivery247/components/header/header.dart';
import 'package:delivery247/components/text_custom.dart';
import 'package:delivery247/models/hex_color.dart';
import 'package:delivery247/pages/home/products/product_order_status_detail/product_order_status_detail.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/http/api_url.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class ProductOrderStatusDetailView
    extends GetView<ProductOrderStatusDetailController> {
  final ProductOrderStatusDetailController c =
      Get.put(ProductOrderStatusDetailController());
  final Map data;
  ProductOrderStatusDetailView({this.data});
  @override
  Widget build(BuildContext context) {
    c.dataList = data;
    return Scaffold(
      appBar: HeaderOnlyUI(
        clickBack: () {
          Get.back();
        },
        title: 'Chi tiết đơn hàng',
      ),
      backgroundColor: AppTheme.backgroundColor,
      body: mBody(),
      // bottomNavigationBar: BottomButton(
      //   title: 'Tiếp tục',
      //   onPress: () => {},
      // ),
    );
  }

  Widget mBody() {
    return SingleChildScrollView(
      child: GetBuilder<ProductOrderStatusDetailController>(
        builder: (_) => Column(
          children: [
            thongTinChungDonHang(),
            SizedBox(height: 12),
            thongTinDonHang(),
            SizedBox(height: 12),
            theoDoiDonHang(),
            SizedBox(height: 12),
            diaChiGiaoHang(),
            SizedBox(height: 12),
            thanhToan(),
            SizedBox(height: 12),
          ],
        ),
      ),
    );
  }

  Widget thongTinChungDonHang() {
    return Container(
      padding: EdgeInsets.all(16),
      color: Colors.white,
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: HexColor('#DBE0E6'), width: 1),
              ),
            ),
            width: double.infinity,
            padding: EdgeInsets.only(bottom: 8),
            margin: EdgeInsets.only(bottom: 10),
            child: rowDataBold(
              title: 'Mã đơn hàng:',
              content: c.dataList['OrderCode'],
            ),
          ),
          rowData(
            title: 'Ngày đặt hàng:',
            content: c.dataList['CreateDate'],
          ),
          rowData(
            title: 'Trạng thái đơn hàng:',
            content: c.dataList['StatusText'],
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget thongTinDonHang() {
    List listData = c.dataList['Products'];
    return Container(
      padding: EdgeInsets.only(top: 8, right: 16, left: 16, bottom: 8),
      color: Colors.white,
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: HexColor('#DBE0E6'), width: 1),
              ),
            ),
            width: double.infinity,
            padding: EdgeInsets.only(bottom: 8),
            margin: EdgeInsets.only(bottom: 10),
            child: rowDataBold(
              title: 'Thông tin đơn hàng',
              content: '',
            ),
          ),
          Column(
            children: List.generate(listData.length, (index) {
              return _productInItem(index);
            }),
          )
        ],
      ),
    );
  }

  Widget _productInItem(index) {
    List listData = c.dataList['Products'];
    return Container(
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
      color: AppTheme.colorWhite,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: ContainerImageCached(
              width: 60,
              height: 60,
              borderRadius: BorderRadius.circular(0),
              imageUrl: url + listData[index]['ImagePath'] ?? '',
            ),
          ),
          SizedBox(width: 8),
          Expanded(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 12),
                  // decoration: BoxDecoration(border: AppTheme.borderBottomLine),
                  child: Row(
                    children: [
                      Expanded(
                        child: MyText(
                          listData[index]['ProductName'] ?? '',
                          style: AppTheme.textStyle16.medium(),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 8),
                Container(
                  padding: EdgeInsets.only(bottom: 12),
                  child: Row(
                    children: [
                      Container(
                        child: MyText(
                          'Số lượng x' +
                                  listData[index]['Quanlity'].toString() ??
                              '',
                          style: AppTheme.textStyle16.medium(),
                        ),
                      ),
                      SizedBox(width: 12),
                      Expanded(
                          child: Container(
                        alignment: Alignment.centerRight,
                        child: MyText(
                          formatCurrency(listData[index]['Price']) ?? '',
                          style: AppTheme.textStyle16.bold(),
                        ),
                      )),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget theoDoiDonHang() {
    List listData = c.dataList['StatusTimeLine'];
    return Container(
      padding: EdgeInsets.only(top: 8, right: 16, left: 16, bottom: 8),
      color: Colors.white,
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: HexColor('#DBE0E6'), width: 1),
              ),
            ),
            width: double.infinity,
            padding: EdgeInsets.only(bottom: 8),
            margin: EdgeInsets.only(bottom: 10),
            child: rowDataBold(
              title: 'Theo dõi đơn hàng',
              content: '',
            ),
          ),
          Container(
            padding: EdgeInsets.all(0),
            child: Column(
              children: List.generate(listData.length, (index) {
                return Stack(
                  children: [
                    Positioned(
                      top: 0,
                      bottom: 0,
                      left: 14,
                      child: Container(
                        color: AppTheme.colorYellow,
                        width: 2,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 40, bottom: 24),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(99),
                              color: AppTheme.colorYellow,
                            ),
                            child: Center(
                              child: SvgPicture.asset(
                                'assets/svgs/success.svg',
                                width: 30,
                                color: AppTheme.white,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 9,
                          ),
                          Expanded(
                            child: _buildRowQuaTrinh(
                              title: listData[index]['StatusText'],
                              content: listData[index]['StatusDate'],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                );
              }),
            ),
          ),
        ],
      ),
    );
  }

  Widget diaChiGiaoHang() {
    return Container(
      padding: EdgeInsets.all(16),
      color: Colors.white,
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: HexColor('#DBE0E6'), width: 1),
              ),
            ),
            width: double.infinity,
            padding: EdgeInsets.only(bottom: 8),
            margin: EdgeInsets.only(bottom: 10),
            child: rowDataBold(
              title: 'Địa chỉ giao hàng',
              content: '',
            ),
          ),
          rowData(
            title: 'Họ tên:',
            content: c.dataList['DeliveryAddress']['FullName'] ?? '',
          ),
          rowData(
            title: 'Điện thoại:',
            content: c.dataList['DeliveryAddress']['Phone'] ?? '',
          ),
          rowData(
            title: 'Địa chỉ:',
            content: c.dataList['DeliveryAddress']['Address'] ?? '',
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget thanhToan() {
    String paymentMethod = '';
    if (c.dataList['Payment']['PaymentMethod'] == '0') {
      paymentMethod = 'Chưa thanh toán';
    } else if (c.dataList['Payment']['PaymentMethod'] == '1') {
      paymentMethod = 'Đã thanh toán';
    } else if (c.dataList['Payment']['PaymentMethod'] == '2') {
      paymentMethod = 'Hoàn tiền';
    } else {
      paymentMethod = '';
    }
    return Container(
      padding: EdgeInsets.all(16),
      color: Colors.white,
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: HexColor('#DBE0E6'), width: 1),
              ),
            ),
            width: double.infinity,
            padding: EdgeInsets.only(bottom: 8),
            margin: EdgeInsets.only(bottom: 10),
            child: rowDataBold(
              title: 'Thanh toán',
              content: '',
            ),
          ),
          rowData(
            title: 'Phí vận chuyển:',
            content:
                formatCurrency(c.dataList['Payment']['TransportAmount']) ?? '',
          ),
          rowData(
            title: 'Giảm giá:',
            content:
                formatCurrency(c.dataList['Payment']['DiscountAmount']) ?? '',
          ),
          rowData(
            title: 'Giảm giá thành viên:',
            content:
                formatCurrency(c.dataList['Payment']['DiscountMemberAmount']) ??
                    '',
          ),
          rowData(
            title: 'Tổng tiền:',
            content: formatCurrency(c.dataList['Payment']['TotalAmount']) ?? '',
          ),
          rowData(
            title: 'Hình thức thanh toán:',
            content: paymentMethod,
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget rowDataBold({title, content}) {
    return Container(
      padding: EdgeInsets.only(top: 6),
      child: Row(
        children: [
          Expanded(
            child: TextCustom(
              title: title,
              fontSize: 16,
              color: AppTheme.colorYellow,
              fontWeight: FontWeight.bold,
            ),
          ),
          TextCustom(
            title: content,
            fontSize: 16,
            color: AppTheme.colorBorder,
            fontWeight: FontWeight.bold,
          )
        ],
      ),
    );
  }

  Widget rowData({title, content}) {
    return Container(
      padding: const EdgeInsets.only(top: 6),
      child: Row(
        children: [
          TextCustom(
            title: title,
            fontSize: 16,
            color: AppTheme.colorBorder,
          ),
          const SizedBox(
            width: 32,
          ),
          Expanded(
              child: TextCustom(
            title: content,
            fontSize: 16,
            color: AppTheme.colorBorder,
            textAlign: TextAlign.right,
          ))
        ],
      ),
    );
  }

  Widget _buildRowQuaTrinh({String title, String content}) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 6),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextCustom(
            title: title,
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: AppTheme.colorYellow,
          ),
          SizedBox(
            height: 10,
          ),
          TextCustom(
            title: content,
            fontSize: 16,
            color: AppTheme.colorBorder,
          ),
        ],
      ),
    );
  }
}
