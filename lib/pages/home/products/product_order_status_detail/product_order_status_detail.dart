import 'package:delivery247/services/http/getx_http.dart';
import 'package:get/get.dart';

class ProductOrderStatusDetailController extends GetxController
    with SingleGetTickerProviderMixin {
  GetXHttpProvider httpProvider = Get.find();

  Map dataList = {};

  @override
  void onInit() {
    super.onInit();

    onLoad();
  }

  onLoad() async {
    update();
  }
}
