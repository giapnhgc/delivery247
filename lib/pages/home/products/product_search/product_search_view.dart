import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/bottom_button.dart';
import 'package:delivery247/components/header/header.dart';
import 'package:delivery247/components/text_custom.dart';
import 'package:delivery247/pages/home/products/product_search/product_search.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductSearchView extends GetView<ProductSearchController> {
  final ProductSearchController c = Get.put(ProductSearchController());
  final Map data;
  ProductSearchView({this.data});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HeaderOnlyUI(
        clickBack: () {
          Get.back();
        },
        title: 'Tìm kiếm sản phẩm',
      ),
      backgroundColor: AppTheme.backgroundColor,
      body: mBody(),
      bottomNavigationBar: BottomButton(
        title: 'Tiếp tục',
        onPress: () => {},
      ),
    );
  }

  Widget mBody() {
    return SingleChildScrollView(
      child: GetBuilder<ProductSearchController>(
        builder: (_) => Column(
          children: [
            SizedBox(height: 16),
            Container(
              padding: EdgeInsets.all(16),
              alignment: Alignment.center,
              child: MyText(
                'Tính năng đang phát triển',
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget rowData({title, content}) {
    return Container(
      padding: EdgeInsets.only(top: 6),
      child: Row(
        children: [
          Expanded(
            child: TextCustom(
              title: title,
              fontSize: 16,
              color: AppTheme.colorBorder,
            ),
          ),
          TextCustom(
            title: content,
            fontSize: 16,
            color: AppTheme.colorBorder,
          )
        ],
      ),
    );
  }
}
 