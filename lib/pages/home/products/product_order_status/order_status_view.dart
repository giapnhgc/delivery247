import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/custom_cached_image.dart';
import 'package:delivery247/components/header/header.dart';
import 'package:delivery247/components/text_custom.dart';
import 'package:delivery247/models/hex_color.dart';
import 'package:delivery247/pages/home/products/product_order_status/order_status.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/http/api_url.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class OrderStatusView extends GetView<OrderStatusController> {
  final OrderStatusController c = Get.put(OrderStatusController());
  final Map data;
  OrderStatusView({this.data});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HeaderOnlyUI(
        clickBack: () {
          Get.back();
        },
        title: 'Trạng thái đơn hàng',
      ),
      backgroundColor: AppTheme.backgroundColor,
      body: mBody(),
      // bottomNavigationBar: BottomButton(
      //   title: 'Tiếp tục',
      //   onPress: () => {},
      // ),
    );
  }

  Widget mBody() {
    return GetBuilder<OrderStatusController>(
      builder: (_) => Container(
        child: Column(
          children: [
            Container(
              width: Get.width,
              decoration: BoxDecoration(border: AppTheme.borderBottomLine),
              child: Obx(
                () => SingleChildScrollView(
                  controller: c.scrollControllerForTab,
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(
                      c.listTab.length,
                      (index) => InkWell(
                        child: Container(
                          padding: EdgeInsets.all(16),
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                width: 2,
                                color: c.tabIndex.value == index
                                    ? AppTheme.colorPrimary
                                    : Colors.transparent,
                              ),
                            ),
                          ),
                          child: MyText(
                            c.listTab[index],
                            style: AppTheme.textStyle16.copyWith(
                                color: c.tabIndex.value == index
                                    ? AppTheme.colorPrimary
                                    : AppTheme.colorText),
                          ),
                        ),
                        onTap: () => c.onClickTabItem(index),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Obx(() {
                if (c.tabIndex.value == 0)
                  return _moi();
                else if (c.tabIndex.value == 1)
                  return _dangXuLy();
                else if (c.tabIndex.value == 2)
                  return _dangVanChuyen();
                else if (c.tabIndex.value == 3)
                  return _hoanTat();
                else
                  return _daHuy();
              }),
              // ),
            )
          ],
        ),
      ),
    );
  }

  Widget _moi() {
    c.itemNoti = 0;
    return _itemInside();
  }

  Widget _dangXuLy() {
    c.itemNoti = 1;
    return _itemInside();
  }

  Widget _dangVanChuyen() {
    c.itemNoti = 2;
    return _itemInside();
  }

  Widget _hoanTat() {
    c.itemNoti = 3;
    return _itemInside();
  }

  Widget _daHuy() {
    c.itemNoti = 4;
    return _itemInside();
  }

  Widget _itemInside() {
    List listData = c.filterListData(c.itemNoti);
    return checkNullOrUndifine([listData]) && listData.length > 0
        ? GetBuilder<OrderStatusController>(
            builder: (_) => Container(
              child: RefreshIndicator(
                onRefresh: () async => c.refreshTab(c.itemNoti),
                child: ListView.builder(
                  controller: c.scrollControllerAll,
                  itemCount: listData.length,
                  itemBuilder: (BuildContext _context, int index) {
                    return _itemDon(index, listData: listData);
                  },
                ),
              ),
            ),
          )
        : SizedBox();
  }

  Widget _itemDon(index, {List listData}) {
    return InkWell(
      onTap: () {
        c.onClickDetail(index, listData);
      },
      child: Container(
        padding: EdgeInsets.all(12),
        child: Column(
          children: [
            Container(
              // margin: EdgeInsets.only(bottom: 4),
              padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
              // color: AppTheme.colorWhite,
              decoration: BoxDecoration(
                color: AppTheme.colorWhite,
                border: Border(
                  bottom: BorderSide(color: HexColor('#DBE0E6'), width: 1),
                ),
              ),
              child: Row(
                children: [
                  TextCustom(
                    title: listData[index]["StatusText"].toUpperCase(),
                    fontSize: 16,
                    color: AppTheme.colorBorder,
                    fontWeight: FontWeight.bold,
                  ),
                  Expanded(child: SizedBox()),
                  SvgPicture.asset(
                    'assets/svgs/icon_next.svg',
                    height: 12,
                    width: 12,
                  ),
                ],
              ),
            ),
            _productInItem(index, numberProduct: 0, listData: listData),
            _productInItem(index, numberProduct: 1, listData: listData),
            _otherProductInItem(index, listData: listData),
          ],
        ),
      ),
    );
  }

  Widget _productInItem(index, {int numberProduct, List listData}) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
      color: AppTheme.colorWhite,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: ContainerImageCached(
              width: 60,
              height: 60,
              borderRadius: BorderRadius.circular(0),
              imageUrl: url + listData[index]['Products'][0]['ImagePath'],
            ),
          ),
          SizedBox(width: 8),
          Expanded(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 12),
                  // decoration: BoxDecoration(border: AppTheme.borderBottomLine),
                  child: Row(
                    children: [
                      Expanded(
                        child: MyText(
                          c.productList[index]['Products'][0]['ProductName'],
                          style: AppTheme.textStyle16.medium(),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 8),
                Container(
                  padding: EdgeInsets.only(bottom: 12),
                  child: Row(
                    children: [
                      Container(
                        child: MyText(
                          'Số lượng x' +
                              c.productList[index]['Products'][0]['Quanlity']
                                  .toString(),
                          style: AppTheme.textStyle16.medium(),
                        ),
                      ),
                      SizedBox(width: 12),
                      Expanded(
                          child: Container(
                        alignment: Alignment.centerRight,
                        child: MyText(
                          formatCurrency(
                              c.productList[index]['Products'][0]['Price']),
                          style: AppTheme.textStyle16.bold(),
                        ),
                      )),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _otherProductInItem(index, {List listData}) {
    return Container(
      width: Get.width,
      margin: EdgeInsets.only(bottom: 8),
      padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
      color: AppTheme.colorWhite,
      child: MyText(
        'Và ' + listData.length.toString() + ' sản phẩm khác',
        style: AppTheme.textStyle16.medium(),
      ),
    );
  }

  Widget rowData({title, content}) {
    return Container(
      padding: EdgeInsets.only(top: 6),
      child: Row(
        children: [
          Expanded(
            child: TextCustom(
              title: title,
              fontSize: 16,
              color: AppTheme.colorBorder,
            ),
          ),
          TextCustom(
            title: content,
            fontSize: 16,
            color: AppTheme.colorBorder,
          )
        ],
      ),
    );
  }
}
