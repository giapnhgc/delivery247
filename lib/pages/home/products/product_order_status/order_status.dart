import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/authentication/authentication_state.dart';
import 'package:delivery247/components/loading.dart';
import 'package:delivery247/models/user.dart';
import 'package:delivery247/pages/home/products/product_order_status_detail/product_order_status_detail_view.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OrderStatusController extends GetxController
    with SingleGetTickerProviderMixin {
  GetXHttpProvider httpProvider = Get.find();

  ScrollController scrollControllerForTab = new ScrollController();
  ScrollController scrollControllerAll = new ScrollController();

  final AuthenticationController authenticationController = Get.find();
  User user;

  var listTab = [
    'Mới',
    'Đang xử lý',
    'Đang vận chuyển',
    'Hoàn tất',
    'Đã hủy',
  ];

  final List tiles = [
    {'title': 'Mới', 'type': 'NEW'},
    {'title': 'Đang xử lý', 'type': 'HANDLING'},
    {'title': 'Đang vận chuyển', 'type': 'SHIPPING'},
    {'title': 'Hoàn tất', 'type': 'FINISH'},
    {'title': 'Đã hủy', 'type': 'CANCEL'},
  ];

  List productList = [];

  RxInt tabIndex = 0.obs;
  bool isLoading = false;
  int itemNoti = 0;
  String phone = '';
  String type;

  @override
  void onInit() async {
    super.onInit();

    if (authenticationController.state is Authenticated) {
      user = (authenticationController.state as Authenticated).user;
    } else {
      debugPrint(
          'Not Authenticated: ' + authenticationController.state.toString());
    }

    await loadList(tiles[itemNoti]);

    // scrollControllerAll.addListener(() {
    //   if (scrollControllerAll.position.maxScrollExtent ==
    //       scrollControllerAll.offset) {
    //     loadList(tiles[itemNoti]);
    //   }
    // });
  }

  loadList(Map itemTypeNoti) async {
    showLoadingX(this);
    var _res = await callApiProductStatusList(user);
    hideLoadingX();

    if (_res['status'] == 1) {
      productList = _res['data'];
    }
    update();
  }

  refreshTab(i) {
    loadList(tiles[i]);
  }

  onClickTabItem(index) {
    tabIndex.value = index;
    loadList(tiles[index]);
  }

  Future<dynamic> callApiProductStatusList(user) async {
    var result = await httpProvider.productStatus(user.id.toString());
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  filterListData(itemNoti) {
    List listData = [];
    for (var i = 0; i < productList.length; i++) {
      if (productList[i]['OrderStatus'] == itemNoti) {
        listData.add(productList[i]);
      }
    }
    return listData;
  }

  onClickDetail(int index, List listData) {
    Get.to(() => ProductOrderStatusDetailView(data: listData[index]));
  }
}
