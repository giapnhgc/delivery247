import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/authentication/authentication_state.dart';
import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/customDialog/custom_confirm_dialog.dart';
import 'package:delivery247/components/custom_input_field/custom_text_area_ui.dart';
import 'package:delivery247/components/loading.dart';
import 'package:delivery247/models/cart_info.dart';
import 'package:delivery247/models/user.dart';
import 'package:delivery247/pages/home/home_bottom_bar/home_bottom_bar_view.dart';
import 'package:delivery247/pages/home/products/product_address/address_view.dart';
import 'package:delivery247/pages/home/products/product_order_status/order_status_view.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/constant.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:delivery247/services/share_preferent_module.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CartController extends GetxController with SingleGetTickerProviderMixin {
  GetXHttpProvider httpProvider = Get.find();

  ScrollController scrollControllerAll = new ScrollController();

  final AuthenticationController _authenticationController = Get.find();
  User user;
  CartInformation cartInfo = CartInformation();
  RxBool isAddressNull = true.obs;
  RxBool isAddressShipNull = true.obs;
  var productList = [];

  double totalPrice = 0;
  String addressShip = '';
  Map pickedAddress = {};

  TextAreaCtrl note = TextAreaCtrl(
    // placeholder: 'Nội dung ghi chú',
    labelText: 'Ghi chú',
  );

  @override
  void onInit() {
    super.onInit();
    onLoad();
  }

  onLoad() async {
    onLoadUser();
    await setDataProductList();
    await setAddressShip();
    update();
  }

  onClickBack() {
    Get.back(result: true);
  }

  setDataProductList() async {
    var cartProducts =
        await SPreferentModule().getItem(StorageKey.CART_PROTDUCT);
    if (checkNullOrUndifine([cartProducts])) {
      productList = cartProducts;
    } else {
      productList = [];
    }
    totalPrice = 0;
    for (var i = 0; i < productList.length; i++) {
      totalPrice += productList[i]['Price'] * productList[i]['Quantity'];
    }

    // print(cartInfo);
  }

  onLoadUser() {
    if (_authenticationController.state is Authenticated) {
      user = (_authenticationController.state as Authenticated).user;
      // shartCount.value = user.shartCount;
      checkNullOrUndifine([user.fullAddress])
          ? isAddressNull.value = false
          : isAddressNull.value = true;
    }
  }

  setAddressShip() async {
    var address = await SPreferentModule().getItem(StorageKey.ADDRESS);
    if (checkNullOrUndifine([address]) && address.length > 0) {
      for (var i = 0; i < address.length; i++) {
        if (address[i]['checked']) {
          pickedAddress = address[i];
          addressShip = address[i]['FullAddress'];
          isAddressShipNull.value = false;
        }
      }
    } else {
      addressShip = 'Vui lòng chọn hoặc thêm mới địa chỉ.';
    }
  }

  editAddress() async {
    var res = await Get.to(() => AddressView());
    if (res) {
      await setAddressShip();
      update();
    }
  }

  pay() async {
    if (checkNullOrUndifine([productList]) && productList.length > 0) {
      await addOrders();
    } else {
      showToastCus(
          "Không có sản phẩm nào trong giỏ hàng. Vui lòng thêm sản phẩm muốn mua.");
    }
  }

  addOrders() async {
    var _body = {
      "AccountUseAppId": cartInfo.accountUseAppId,
      "PaymentMethod": 0, //0 tm, 1 ck
      "DeliveryAddressId": pickedAddress['Id'],
      "DiscountCode": "",
      "TransportAmount": 0,
      "DiscountAmount": 0,
      "DiscountMemberAmount": 0,
      "TotalAmount": totalPrice,
      "Remark": note.text,
      "Products": productList
    };

    if (checkNullOrUndifine([pickedAddress]) &&
        pickedAddress.length > 0 &&
        pickedAddress['Id'] != 0) {
      showLoadingX(this);
      var _res = await httpProvider.addOrders(_body);
      hideLoadingX();
      if (_res != null && _res['Code'] == '00') {
        // showToastCus(
        // 'Đã tạo đơn hàng, chúng tôi sẽ liên hệ tới bạn trong thời gian sớm nhất.');
        SPreferentModule().setItem(StorageKey.CART_PROTDUCT, []);
        showAlertFinishDialog(
          isSendMail: true,
          text:
              'Đã tạo đơn hàng, chúng tôi sẽ liên hệ tới bạn trong thời gian sớm nhất.',
        );
      } else {
        showToastCus("Lỗi tạo đơn hàng. Vui lòng thử lại");
      }
    } else {
      showToastCus('Vui lòng chọn địa chỉ giao hàng.');
    }
  }

  addPlusCartItem(int index) {
    productList[index]['Quantity'] = productList[index]['Quantity'] + 1;
    SPreferentModule().setItem(StorageKey.CART_PROTDUCT, productList);
    setDataProductList();
    update();
  }

  minusPlusCartItem(int index) {
    if (productList[index]['Quantity'] <= 1) {
      deleteProduct(index);
    } else {
      productList[index]['Quantity'] = productList[index]['Quantity'] - 1;
      SPreferentModule().setItem(StorageKey.CART_PROTDUCT, productList);
    }
    setDataProductList();
    update();
  }

  deleteProduct(index) {
    productList.removeAt(index);
    SPreferentModule().setItem(StorageKey.CART_PROTDUCT, productList);
    update();
  }

  showAlertFinishDialog(
      {String text, Function onTap, bool isSendMail = false}) {
    return showDialog(
      barrierDismissible: false,
      context: Get.context,
      builder: (BuildContext context) => CustomConfirmDialog(
        content: Column(
          children: [
            MyText(
              text,
              style: AppTheme.textStyle18.grey(),
              textAlign: TextAlign.center,
            ),
          ],
        ),
        onTapYes: () async {
          List cartProducts = [];
          SPreferentModule().setItem(StorageKey.CART_PROTDUCT, cartProducts);
          Get.offAll(() => MyTabView(), duration: Duration(seconds: 0));
        },
        onTapNo: () {
          Get.to(() => OrderStatusView());
        },
        confirmText: 'Về trang chủ',
        declineText: 'Xem đơn hàng',
        alertHeight: 280,
        alertPaddingTop: 30,
        imageUrl: 'assets/images/logo/logo.png',
        isSvg: false,
      ),
    );
  }
}
