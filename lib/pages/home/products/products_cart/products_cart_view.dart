import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/custom_cached_image.dart';
import 'package:delivery247/components/custom_input_field/custom_text_area_ui.dart';
import 'package:delivery247/components/header/header.dart';
import 'package:delivery247/pages/home/products/product_type/product_type_view.dart';
import 'package:delivery247/pages/home/products/products_cart/products_cart.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/http/api_url.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:delivery247/services/modules/delivery_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class CartView extends GetView<CartController> {
  final CartController c = Get.put(CartController());
  final Map data;
  CartView({this.data});
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => c.onClickBack(),
      child: Scaffold(
        appBar: HeaderOnlyUI(
          clickBack: () {
            c.onClickBack();
          },
          title: 'Giỏ hàng',
        ),
        backgroundColor: AppTheme.backgroundColor,
        body: mBody(),
        bottomNavigationBar: mBottomBar(),
        floatingActionButton: _buildFAB(),
      ),
    );
  }

  Widget mBody() {
    return SingleChildScrollView(
      child: GetBuilder<CartController>(
        builder: (_) => Column(
          children: [
            SizedBox(height: 12),
            Container(
              padding: EdgeInsets.only(left: 16, bottom: 12),
              alignment: Alignment.centerLeft,
              child: MyText(
                'Mua tại cửa hàng',
                style: AppTheme.textStyle20,
                color: AppTheme.colorPrimary,
              ),
            ),
            _productShippingAddress(),
            // Container(
            //   padding: EdgeInsets.only(left: 16, bottom: 12),
            //   alignment: Alignment.centerLeft,
            //   child: MyText(
            //     'Hình thức thanh toán',
            //     style: AppTheme.textStyle20,
            //     color: AppTheme.colorPrimary,
            //   ),
            // ),
            SizedBox(
              height: 12,
            ),
            _paymentMethod(),
            _note(),
            SizedBox(
              height: 12,
            ),
            Container(
              padding: EdgeInsets.only(left: 16, bottom: 12),
              alignment: Alignment.centerLeft,
              child: MyText(
                'Danh sách sản phẩm',
                style: AppTheme.textStyle20,
                color: AppTheme.colorPrimary,
              ),
            ),
            _productListInCart(),
            SizedBox(
              height: 48,
            )
          ],
        ),
      ),
    );
  }

  Widget _paymentMethod() {
    return Container(
      color: AppTheme.white,
      padding: EdgeInsets.only(left: 16, bottom: 12, right: 16, top: 12),
      alignment: Alignment.centerLeft,
      child: Column(
        children: [
          Row(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                // padding: EdgeInsets.only(bottom: 12),
                child: MyText(
                  'Hình thức thanh toán',
                  style: AppTheme.textStyle18,
                  color: AppTheme.colorPrimary,
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () {},
                  child: Container(
                    alignment: Alignment.centerRight,
                    // padding: EdgeInsets.only(bottom: 12),
                    child: MyText(
                      'Tiền mặt',
                      style: AppTheme.textStyle18.primary().bold(),
                      // color: AppTheme.colorYellow,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _note() {
    return Container(
      color: AppTheme.white,
      padding: EdgeInsets.only(left: 16, bottom: 12, right: 16, top: 12),
      alignment: Alignment.centerLeft,
      child: Column(
        children: [
          CustomTextAreaComponent(
            controller: c.note,
            validate: false,
            inputFormater: [
              new LengthLimitingTextInputFormatter(1900),
            ],
          ),
        ],
      ),
    );
  }

  Widget _productShippingAddress() {
    return Container(
      color: AppTheme.white,
      padding: EdgeInsets.only(left: 16, bottom: 12, right: 16, top: 12),
      alignment: Alignment.centerLeft,
      child: Column(
        children: [
          Row(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(bottom: 12),
                child: MyText(
                  'Địa chỉ giao hàng',
                  style: AppTheme.textStyle18,
                  color: AppTheme.colorPrimary,
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () => c.editAddress(),
                  child: Container(
                    alignment: Alignment.centerRight,
                    padding: EdgeInsets.only(bottom: 12),
                    child: MyText(
                      c.isAddressNull.value && c.isAddressShipNull.value
                          ? 'Thêm địa chỉ'
                          : 'Thay đổi',
                      style: AppTheme.textStyle18.primary(),
                      // color: AppTheme.colorYellow,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Obx(
            () => c.isAddressNull.value && c.isAddressShipNull.value
                ? SizedBox()
                : Row(
                    children: [
                      MyText(
                        'Người nhận: ',
                        style: AppTheme.textStyle16,
                        color: AppTheme.colorPrimary,
                      ),
                      SizedBox(width: 32),
                      Expanded(
                        child: Container(
                          alignment: Alignment.centerRight,
                          child: MyText(
                            c.user.fullName,
                            style: AppTheme.textStyle16,
                            color: AppTheme.colorPrimary,
                          ),
                        ),
                      ),
                    ],
                  ),
          ),
          Obx(
            () => c.isAddressNull.value && c.isAddressShipNull.value
                ? SizedBox()
                : Row(
                    children: [
                      MyText(
                        'Giao đến: ',
                        style: AppTheme.textStyle16,
                        color: AppTheme.colorPrimary,
                      ),
                      SizedBox(width: 32),
                      Expanded(
                        child: Container(
                          alignment: Alignment.centerRight,
                          child: MyText(
                            c.addressShip,
                            style: AppTheme.textStyle16,
                            color: AppTheme.colorPrimary,
                            textAlign: TextAlign.right,
                          ),
                        ),
                      ),
                    ],
                  ),
          ),
        ],
      ),
    );
  }

  Widget _productListInCart() {
    if (checkNullOrUndifine([c.productList]) && c.productList.length > 0) {
      return Container(
        width: Get.width,
        // height: Get.height,
        color: AppTheme.backgroundColor,
        child: Column(
          children: List.generate(
            c.productList.length,
            (index) {
              return _item(index);
            },
          ),
        ),
      );
    } else {
      return Container(
        padding: EdgeInsets.all(16),
        child: MyText(
          "Không có sản phẩm nào trong giỏ hàng.\n Vui lòng đặt sản phẩm",
          textAlign: TextAlign.center,
        ),
      );
    }
  }

  Widget _item(index) {
    return Container(
      margin: EdgeInsets.only(bottom: 4),
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
      color: AppTheme.colorWhite,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Container(
          //   padding: EdgeInsets.only(top: 2),
          //   child: MyCheckbox(
          //     isChecked: true,
          //   ),
          // ),
          Container(
            child: ContainerImageCached(
              width: 100,
              height: 100,
              borderRadius: BorderRadius.circular(0),
              imageUrl: url + c.productList[index]['ImageLink'],
              //'http://via.placeholder.com/320x320',
            ),
          ),
          SizedBox(width: 8),
          Expanded(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 12),
                  // decoration: BoxDecoration(border: AppTheme.borderBottomLine),
                  child: Row(
                    children: [
                      Expanded(
                        child: MyText(
                          c.productList[index]['ProductName'],
                          style: AppTheme.textStyle16.medium(),
                        ),
                      ),
                      Row(
                        children: [
                          InkWell(
                            child: Row(
                              children: [
                                SizedBox(width: 8),
                                Icon(Delivery247Icon.close)
                              ],
                            ),
                            onTap: () {
                              //delete san pham trong gio
                              c.deleteProduct(index);
                            },
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(height: 32),
                Container(
                  padding: EdgeInsets.only(bottom: 12),
                  child: Row(
                    children: [
                      InkWell(
                        child: Container(
                          child: Icon(
                            Delivery247Icon.minus,
                            color: AppTheme.colorPrimary,
                            size: 18,
                          ),
                        ),
                        onTap: () {
                          //tru so luong sp trong gio
                          c.minusPlusCartItem(index);
                        },
                      ),
                      SizedBox(width: 12),
                      Container(
                        child:
                            MyText(c.productList[index]['Quantity'].toString()),
                      ),
                      SizedBox(width: 12),
                      InkWell(
                        child: Container(
                          child: Icon(
                            Delivery247Icon.plus_1,
                            color: AppTheme.colorPrimary,
                            size: 18,
                          ),
                        ),
                        onTap: () {
                          //cong so luong sp trong gio
                          c.addPlusCartItem(index);
                        },
                      ),
                      Expanded(
                          child: Container(
                        alignment: Alignment.centerRight,
                        child: MyText(
                            formatCurrency(c.productList[index]['Price'])),
                      )),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget mBottomBar() {
    return Container(
      decoration: BoxDecoration(
          color: AppTheme.colorWhite, border: AppTheme.borderTopLine),
      child: SafeArea(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding:
                    EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 8),
                child: GetBuilder<CartController>(
                  builder: (_) => Row(
                    children: [
                      Expanded(
                        child: MyText(
                          'Tạm tính:',
                          style: AppTheme.textStyle16.medium().secondary(),
                        ),
                      ),
                      MyText(
                        formatCurrency(c.totalPrice),
                        style: AppTheme.textStyle16.medium().secondary(),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                width: double.infinity,
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 50,
                        child: TextButton(
                          style: AppTheme.buttonPrimary.copyWith(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                AppTheme.colorYellowWhite),
                          ),
                          child: MyText(
                            'Mua thêm',
                            style: AppTheme.textStyle16.medium().primary(),
                          ),
                          onPressed: () {
                            //them gio hang
                            Get.to(() => DanhMucSanPhamView());
                          },
                        ),
                      ),
                    ),
                    SizedBox(width: 16),
                    Expanded(
                      child: Container(
                        height: 50,
                        child: TextButton(
                          style: AppTheme.buttonPrimary,
                          child: MyText(
                            'Đặt hàng',
                            style: AppTheme.textStyle16.medium().white(),
                          ),
                          onPressed: () {
                            //thanh toan
                            c.pay();
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildFAB() {
    return FloatingActionButton(
      onPressed: () {
        launch('tel://0585669888');
      },
      backgroundColor: AppTheme.colorPrimary,
      child: Icon(Delivery247Icon.phone),
    );
    // return FloatingActionButton.extended(
    //   onPressed: () {
    //     launch('tel://0585669888');
    //   },
    //   label: Text('0585.669.888'),
    //   icon: Icon(Delivery247Icon.phone),
    //   backgroundColor: AppTheme.colorPrimary,
    // );
  }
}
