import 'package:badges/badges.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/custom_bottom_sheet_html_view.dart';
import 'package:delivery247/components/custom_cached_image.dart';
import 'package:delivery247/components/custom_scroll_wrapper.dart';
import 'package:delivery247/components/header/header.dart';
import 'package:delivery247/models/hex_color.dart';
import 'package:delivery247/pages/home/products/product_details/product_detail.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/http/api_url.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:delivery247/services/modules/delivery_icons.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class ProductDetailsView extends GetView<ProductDetailsController> {
  final Map data;
  final String idCat;
  ProductDetailsView({this.data, this.idCat});

  @override
  Widget build(BuildContext context) {
    final ProductDetailsController c =
        Get.put(ProductDetailsController(data, idCat));
    return WillPopScope(
      onWillPop: () => c.onClickBack(),
      child: Scaffold(
        appBar: HeaderOnlyUI(
          clickBack: () {
            c.onClickBack();
          },
          title: 'Chi tiết sản phẩm',
          actionWidget: Container(
            padding: EdgeInsets.only(right: 12, top: 2),
            child: cartView(c),
          ),
        ),
        backgroundColor: AppTheme.backgroundColor,
        body: mBody(c),
        bottomNavigationBar: mBottomBar(c),
        floatingActionButton: _buildFAB(),
      ),
    );
  }

  Widget mBody(c) {
    return SingleChildScrollView(
      child: GetBuilder<ProductDetailsController>(
        builder: (_) => Column(
          children: [
            _productz(c),
            _productDetailsInfoHtml(c),
            _otherProducts(c),
            SizedBox(
              height: 36,
            )
          ],
        ),
      ),
    );
  }

  Widget _productz(c) {
    return Container(
      child: Column(
        children: [
          GetBuilder<ProductDetailsController>(
            builder: (_) {
              if (checkNullOrUndifine([c.listProductDetailsImage]) &&
                  c.listProductDetailsImage.length > 0) {
                return Container(
                  // color: AppTheme.colorWhite,
                  // padding: EdgeInsets.only(top: 16, left: 16, right: 16),
                  child: Container(
                    width: Get.width,
                    margin: EdgeInsets.only(
                      bottom: 12,
                    ),
                    decoration: BoxDecoration(
                      color: AppTheme.colorWhite,
                      boxShadow: AppTheme.boxShadow,
                    ),
                    child: ClipRRect(
                      // borderRadius: BorderRadius.circular(8),
                      child: Column(
                        children: [
                          CarouselSlider(
                            options: CarouselOptions(
                                disableCenter: true,
                                aspectRatio: 1.2,
                                initialPage: 2,
                                viewportFraction: 1,
                                enlargeCenterPage: true,
                                scrollDirection: Axis.horizontal,
                                autoPlay: true,
                                autoPlayInterval: Duration(seconds: 2),
                                onPageChanged: (index, reson) =>
                                    c.onIndexCarouselChange(index)),
                            items: List.generate(
                              c.listProductDetailsImage.length,
                              (index) {
                                return Container(
                                  // padding: EdgeInsets.symmetric(horizontal: 16),
                                  child: InkWell(
                                    child: ContainerImageCached(
                                      width: 320,
                                      height: 320,
                                      borderRadius: BorderRadius.circular(0),
                                      imageUrl: url +
                                          c.listProductDetailsImage[index],
                                    ),
                                    onTap: () => {},
                                  ),
                                );
                              },
                            ),
                            carouselController: c.carouselController,
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: List.generate(
                                  c.listProductDetailsImage.length, (index) {
                                return Obx(
                                  () => Container(
                                    margin: EdgeInsets.all(2),
                                    height: 4,
                                    width: 10,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4),
                                      color: c.indexCarousel.value == index
                                          ? HexColor('#8592AB')
                                          : HexColor('#DBE0E6'),
                                    ),
                                  ),
                                );
                              }),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(12),
                            child: Row(children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    MyText(
                                      c.listProductDetails['Name'],
                                      style: AppTheme.textStyle20.medium(),
                                      maxLine: 2,
                                    ),
                                    SizedBox(height: 8),
                                    MyText(
                                      formatCurrency(
                                          c.listProductDetails['Price']),
                                      style: AppTheme.textStyle12.grey(),
                                    ),
                                  ],
                                ),
                              ),
                              _ratingStar(),
                            ]),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              } else {
                return SizedBox();
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _ratingStar() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        InkWell(
          child: Container(
            alignment: Alignment.centerRight,
            child: Icon(
              Delivery247Icon.star,
              color: AppTheme.colorPrimary,
            ),
          ),
          onTap: () {},
        ),
        InkWell(
          child: Container(
            alignment: Alignment.centerRight,
            child: Icon(
              Delivery247Icon.star,
              color: AppTheme.colorPrimary,
            ),
          ),
          onTap: () {},
        ),
        InkWell(
          child: Container(
            alignment: Alignment.centerRight,
            child: Icon(
              Delivery247Icon.star,
              color: AppTheme.colorPrimary,
            ),
          ),
          onTap: () {},
        ),
        InkWell(
          child: Container(
            alignment: Alignment.centerRight,
            child: Icon(
              Delivery247Icon.star,
              color: AppTheme.colorPrimary,
            ),
          ),
          onTap: () {},
        ),
        InkWell(
          child: Container(
            alignment: Alignment.centerRight,
            child: Icon(
              Delivery247Icon.star_empty,
              color: AppTheme.colorPrimary,
            ),
          ),
          onTap: () {},
        ),
      ],
    );
  }

  Widget _otherProducts(c) {
    return Container(
      color: AppTheme.colorWhite,
      padding: EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.only(
                  bottom: 8,
                ),
                child: MyText(
                  'Sản phẩm thường xem cùng',
                  style: AppTheme.textStyle20,
                ),
              ),
            ],
          ),
          SizedBox(height: 12),
          GetBuilder<ProductDetailsController>(
            builder: (_) => Container(
              height: 280,
              margin: EdgeInsets.only(bottom: 16),
              child: CustomScrollWrapper(
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    if (checkNullOrUndifine(
                            [c.listProductsByCat['Products']]) &&
                        c.listProductsByCat['Products'].length > 0) {
                      return InkWell(
                        child: Container(
                          width: (Get.width / 2) - 24,
                          margin: EdgeInsets.only(
                            right: index % 2 == 0 ? 8 : 0,
                            left: index % 2 == 1 ? 8 : 0,
                            bottom: 12,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: AppTheme.colorWhite,
                            boxShadow: AppTheme.boxShadow,
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Column(
                              children: [
                                ContainerImageCached(
                                  width: (Get.width / 2) - 24,
                                  height: (Get.width / 2) - 24,
                                  imageUrl: url +
                                      c.listProductsByCat['Products'][index]
                                          ['Avatar'],
                                ),
                                // Container(
                                //   width: 1,
                                //   color: AppTheme.colorBorder1,
                                // ),
                                Container(
                                  padding: EdgeInsets.all(12),
                                  child: Column(
                                    children: [
                                      MyText(
                                        c.listProductsByCat['Products'][index]
                                            ['Name'],
                                        style: AppTheme.textStyle16,
                                        maxLine: 2,
                                      ),
                                      Row(children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(height: 8),
                                            MyText(
                                              formatCurrency(
                                                  c.listProductsByCat[
                                                          'Products'][index]
                                                      ['ImportPrice']),
                                              style:
                                                  AppTheme.textStyle12.grey(),
                                            ),
                                          ],
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              InkWell(
                                                child: Container(
                                                  alignment:
                                                      Alignment.centerRight,
                                                  child: Icon(
                                                    Delivery247Icon.cart_plus,
                                                    color:
                                                        AppTheme.colorPrimary,
                                                  ),
                                                ),
                                                onTap: () async {
                                                  //them gio hang
                                                  var res = await addToCart(
                                                      c.listProductsByCat[
                                                          'Products'][index]);
                                                  c.cartNumber.value = res;
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ]),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        onTap: () {
                          //chi tiet san pham
                          c.productDetails(
                              c.listProductsByCat['Products'][index]);
                        },
                      );
                    } else {
                      return SizedBox();
                    }
                  },
                  separatorBuilder: (context, index) => SizedBox(
                    width: 8,
                  ),
                  itemCount: c.listProductsByCat.length,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _productDetailsInfoHtml(c) {
    //CustomBottomSheetHtmlView

    return Container(
      color: AppTheme.white,
      width: Get.width,
      height: 60,
      padding: EdgeInsets.all(8),
      child: TextButton(
        style: AppTheme.buttonPrimary.copyWith(
          backgroundColor:
              MaterialStateProperty.all<Color>(AppTheme.colorYellowWhite),
        ),
        child: MyText(
          'Thông tin sản phẩm',
          style: AppTheme.textStyle16.medium().primary(),
        ),
        onPressed: () {
          Get.bottomSheet(
            SafeArea(
                bottom: true,
                child: CustomBottomSheetHtmlView(
                  label: "Thông tin sản phẩm",
                  html: c.htmlOfProductDetails,
                  maxHeight: Get.height - 200,
                )),
            isScrollControlled: true,
          );
        },
      ),
    );
  }

  Widget mBottomBar(c) {
    return Container(
      decoration: BoxDecoration(
          color: AppTheme.colorWhite, border: AppTheme.borderTopLine),
      child: SafeArea(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                width: double.infinity,
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 50,
                        child: TextButton(
                          style: AppTheme.buttonPrimary.copyWith(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                AppTheme.colorYellowWhite),
                          ),
                          child: MyText(
                            'Thêm vào giỏ',
                            style: AppTheme.textStyle16.medium().primary(),
                          ),
                          onPressed: () async {
                            //them gio hang
                            var res = await addToCart(c.listProductDetails);
                            c.cartNumber.value = res;
                          },
                        ),
                      ),
                    ),
                    SizedBox(width: 16),
                    Expanded(
                      child: Container(
                        height: 50,
                        child: TextButton(
                          style: AppTheme.buttonPrimary,
                          child: MyText(
                            'Mua ngay',
                            style: AppTheme.textStyle16.medium().white(),
                          ),
                          onPressed: () {
                            //Mua ngay
                            c.buyNow();
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildFAB() {
    return FloatingActionButton(
      onPressed: () {
        launch('tel://0585669888');
      },
      backgroundColor: AppTheme.colorPrimary,
      child: Icon(Delivery247Icon.phone),
    );
    // return FloatingActionButton.extended(
    //   onPressed: () {
    //     launch('tel://0585669888');
    //   },
    //   label: Text('0585.669.888'),
    //   icon: Icon(Delivery247Icon.phone),
    //   backgroundColor: AppTheme.colorPrimary,
    // );
  }

  Widget cartView(c) {
    return InkWell(
      child: Container(
        height: 40,
        width: 60,
        child: Container(
          padding: EdgeInsets.only(right: 12, top: 12),
          child: Badge(
            badgeColor: AppTheme.colorRed,
            showBadge: true,
            badgeContent: Container(
              height: 12,
              width: 12,
              alignment: Alignment.center,
              child: Obx(
                () => MyText(
                  c.cartNumber.value <= 9
                      ? c.cartNumber.value.toString()
                      : '9+',
                  style: TextStyle(
                    fontSize: 9,
                    color: AppTheme.colorWhite,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            child: Icon(
              Delivery247Icon.shopping_cart_1,
              color: AppTheme.white,
            ),
          ),
        ),
      ),
      onTap: () => c.goToCartView(),
    );
    // IconButton(
    //   icon:
    //   Icon(Delivery247Icon.shopping_cart_1),
    //   onPressed: () {
    //     Get.to(() => CartView());
    //   },
    // ),
  }
}
