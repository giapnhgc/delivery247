import 'package:carousel_slider/carousel_controller.dart';
import 'package:delivery247/components/loading.dart';
import 'package:delivery247/models/user.dart';
import 'package:delivery247/pages/home/products/products_cart/products_cart_view.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:get/get.dart';

class ProductDetailsController extends GetxController
    with SingleGetTickerProviderMixin {
  GetXHttpProvider httpProvider = Get.find();
  Map data;
  final String idCat;
  ProductDetailsController(this.data, this.idCat);

  final CarouselController carouselController = CarouselController();

  Map listProductDetails = {};
  List listProductDetailsImage = [];
  String htmlOfProductDetails = '';

  Map listProductsByCat = {};

  RxInt indexCarousel = 0.obs;

  User user;
  // CartInformation cartInfo = CartInformation();
  RxInt cartNumber = 0.obs;

  @override
  void onInit() {
    super.onInit();
    onLoad();
  }

  onLoad() async {
    setCartNumber();
    showLoadingX(this);
    await callProductDetails();
    await callListProductByCat();
    hideLoadingX();
    update();
  }

  setCartNumber() async {
    cartNumber.value = await countCartNumber();
  }

  callProductDetails() async {
    var _res = await callApiProductDetails();

    listProductDetails = _res['data'];
    listProductDetailsImage = listProductDetails['ListImageDetails'];
    htmlOfProductDetails = listProductDetails['DetailDescription'];
  }

  Future<dynamic> callApiProductDetails() async {
    var result = await httpProvider.productDetailsz(data['Id']);
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  callListProductByCat() async {
    var _res = await callApiProductListbyCategory();
    listProductsByCat = _res['data'];
  }

  Future<dynamic> callApiProductListbyCategory() async {
    var result = await httpProvider.productListByCategory(idCat, 1, 10);
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  buyNow() async {
    var res = await addToCart(listProductDetails);
    cartNumber.value = res;
    goToCartView();
  }

  goToCartView() async {
    var _res = await Get.to(() => CartView());
    if (_res != null && _res) {
      setCartNumber();
    }
  }

  onIndexCarouselChange(index) {
    indexCarousel.value = index;
  }

  productDetails(Map _data) async {
    data = _data;
    onInit();
    // await onLoad();
    update();
    // Get.to(ProductDetailsView(data: _data, idCat: idCat));
  }

  onClickBack() {
    Get.back(result: true);
  }
}
