import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/bottom_button.dart';
import 'package:delivery247/components/check_box_custom_new.dart';
import 'package:delivery247/components/custom_input_field/custom_drop_down_ui.dart';
import 'package:delivery247/components/custom_input_field/custom_text_field_ui.dart';
import 'package:delivery247/components/header/header.dart';
import 'package:delivery247/pages/home/products/product_address_add/address_add.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/modules/delivery_icons.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class AddressAddView extends GetView<AddressAddController> {
  final AddressAddController c = Get.put(AddressAddController());
  final Map data;
  AddressAddView({this.data});
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => c.onClickBack(),
      child: Scaffold(
        appBar: HeaderOnlyUI(
          clickBack: () {
            c.onClickBack();
          },
          title: 'Thêm địa chỉ giao hàng',
        ),
        backgroundColor: AppTheme.backgroundColor,
        body: mBody(),
        bottomNavigationBar: BottomButton(
          title: 'Tiếp tục',
          onPress: () => c.addAddress(),
        ),
        floatingActionButton: _buildFAB(),
      ),
    );
  }

  Widget mBody() {
    return SingleChildScrollView(
      child: GetBuilder<AddressAddController>(
        builder: (_) => Container(
          padding: EdgeInsets.all(12),
          child: Column(
            children: [
              SizedBox(height: 12),
              CustomTextFieldComponent(
                controller: c.recipientName,
                validate: true,
              ),
              SizedBox(height: 12),
              CustomDropdownComponent(
                controller: c.tinhThanh,
                validate: true,
                onChange: () => c.loadDataQuanHuyen(),
              ),
              SizedBox(height: 12),
              CustomDropdownComponent(
                controller: c.quanHuyen,
                validate: true,
                onChange: () => c.loadDataXaPhg(),
              ),
              SizedBox(height: 12),
              CustomDropdownComponent(
                controller: c.xaPhuong,
                validate: true,
                onChange: () => {},
              ),
              SizedBox(height: 12),
              CustomTextFieldComponent(
                controller: c.phone,
                validate: true,
              ),
              SizedBox(height: 12),
              CustomTextFieldComponent(
                controller: c.address,
                validate: true,
              ),
              SizedBox(height: 12),
              Obx(
                () => InkWell(
                  child: Row(
                    children: [
                      MyCheckbox(
                        isChecked: c.isDefaultCheck.value,
                      ),
                      SizedBox(width: 12),
                      MyText('Đặt là mặc định'),
                    ],
                  ),
                  onTap: () => c.onClickDefault(),
                ),
              ),
              SizedBox(height: 36),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildFAB() {
    return FloatingActionButton(
      onPressed: () {
        launch('tel://0585669888');
      },
      backgroundColor: AppTheme.colorPrimary,
      child: Icon(Delivery247Icon.phone),
    );
    // return FloatingActionButton.extended(
    //   onPressed: () {
    //     launch('tel://0585669888');
    //   },
    //   label: Text('0585.669.888'),
    //   icon: Icon(Delivery247Icon.phone),
    //   backgroundColor: AppTheme.colorPrimary,
    // );
  }
}
