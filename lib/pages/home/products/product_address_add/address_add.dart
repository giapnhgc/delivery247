import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/authentication/authentication_state.dart';
import 'package:delivery247/components/custom_input_field/custom_drop_down_ui.dart';
import 'package:delivery247/components/custom_input_field/custom_text_field_ui.dart';
import 'package:delivery247/components/loading.dart';
import 'package:delivery247/models/user.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:get/get.dart';

class AddressAddController extends GetxController
    with SingleGetTickerProviderMixin {
  GetXHttpProvider httpProvider = Get.find();
  final AuthenticationController _authenticationController = Get.find();
  User user;
  RxBool isDefaultCheck = false.obs;

  TextEditingCtrl recipientName = TextEditingCtrl(
    placeholder: 'Tên người nhận',
  );
  DropDownCtrl tinhThanh = DropDownCtrl(
    placeholder: 'Tỉnh thành',
    listDataInit: [],
  );

  DropDownCtrl quanHuyen = DropDownCtrl(
    placeholder: 'Quận huyện',
    listDataInit: [],
  );

  DropDownCtrl xaPhuong = DropDownCtrl(
    placeholder: 'Xã phường',
    listDataInit: [],
  );
  TextEditingCtrl phone = TextEditingCtrl(
    placeholder: 'Số điện thoại liên hệ',
  );
  TextEditingCtrl address = TextEditingCtrl(
    placeholder: 'Địa chỉ chi tiết',
  );

  List listTinhThanh = [];
  List listQuanHuyen = [];
  List listXaPhuong = [];
  List listAddress = [];

  @override
  void onInit() async {
    super.onInit();
    await onLoad();
  }

  onLoad() async {
    onLoadUser();
    recipientName.text = user.fullName;
    phone.text = user.phoneNumber;
    await loadDataTinhThanh();
    update();
  }

  onClickBack() {
    Get.back(result: true);
  }

  onLoadUser() {
    if (_authenticationController.state is Authenticated) {
      user = (_authenticationController.state as Authenticated).user;
      // shartCount.value = user.shartCount;

    }
  }

//tinh thanh
  loadDataTinhThanh() async {
    showLoadingX(this);
    var _res = await callApiTinhThanh();
    hideLoadingX();
    listTinhThanh = _res['data'];
    tinhThanh.setListData = listTinhThanh
        .map((e) => {
              ...e,
              'value': e['Id'],
              'title': e['Name'],
            })
        .toList();
    update();
  }

  Future<dynamic> callApiTinhThanh() async {
    var result = await httpProvider.getTinhThanh();
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

//quan huyen
  loadDataQuanHuyen() async {
    showLoadingX(this);
    var _res = await callApiQuanHuyen();
    hideLoadingX();
    listQuanHuyen = _res['data'];
    quanHuyen.setListData = listQuanHuyen
        .map((e) => {
              ...e,
              'value': e['Id'],
              'title': e['Name'],
            })
        .toList();
    update();
  }

  Future<dynamic> callApiQuanHuyen() async {
    var result = await httpProvider.getQuanHuyen(tinhThanh.itemValue);
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  //xa phg
  loadDataXaPhg() async {
    showLoadingX(this);
    var _res = await callApiXaPhg();
    hideLoadingX();
    listXaPhuong = _res['data'];
    xaPhuong.setListData = listXaPhuong
        .map((e) => {
              ...e,
              'value': e['Id'],
              'title': e['Name'],
            })
        .toList();
    update();
  }

  Future<dynamic> callApiXaPhg() async {
    var result = await httpProvider.getXaPhuong(quanHuyen.itemValue);
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  addAddress() async {
    var _body = {
      "AccountUseId": user.id,
      "RecipientName": recipientName.text,
      "Phone": phone.text,
      "Address": address.text,
      "IsDefault": isDefaultCheck.value ? 1 : 0, //1 mac dinh, 0 hien tai
      "ProvinceId": tinhThanh.itemValue, //tyinh thanh id
      "DistrictId": quanHuyen.itemValue,
      "WardId": xaPhuong.itemValue,
    };
    var _res = await httpProvider.addAddress(_body);
    if (_res != null && _res['Code'] == '00') {
      showToastCus('Thêm địa chỉ thành công');
      Get.back(result: {'isAdd': true});
    } else {
      showToastCus(_res['Message']);
    }
  }

  onClickDefault() {
    isDefaultCheck.value = !isDefaultCheck.value;
  }
}
