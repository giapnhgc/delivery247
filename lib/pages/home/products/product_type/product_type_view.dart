import 'package:badges/badges.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/custom_cached_image.dart';
import 'package:delivery247/components/custom_scroll_wrapper.dart';
import 'package:delivery247/components/header/header.dart';
import 'package:delivery247/models/hex_color.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/http/api_url.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:delivery247/services/modules/delivery_icons.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import 'product_type.dart';

class DanhMucSanPhamView extends GetView<DanhMucSanPhamController> {
  final DanhMucSanPhamController c = Get.put(DanhMucSanPhamController());
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => c.onClickBack(),
      child: Scaffold(
        appBar: HeaderOnlyUI(
          clickBack: () {
            c.onClickBack();
          },
          title: 'Sản phẩm',
          actionWidget: Container(
            padding: EdgeInsets.only(right: 12, top: 2),
            child: cartView(c),
          ),
        ),
        body: mBody(),
        floatingActionButton: _buildFAB(),
      ),
    );
  }

  Widget mBody() {
    return Container(
      child: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                _productCategory(),
                _banner(),
                _promotion(),
                _productGroupCategory(),
                SizedBox(
                  height: 48,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _productCategory() {
    return GetBuilder<DanhMucSanPhamController>(builder: (_) {
      return Container(
        color: AppTheme.colorWhite,
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(left: 4),
              child: MyText(
                'Danh mục sản phẩm',
                style: AppTheme.textStyle20,
              ),
            ),
            SizedBox(height: 8),
            Container(
              height: 125,
              // margin: EdgeInsets.only(bottom: 16),
              child: CustomScrollWrapper(
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return InkWell(
                      child: Container(
                        padding: EdgeInsets.all(4),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.all(14),
                              decoration: BoxDecoration(
                                border:
                                    Border.all(color: AppTheme.colorBorder1),
                                borderRadius: BorderRadius.circular(14),
                              ),
                              child: Image.network(
                                url + c.listProductCategory[index]['ImagePath'],
                                width: 40,
                                height: 40,
                              ),
                            ),
                            SizedBox(height: 8),
                            MyText(
                              c.listProductCategory[index]['Name'],
                              style: AppTheme.textStyle14,
                              maxLine: 2,
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                      onTap: () =>
                          c.onClickServiceItem(c.listProductCategory[index]),
                    );
                  },
                  separatorBuilder: (context, index) => SizedBox(
                    width: 8,
                  ),
                  itemCount: c.listProductCategory.length,
                ),
              ),
            ),
          ],
        ),
      );
    });
  }

  Widget _banner() {
    return Container(
      color: AppTheme.colorWhite,
      child: GetBuilder<DanhMucSanPhamController>(
          builder: (_) => Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 4, bottom: 4),
                    child: CarouselSlider(
                      options: CarouselOptions(
                          disableCenter: true,
                          aspectRatio: 21 / 9,
                          initialPage: 1,
                          viewportFraction: 1,
                          enlargeCenterPage: true,
                          scrollDirection: Axis.horizontal,
                          autoPlay: true,
                          autoPlayInterval: Duration(seconds: 5),
                          onPageChanged: (index, reson) =>
                              c.onIndexCarouselChange(index)),
                      items: List.generate(c.listBanner.length, (index) {
                        return Container(
                          // padding: EdgeInsets.symmetric(horizontal: 16),
                          child: InkWell(
                            child: Image.network(
                              url + c.listBanner[index]['ImagePath'],
                              width: Get.width,
                              height: (Get.width / (21 / 9)) - 32,
                            ),

                            //url banner
                            // ContainerImageCached(
                            // width: Get.width,
                            // height: (Get.width / 2.2) - 32,
                            // borderRadius: BorderRadius.circular(8),
                            //     imageUrl: c.listBanner[index]['URL']),
                            onTap: () =>
                                c.onClickBannerItem(c.listBanner[index]),
                          ),
                        );
                      }),
                      carouselController: c.carouselController,
                    ),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(c.listBanner.length, (index) {
                        return Obx(
                          () => Container(
                            margin: EdgeInsets.all(2),
                            height: 4,
                            width: 4,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: c.indexCarousel.value == index
                                  ? HexColor('#8592AB')
                                  : HexColor('#DBE0E6'),
                            ),
                          ),
                        );
                      }),
                    ),
                  ),
                ],
              )),
    );
  }

  Widget _promotion() {
    return GetBuilder<DanhMucSanPhamController>(builder: (_) {
      return Container(
        color: AppTheme.colorWhite,
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 8),
                  child: MyText(
                    'Thông tin khuyến mãi',
                    style: AppTheme.textStyle20,
                  ),
                ),
                InkWell(
                  child: Container(
                    padding: EdgeInsets.only(left: 20, top: 8, bottom: 8),
                    child: MyText(
                      'Tất cả',
                      style: AppTheme.textStyle14.grey(),
                    ),
                  ),
                  onTap: () => c.goToSaleView(),
                )
              ],
            ),
            SizedBox(height: 12),
            GetBuilder<DanhMucSanPhamController>(
              builder: (_) => Container(
                height: 280,
                margin: EdgeInsets.only(bottom: 16),
                child: CustomScrollWrapper(
                  child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return InkWell(
                        child: Container(
                          width: (Get.width / 2) - 24,
                          margin: EdgeInsets.only(
                            right: index % 2 == 0 ? 8 : 0,
                            left: index % 2 == 1 ? 8 : 0,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: AppTheme.colorWhite,
                            boxShadow: AppTheme.boxShadow,
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Column(
                              children: [
                                ContainerImageCached(
                                    width: (Get.width / 2) - 24,
                                    height: (Get.width / 2) - 24,
                                    imageUrl: url +
                                        c.listPromotion[index]['ImagePath']),
                                Container(
                                  width: 1,
                                  color: AppTheme.colorBorder1,
                                ),
                                Container(
                                  padding: EdgeInsets.all(12),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      MyText(
                                        c.listPromotion[index]['Name'],
                                        style: AppTheme.textStyle16,
                                        maxLine: 2,
                                      ),
                                      SizedBox(height: 8),
                                      MyText(
                                        'Hạn đến ' +
                                            c.listPromotion[index]
                                                ['ExpireDate'],
                                        // 'Hạn đến ${dateConvertFromStringYYYYMMDD(c.listPromotion[index]['time'])}',
                                        style: AppTheme.textStyle12.grey(),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        onTap: () {
                          c.goToSaleDetailView(c.listPromotion[index]);
                        },
                      );
                    },
                    separatorBuilder: (context, index) => SizedBox(
                      width: 8,
                    ),
                    itemCount: c.listPromotion.length,
                  ),
                ),
              ),
            )
          ],
        ),
      );
    });
  }

  Widget _productGroupCategory() {
    return GetBuilder<DanhMucSanPhamController>(builder: (_) {
      return Column(
        children: List.generate(c.listGroupCategory.length, (index) {
          return _productGroupItem(index);
        }),
      );
    });
  }

  Widget _productGroupItem(i) {
    return Container(
      height: 350,
      color: AppTheme.colorWhite,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 8),
                child: MyText(
                  c.listGroupCategory[i]['ProductCategoryName'],
                  style: AppTheme.textStyle20,
                ),
              ),
              InkWell(
                child: Container(
                  padding: EdgeInsets.only(left: 20, top: 8, bottom: 8),
                  child: MyText(
                    'Tất cả',
                    style: AppTheme.textStyle14.grey(),
                  ),
                ),
                onTap: () => c.onClickAllProductCat(i),
              ),
            ],
          ),
          SizedBox(height: 12),
          GetBuilder<DanhMucSanPhamController>(
            builder: (_) => Container(
              height: 289,
              margin: EdgeInsets.only(bottom: 16),
              child: CustomScrollWrapper(
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    List listDataProduct = c.listGroupCategory[i]['Products'];
                    String tagGiftNew = '';
                    bool isShowGiftOrNewTag = false;

                    if (listDataProduct[index]['IsGift'] == 1) {
                      tagGiftNew = 'Mua 2 tặng 1';
                    }
                    if (listDataProduct[index]['IsNewProduct'] == 1) {
                      tagGiftNew = 'New';
                    }

                    if (listDataProduct[index]['IsGift'] == 0 &&
                        listDataProduct[index]['IsNewProduct'] == 0) {
                      isShowGiftOrNewTag = false;
                    } else {
                      isShowGiftOrNewTag = true;
                    }
                    return InkWell(
                      child: Container(
                        width: (Get.width / 2) - 24,
                        margin: EdgeInsets.only(
                          right: index % 2 == 0 ? 8 : 0,
                          left: index % 2 == 1 ? 8 : 0,
                          bottom: 12,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: AppTheme.colorWhite,
                          boxShadow: AppTheme.boxShadow,
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Stack(
                            children: [
                              Column(
                                children: [
                                  ContainerImageCached(
                                      width: (Get.width / 2) - 24,
                                      height: (Get.width / 2) - 24,
                                      imageUrl: url +
                                          listDataProduct[index]['Avatar']),
                                  Container(
                                    padding: EdgeInsets.all(12),
                                    child: Column(
                                      children: [
                                        MyText(
                                          listDataProduct[index]['Name'],
                                          style: AppTheme.textStyle16,
                                          maxLine: 2,
                                        ),
                                        SizedBox(height: 8),
                                        Row(
                                          children: [
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(height: 8),
                                                MyText(
                                                  formatCurrency(
                                                      listDataProduct[index]
                                                          ['Price']),
                                                  style: AppTheme.textStyle12
                                                      .grey(),
                                                ),
                                              ],
                                            ),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  InkWell(
                                                    child: Container(
                                                      alignment:
                                                          Alignment.centerRight,
                                                      child: Icon(
                                                          Delivery247Icon
                                                              .cart_plus,
                                                          color: AppTheme
                                                              .colorPrimary,
                                                          size: 18),
                                                    ),
                                                    onTap: () async {
                                                      //them gio hang
                                                      var res = await addToCart(
                                                          listDataProduct[
                                                              index]);
                                                      c.cartNumber.value = res;
                                                    },
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              isShowGiftOrNewTag
                                  ? Positioned(
                                      top: 4,
                                      right: 4,
                                      child: Container(
                                        padding: EdgeInsets.only(
                                          left: 12,
                                          right: 12,
                                          top: 4,
                                          bottom: 4,
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          color: AppTheme.colorYellow,
                                          boxShadow: AppTheme.boxShadow,
                                        ),
                                        child: MyText(
                                          tagGiftNew,
                                          style: AppTheme.textStyle16.white(),
                                        ),
                                      ),
                                    )
                                  : SizedBox(),
                            ],
                          ),
                        ),
                      ),
                      onTap: () {
                        //chi tiet san pham
                        c.productDetails(
                            listDataProduct[index],
                            c.listGroupCategory[i]['ProductCategoryId']
                                .toString());
                      },
                    );
                  },
                  separatorBuilder: (context, index) => SizedBox(
                    width: 8,
                  ),
                  itemCount: c.listGroupCategory[i]['Products'].length,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildFAB() {
    return FloatingActionButton(
      onPressed: () {
        launch('tel://0585669888');
      },
      backgroundColor: AppTheme.colorPrimary,
      child: Icon(Delivery247Icon.phone),
    );
    // return FloatingActionButton.extended(
    //   onPressed: () {
    //     launch('tel://0585669888');
    //   },
    //   label: Text('0585.669.888'),
    //   icon: Icon(Delivery247Icon.phone),
    //   backgroundColor: AppTheme.colorPrimary,
    // );
  }

  Widget cartView(c) {
    return InkWell(
      child: Container(
        height: 40,
        width: 60,
        child: Container(
          padding: EdgeInsets.only(right: 12, top: 12),
          child: Badge(
            badgeColor: AppTheme.colorRed,
            showBadge: true,
            badgeContent: Container(
              height: 12,
              width: 12,
              alignment: Alignment.center,
              child: Obx(
                () => MyText(
                  c.cartNumber.value <= 9
                      ? c.cartNumber.value.toString()
                      : '9+',
                  style: TextStyle(
                    fontSize: 9,
                    color: AppTheme.colorWhite,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            child: Icon(
              Delivery247Icon.shopping_cart_1,
              color: AppTheme.white,
            ),
          ),
        ),
      ),
      onTap: () => c.goToCartView(),
    );
  }
}
