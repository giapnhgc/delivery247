import 'package:carousel_slider/carousel_controller.dart';
import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/pages/home/products/product_details/product_detail_view.dart';
import 'package:delivery247/pages/home/products/product_list/product_list_view.dart';
import 'package:delivery247/pages/home/products/products_cart/products_cart_view.dart';
import 'package:delivery247/pages/sales/sales_ui.dart';
import 'package:delivery247/pages/sales_details/sales_details_ui.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:get/get.dart';

class DanhMucSanPhamController extends GetxController
    with SingleGetTickerProviderMixin {
  final CarouselController carouselController = CarouselController();

  GetXHttpProvider httpProvider = Get.find();
  final AuthenticationController _authenticationController = Get.find();

  RxInt indexCarousel = 0.obs;

  List listProductCategory = [];
  List listBanner = [];
  List listPromotion = [];

  //Group Category
  List listGroupCategory = [];
  RxInt cartNumber = 0.obs;

  @override
  void onInit() {
    super.onInit();
    onLoad();
  }

  onLoad() async {
    onloadProductCategory();
    onLoadBanner();
    onLoadPromotion();
    onLoadProductsCategory();
    setCartNumber();
  }

  goToCartView() async {
    var _res = await Get.to(() => CartView());
    if (_res != null && _res) {
      setCartNumber();
    }
  }

  goToSaleView() async {
    var _res = await Get.to(() => SalesView());
    if (_res != null && _res) {
      setCartNumber();
    }
  }

  goToSaleDetailView(data) async {
    var _res = await Get.to(() => SalesDetailsView(data: data));
    if (_res != null && _res) {
      setCartNumber();
    }
  }

  setCartNumber() async {
    cartNumber.value = await countCartNumber();
  }

  onloadProductCategory() async {
    var _res = await callApiProductCategory();
    listProductCategory = _res['data'];
    update();
  }

  Future<dynamic> callApiProductCategory() async {
    var result = await httpProvider.productCategory();
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  onLoadBanner() async {
    var _res = await callApiLoadBanner();
    listBanner = _res['data'];
    update();
  }

  Future<dynamic> callApiLoadBanner() async {
    var result = await httpProvider.bannerList();
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  onLoadPromotion() async {
    var _res = await callApiPromotion();
    listPromotion = _res['data'];
    update();
  }

  Future<dynamic> callApiPromotion() async {
    var result = await httpProvider.promotion(1, 10);
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  onLoadProductsCategory() async {
    var _res = await callApiProductsCategory();
    listGroupCategory = _res['data'];
    update();
  }

  Future<dynamic> callApiProductsCategory() async {
    var result = await httpProvider.productListByGroupCategory();
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  onIndexCarouselChange(index) {
    indexCarousel.value = index;
  }

  onClickBannerItem(item) async {
    // showLoadingX(this);
    // var _res = await httpProvider.getNewBanner(id: item['ID']);
    // if (_res != null && _res['resultlist']['Table'].length > 0) {
    //   Get.to(() => BannerDetailView(),
    //       arguments: _res['resultlist']['Table'][0]);
    // }
  }

  onClickServiceItem(item) {
    Get.to(() => ProductListView(data: item));
  }

  onClickAllProductCat(i) {
    Get.to(
      () => ProductListView(
        data: {
          'Id': listGroupCategory[i]['ProductCategoryId'].toString(),
          'Name': listGroupCategory[i]['ProductCategoryName'].toString()
        },
      ),
    );
  }

  onClickBack() {
    Get.back(result: true);
  }

  productDetails(Map data, String catId) {
    Get.to(() => ProductDetailsView(
          data: data,
          idCat: catId,
        ));
  }
}
