import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/header/header.dart';
import 'package:delivery247/components/radio_box_custom.dart';
import 'package:delivery247/pages/home/products/product_address/address.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/modules/delivery_icons.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddressView extends GetView<AddressController> {
  final AddressController c = Get.put(AddressController());
  final Map data;
  AddressView({this.data});
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => c.onClickBack(),
      child: Scaffold(
        appBar: HeaderOnlyUI(
          clickBack: () {
            c.onClickBack();
          },
          title: 'Địa điểm giao hàng',
        ),
        backgroundColor: AppTheme.backgroundColor,
        body: mBody(),
        bottomNavigationBar: mBottomBar(),
      ),
    );
  }

  Widget mBody() {
    return SingleChildScrollView(
      child: GetBuilder<AddressController>(
        builder: (_) => c.listAddress.length > 0
            ? Column(
                children: List.generate(
                  c.listAddress.length,
                  (index) => Container(
                    margin: EdgeInsets.only(top: 16, left: 16, right: 16),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8)),
                    child: TextButton(
                      style: AppTheme.buttonNoPadding,
                      onPressed: () => c.onCheckBoxChange(index),
                      child: Container(
                        padding: EdgeInsets.all(16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: CustomRadiobox(
                                isChecked:
                                    // c.listAddress[index]['IsDefault'] == 1
                                    // ? true
                                    // : false,
                                    c.listAddress[index]['checked'],
                              ),
                            ),
                            SizedBox(width: 18),
                            Expanded(
                              child: Column(
                                children: [
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    child: MyText(
                                      c.listAddress[index]['RecipientName'],
                                      style: AppTheme.textStyleDark,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    child: MyText(
                                      c.listAddress[index]['Phone'],
                                      style: AppTheme.textStyleDark,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    child: MyText(
                                      c.listAddress[index]['FullAddress'],
                                      style: AppTheme.textStyleDark,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            InkWell(
                              child: Row(
                                children: [
                                  SizedBox(width: 8),
                                  Icon(
                                    Delivery247Icon.more_vert,
                                    color: AppTheme.colorPrimary,
                                  )
                                ],
                              ),
                              onTap: () {
                                //more btn,  edit address.
                                c.editAddress();
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              )
            : SizedBox(),
      ),
    );
  }

  Widget mBottomBar() {
    return Container(
      decoration: BoxDecoration(
          color: AppTheme.colorWhite, border: AppTheme.borderTopLine),
      child: SafeArea(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                width: double.infinity,
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 50,
                        child: TextButton(
                          style: AppTheme.buttonPrimary.copyWith(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                AppTheme.colorYellowWhite),
                          ),
                          child: MyText(
                            'Thêm mới',
                            style: AppTheme.textStyle16.medium().primary(),
                          ),
                          onPressed: () {
                            //them moi dia chi
                            c.addAddress();
                          },
                        ),
                      ),
                    ),
                    SizedBox(width: 16),
                    Expanded(
                      child: Container(
                        height: 50,
                        child: TextButton(
                          style: AppTheme.buttonPrimary,
                          child: MyText(
                            'Đã chọn',
                            style: AppTheme.textStyle16.medium().white(),
                          ),
                          onPressed: () {
                            c.onChooseAddress();
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
