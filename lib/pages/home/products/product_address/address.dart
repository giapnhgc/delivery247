import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/authentication/authentication_state.dart';
import 'package:delivery247/components/loading.dart';
import 'package:delivery247/models/user.dart';
import 'package:delivery247/pages/home/products/product_address_add/address_add_view.dart';
import 'package:delivery247/services/constant.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:delivery247/services/share_preferent_module.dart';
import 'package:get/get.dart';

class AddressController extends GetxController
    with SingleGetTickerProviderMixin {
  GetXHttpProvider httpProvider = Get.find();
  final AuthenticationController _authenticationController = Get.find();
  User user;

  List listAddress = [];

  @override
  void onInit() {
    super.onInit();

    onLoad();
  }

  onLoad() async {
    onLoadUser();
    loadData();
  }

  onLoadUser() {
    if (_authenticationController.state is Authenticated) {
      user = (_authenticationController.state as Authenticated).user;
      // shartCount.value = user.shartCount;
    }
  }

  loadData() async {
    await setAddress();
    update();
  }

  setAddress() async {
    var address = await SPreferentModule().getItem(StorageKey.ADDRESS);
    if (checkNullOrUndifine([address]) && address.length > 0) {
      listAddress = address;
    } else {
      showLoadingX(this);
      var _res = await callApiListAddress();
      hideLoadingX();
      listAddress = _res['data'];

      for (var i in listAddress) {
        if (i['IsDefault'] == 1 && i['checked'] == null) {
          i['checked'] = true;
        } else {
          i['checked'] = false;
        }
      }

      SPreferentModule().setItem(StorageKey.ADDRESS, listAddress);
    }
  }

  Future<dynamic> callApiListAddress() async {
    var result = await httpProvider.getListAddress(user.id.toString());
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }

  onCheckBoxChange(int index) {
    for (var i = 0; i < listAddress.length; i++) {
      if (i == index) {
        listAddress[i]['checked'] = true;
      } else {
        listAddress[i]['checked'] = false;
      }
    }
    SPreferentModule().setItem(StorageKey.ADDRESS, listAddress);
    update();
  }

  setAddressAfterAdd() async {
    showLoadingX(this);
    var _res = await callApiListAddress();
    hideLoadingX();
    listAddress = _res['data'];

    for (var i = 0; i < listAddress.length; i++) {
      if (i == listAddress.length - 1) {
        listAddress[i]['checked'] = true;
      } else {
        listAddress[i]['checked'] = false;
      }
    }
    SPreferentModule().setItem(StorageKey.ADDRESS, listAddress);
  }

  addAddress() async {
    var _res = await Get.to(() => AddressAddView());
    if (_res != null && _res['isAdd']) {
      //lay dia chi moi nhap
      await setAddressAfterAdd();
      loadData();
    }
  }

  editAddress() {
    //TODO:Sua thong tin dia chi chua co api se lam sau
  }

  onClickBack() async {
    Get.back(result: true);
  }

  onChooseAddress() async {
    bool hasChooseAddress = false;
    for (var i = 0; i < listAddress.length; i++) {
      if (listAddress[i]['checked']) {
        hasChooseAddress = true;
      }
    }

    if (hasChooseAddress) {
      Get.back(result: true);
    } else {
      showToastCus("Vui lòng chọn địa chỉ");
    }
  }
}
