import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/authentication/authentication_state.dart';
import 'package:delivery247/pages/authenticate/login/login_view.dart';
import 'package:delivery247/pages/home/home_bottom_bar/home_bottom_bar_view.dart';
import 'package:delivery247/services/constant.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:delivery247/services/http/http_client.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:delivery247/services/share_preferent_module.dart';
import 'package:get/get.dart';

class SplashController extends GetxController {
  final AuthenticationController _authenticationController = Get.find();

  GetXHttpProvider httpProvider = Get.find();

  @override
  void onInit() async {
    super.onInit();
    HttpClient().onLoadDevice();
    onActionWhenLoading();
  }

  onActionWhenLoading() async {
    var loginInfo = await SPreferentModule().getItem(StorageKey.LOGIN);
    await Future.delayed(Duration(seconds: 3));
    if (checkNullOrUndifine([loginInfo])) {
      await _authenticationController.signIn(
          loginInfo['userName'], loginInfo['password']);

      if (_authenticationController.state is Authenticated) {
        // SPreferentModule().clearAll();
        // SqliteModule.instance.dropTable();
        // SqliteModule.instance.createTable();
        Get.off(() => MyTabView(), duration: Duration(seconds: 0));
      } else {
        Get.off(() => LoginView(), duration: Duration(seconds: 0)); //login
      }
    } else {
      Get.off(() => LoginView(), duration: Duration(seconds: 0)); //login
    }
  }

  onFinishLoading() {
    // Get.off(() => MyTabView(), duration: Duration(seconds: 0));
  }

  onLoadingError() {
    // Get.off(() => LoginView(), duration: Duration(seconds: 0));
  }
}
