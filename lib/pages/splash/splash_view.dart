import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:rive/rive.dart';

import 'splash.dart';

class SplashView extends GetView<SplashController> {
  final SplashController c = Get.put(SplashController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: mBody(context),
    );
  }

  Widget mBody(context) {
    final size = MediaQuery.of(context).size;
    final sizeWidth = size.width;
    final sizeHeight = size.height;
    return Container(
      width: sizeWidth,
      height: sizeHeight,
      color: AppTheme.white,
      child: Image.asset(
        'assets/images/logo/logo.png',
        width: 100,
        height: 100,
      ),
    );
    // return RiveAnimation.asset(
    //   'assets/rive/247delivery.riv',
    // );
  }
}
