import 'package:delivery247/pages/authenticate/login/login_view.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterController extends GetxController
    with SingleGetTickerProviderMixin {
  GetXHttpProvider httpProvider = Get.find();

  TextEditingController name = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController pass = TextEditingController();
  TextEditingController passConfirm = TextEditingController();

  var loginData;

  String sourceTypeLogin = '';
  String socialUserFullName = '';
  String socialUserIdentifier = '';
  String socialUserEmail = '';

  @override
  void onInit() {
    super.onInit();
  }

  onClickRegister() async {
    if (name.text == '') return showToastCus("Bạn chưa nhập họ tên");
    if (phone.text == '') return showToastCus("Bạn chưa nhập số điện thoại");
    if (checkValidatePhone(phone.text))
      return showToastCus("Số điện thoại không hợp lệ");
    if (pass.text == '') return showToastCus("Bạn chưa nhập mật khẩu");
    if (passConfirm.text == '')
      return showToastCus("Bạn chưa nhập mật khẩu xác nhận");
    if (passConfirm.text != pass.text)
      return showToastCus("Mật khẩu xác nhận không trùng khớp");

    var _body = {
      "Phone": phone.text,
      "FullName": name.text,
      "Password": pass.text,
      "ConfirmPassword": passConfirm.text
    };
    var _res = await httpProvider.register(_body);
    if (_res != null && _res['Code'] == '00') {
      showToastCus(
          'Đăng ký thành công. Chào mừng bạn đến với 247 Delivery. Mời bạn đăng nhập.');
      Future.delayed(Duration(milliseconds: 1000), () {
        Get.offAll(LoginView());
      });
    } else {
      // ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
      //   content: Text(_res['Message']),
      // ));
      showToastCus(_res['Message']);
    }
  }
}
