import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/custom_round_text_field/custom_round_text_field.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'register.dart';

class RegisterView extends GetView<RegisterController> {
  final RegisterController c = Get.put(RegisterController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
        elevation: 0,
        backgroundColor: Colors.transparent,
        brightness: Brightness.light,
      ),
      body: mBody(context),
    );
  }

  Widget mBody(context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          children: [
            SizedBox(height: 100),
            MyText('Đăng ký tài khoản',
                style: AppTheme.textStyle.copyWith(fontSize: 32)),
            SizedBox(height: 48),
            CustomRoundTextField(
              controller: c.name,
              hintText: 'Nhập họ tên',
              keyboardType: TextInputType.name,
            ),
            SizedBox(height: 16),
            CustomRoundTextField(
              controller: c.phone,
              hintText: 'Nhập số điện thoại',
              keyboardType: TextInputType.phone,
            ),
            SizedBox(height: 16),
            CustomRoundTextField(
              controller: c.pass,
              hintText: 'Nhập mật khẩu',
              obscureText: true,
              keyboardType: TextInputType.text,
            ),
            SizedBox(height: 16),
            CustomRoundTextField(
              controller: c.passConfirm,
              hintText: 'Nhập lại mật khẩu',
              obscureText: true,
              keyboardType: TextInputType.text,
            ),
            SizedBox(height: 16),
            Container(
              width: double.infinity,
              height: 48,
              child: TextButton(
                style: AppTheme.buttonPrimary,
                child: MyText(
                  'Đăng ký',
                  style: AppTheme.textStyle16.white().medium(),
                ),
                onPressed: () => c.onClickRegister(),
              ),
            ),
            SizedBox(height: 16),
            Container(
              width: double.infinity,
              height: 48,
              child: TextButton(
                child: MyText(
                  'Đăng nhập',
                  style: AppTheme.textStyle16.primary().medium(),
                ),
                onPressed: () => Get.back(),
              ),
            ),
            SizedBox(
              height: 70,
            ),
            Wrap(
              alignment: WrapAlignment.center,
              children: [
                MyText(
                  'Nhấn nút đăng ký là bạn đồng ý với ',
                  style: AppTheme.textStyle12,
                ),
                InkWell(
                  onTap: () {},
                  child: MyText(
                    'Điều khoản sử dụng của 247Delivery',
                    style: AppTheme.textStyle12.primary(),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
