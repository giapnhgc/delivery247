import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/authentication/authentication_state.dart';
import 'package:delivery247/components/loading.dart';
import 'package:delivery247/models/user.dart';
import 'package:delivery247/pages/home/home_bottom_bar/home_bottom_bar_view.dart';
import 'package:delivery247/services/constant.dart';
import 'package:delivery247/services/http/getx_http.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:delivery247/services/share_preferent_module.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class LoginController extends GetxController with SingleGetTickerProviderMixin {
  final AuthenticationController _authenticationController = Get.find();

  GetXHttpProvider httpProvider = Get.find();

  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();

  BuildContext loadingContext;

  @override
  void onInit() {
    super.onInit();
  }

  onLoginResult() async {
    FocusScope.of(Get.overlayContext).unfocus();
    if (username.text.length == 0) {
      showToastCus('Vui lòng nhập số điện thoại');
    } else if (password.text.length == 0) {
      showToastCus('Vui lòng nhập mật khẩu');
    } else if (username.text.length != 0 && password.text.length != 0) {
      await _authenticationController.signIn(username.text, password.text);
      if (_authenticationController.state is Authenticated) {
        // SqliteModule.instance.createTable();
        SPreferentModule().setItem(StorageKey.LOGIN,
            {'userName': username.text, 'password': password.text});

        User user = (_authenticationController.state as Authenticated).user;
        await setAddress(user);

        Get.off(() => MyTabView(), duration: Duration(seconds: 0));
      } else {
        showToastCus('Đăng nhập không thành công');
      }
    }
  }

  setAddress(user) async {
    showLoadingX(this);
    var _res = await callApiListAddress(user);
    hideLoadingX();
    var listAddress = _res['data'];

    for (var i in listAddress) {
      if (i['IsDefault'] == 1 && i['checked'] == null) {
        i['checked'] = true;
      } else {
        i['checked'] = false;
      }
    }

    SPreferentModule().setItem(StorageKey.ADDRESS, listAddress);
  }

  Future<dynamic> callApiListAddress(user) async {
    var result = await httpProvider.getListAddress(user.id.toString());
    if (result['Code'] == '00') {
      return {
        'status': 1,
        'data': result['Result'],
      };
    } else {
      return {'status': 0, 'message': 'sysErr'.tr};
    }
  }
}
