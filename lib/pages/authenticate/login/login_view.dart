import 'package:delivery247/components/_text_custom.dart';
import 'package:delivery247/components/custom_round_text_field/custom_round_text_field.dart';
import 'package:delivery247/models/hex_color.dart';
import 'package:delivery247/pages/authenticate/register/register_view.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'login.dart';

class LoginView extends GetView<LoginController> {
  final LoginController c = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Center(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(15),
                child: Column(
                  children: <Widget>[
                    logoImg(800.0),
                    // title(),
                    phoneInput(),
                    SizedBox(height: 16),
                    passwordInput(),
                    SizedBox(height: 16),
                    loginButton(),
                    // divider(),
                    // loginByGoogle(context),
                    // loginByApple(context),
                    register()
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget logoImg(height) {
    return Container(
      alignment: Alignment.center,
      child:
          // SvgPicture.asset(
          //   'assets/svgs/login_mobile.svg',
          //   color: AppTheme.colorText,
          // ),
          Image.asset(
        'assets/images/logo/logo1.png',
        height: height * 0.3,
      ),
    );
  }

  Widget title() {
    return MyText(
      "Đăng nhập",
      style: AppTheme.textStyle14.copyWith(fontSize: 30),
    );
  }

  Widget phoneInput() {
    return Padding(
      padding: EdgeInsets.only(top: 30),
      child: CustomRoundTextField(
        controller: c.username,
        hintText: 'Số điện thoại',
        // prefixIcon: Container(
        //   padding: EdgeInsets.symmetric(horizontal: 15),
        //   child: SvgPicture.asset('assets/svgs/icon_phone_number.svg'),
        // ),
        keyboardType: TextInputType.number,
      ),
    );
  }

  Widget passwordInput() {
    return CustomRoundTextField(
      controller: c.password,
      hintText: 'Mật khẩu',
      // prefixIcon: Container(
      //   padding: EdgeInsets.symmetric(horizontal: 15),
      //   child: SvgPicture.asset('assets/svgs/icon_password.svg'),
      // ),
      keyboardType: TextInputType.text,
      obscureText: true,
      suffixIcon: InkWell(
          child: Container(
            width: 100,
            margin: EdgeInsets.only(right: 16),
            alignment: Alignment.centerRight,
            child: MyText(
              'Quên mật khẩu?',
              style: AppTheme.textStyle14.primary(),
            ),
          ),
          onTap: () {} // c.onForgotPassword(),
          ),
    );
  }

  Widget loginButton() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            height: 48,
            child: TextButton(
              style: AppTheme.buttonPrimary,
              onPressed: () => c.onLoginResult(),
              child: MyText(
                'Đăng nhập',
                style: AppTheme.textStyle16.white().medium(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget divider() {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: 20,
        horizontal: 70,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Flexible(
            child: Divider(
              color: HexColor('#DBE0E6'),
            ),
          ),
          SizedBox(width: 16),
          MyText('Hoặc'),
          SizedBox(width: 16),
          Flexible(
            child: Divider(
              color: HexColor('#DBE0E6'),
            ),
          ),
        ],
      ),
    );
  }

  // Widget loginByGoogle(BuildContext context) {
  // return Padding(
  //   padding: EdgeInsets.only(top: 16, bottom: 10),
  //   child: Container(
  //     width: double.infinity,
  //     height: 48,
  //     child: TextButton(
  //       style: AppTheme.buttonPrimaryOutline.copyWith(
  //           shape: MaterialStateProperty.all<RoundedRectangleBorder>(
  //         RoundedRectangleBorder(
  //           borderRadius: BorderRadius.circular(10.0),
  //           side: BorderSide(color: HexColor('#DBE0E6')),
  //         ),
  //       )),
  //       child: Row(
  //         crossAxisAlignment: CrossAxisAlignment.center,
  //         mainAxisAlignment: MainAxisAlignment.center,
  //         children: [
  //           SvgPicture.asset(
  //             'assets/svgs/icon_login_google.svg',
  //             width: 20.5,
  //             fit: BoxFit.contain,
  //           ),
  //           SizedBox(
  //             width: 14,
  //           ),
  //           MyText(
  //             'Đăng nhập bằng Google',
  //             style: AppTheme.textStyle20,
  //           )
  //         ],
  //       ),
  //       onPressed: () => c.onLoginGoogle(),
  //     ),
  //   ),
  // );
  // }

  // Widget loginByApple(BuildContext context) {
  //   int versionIOS = 0;
  //   if (Platform.isIOS && checkNullOrUndifine([HttpClient().iosVersion])) {
  //     versionIOS = int.parse(HttpClient().iosVersion.split('.')[0]);
  //     if (versionIOS >= 13) {
  //       return Padding(
  //         padding: EdgeInsets.only(top: 8, bottom: 10),
  //         child: Container(
  //           width: double.infinity,
  //           height: 48,
  //           child: TextButton(
  //             style: AppTheme.buttonPrimaryOutline.copyWith(
  //                 shape: MaterialStateProperty.all<RoundedRectangleBorder>(
  //               RoundedRectangleBorder(
  //                 borderRadius: BorderRadius.circular(10.0),
  //                 side: BorderSide(color: HexColor('#DBE0E6')),
  //               ),
  //             )),
  //             child: Row(
  //               crossAxisAlignment: CrossAxisAlignment.center,
  //               mainAxisAlignment: MainAxisAlignment.center,
  //               children: [
  //                 SvgPicture.asset(
  //                   'assets/svgs/icon_login_apple.svg',
  //                   width: 24,
  //                   fit: BoxFit.contain,
  //                 ),
  //                 SizedBox(
  //                   width: 14,
  //                 ),
  //                 MyText(
  //                   'Đăng nhập bằng Apple',
  //                   style: AppTheme.textStyle20,
  //                 )
  //               ],
  //             ),
  //             onPressed: () => c.onAppleLogin(),
  //           ),
  //         ),
  //       );
  //     } else {
  //       return SizedBox();
  //     }
  //   } else {
  //     return SizedBox();
  //   }
  // }

  Widget register() {
    return Padding(
      padding: EdgeInsets.only(top: 60, bottom: 10),
      child: Wrap(
        alignment: WrapAlignment.center,
        children: [
          MyText(
            'Bạn chưa có tài khoản? ',
            style: AppTheme.textStyle14,
          ),
          InkWell(
            onTap: () {
              Get.to(() => RegisterView());
            },
            child: MyText(
              'Đăng ký',
              style: AppTheme.textStyle14.primary(),
            ),
          )
        ],
      ),
    );
  }
}
