import 'package:delivery247/components/bottom_button.dart';
import 'package:delivery247/components/header/header.dart';
import 'package:delivery247/components/text_custom.dart';
import 'package:delivery247/pages/sales_details/sales_details.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SalesDetailsView extends GetView<SalesDetailsController> {
  final SalesDetailsController c = Get.put(SalesDetailsController());
  final Map data;
  SalesDetailsView({this.data});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HeaderOnlyUI(
        clickBack: () {
          Get.back();
        },
        title: 'Chuơng trình khuyến mãi',
      ),
      backgroundColor: AppTheme.backgroundColor,
      body: mBody(),
      bottomNavigationBar: BottomButton(
        title: 'Tiếp tục',
        onPress: () => {},
      ),
    );
  }

  Widget mBody() {
    return SingleChildScrollView(
      child: GetBuilder<SalesDetailsController>(
        builder: (_) => Column(
          children: [
            SizedBox(height: 4),
          ],
        ),
      ),
    );
  }

  Widget rowData({title, content}) {
    return Container(
      padding: EdgeInsets.only(top: 6),
      child: Row(
        children: [
          Expanded(
            child: TextCustom(
              title: title,
              fontSize: 16,
              color: AppTheme.colorBorder,
            ),
          ),
          TextCustom(
            title: content,
            fontSize: 16,
            color: AppTheme.colorBorder,
          )
        ],
      ),
    );
  }
}
