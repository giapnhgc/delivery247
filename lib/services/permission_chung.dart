import 'dart:io';
import 'package:delivery247/components/text_custom.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/modules/app_module.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

Future<PermissionStatus> checkContactPermission() async {
  PermissionStatus permission = await Permission.contacts.status;
  if (permission != PermissionStatus.granted &&
      permission != PermissionStatus.permanentlyDenied) {
    PermissionStatus permissionStatus = await Permission.contacts.request();
    return permissionStatus;
  } else {
    return permission;
  }
}

Future<PermissionStatus> checkCameraPermission() async {
  PermissionStatus permission = await Permission.camera.status;

  if (Platform.isAndroid) {
    if (permission == PermissionStatus.denied) {
      //SẼ CÓ CASE TRÊN ANDROID XẢY RA KHI YC QUYỀN QUÁ 2-3 LẦN MÀ KHÔNG CHẤP NHẬN
      //THÌ LẦN TIẾP THEO GỌI REQUEST SẼ KHÔNG HIỆN ALERT NỮA.
      //XẢY RA TỎNG KHI TRẠNG THÁI VẪN LÀ DENY
      PermissionStatus permissionStatus = await Permission.camera.request();
      return permissionStatus;
    } else if (permission == PermissionStatus.permanentlyDenied) {
      return permission;
    } else {
      PermissionStatus permissionStatus = await Permission.camera.request();
      return permissionStatus;
    }
  } else {
    //IOS
    if (permission == PermissionStatus.permanentlyDenied) {
      return permission;
    } else {
      PermissionStatus permissionStatus = await Permission.camera.request();
      return permissionStatus;
    }
  }

  // if ((permission == PermissionStatus.denied && Platform.isAndroid) ||
  //       permission == PermissionStatus.permanentlyDenied) {
  //     return permission;
  //   } else {
  //     PermissionStatus permissionStatus = await Permission.camera.request();
  //     return permissionStatus;
  //   }
}

Future<PermissionStatus> checkMicrophonePermission() async {
  PermissionStatus permission = await Permission.microphone.status;

  if (Platform.isAndroid) {
    if (permission == PermissionStatus.denied) {
      PermissionStatus permissionStatus = await Permission.microphone.request();
      return permissionStatus;
    } else if (permission == PermissionStatus.permanentlyDenied) {
      return permission;
    } else {
      PermissionStatus permissionStatus = await Permission.microphone.request();
      return permissionStatus;
    }
  } else {
    //IOS
    if (permission == PermissionStatus.permanentlyDenied) {
      return permission;
    } else {
      PermissionStatus permissionStatus = await Permission.microphone.request();
      return permissionStatus;
    }
  }

  // if ((permission == PermissionStatus.denied && Platform.isAndroid) ||
  //     permission == PermissionStatus.permanentlyDenied) {
  //   return permission;
  // } else {
  //   PermissionStatus permissionStatus = await Permission.microphone.request();
  //   return permissionStatus;
  // }
}

Future<PermissionStatus> checkPhotoPermission() async {
  PermissionStatus permission = await Permission.photos.status;
  if (permission == PermissionStatus.permanentlyDenied) {
    return permission;
  } else {
    PermissionStatus permissionStatus = await Permission.photos.request();
    return permissionStatus;
  }
  // if ((permission == PermissionStatus.denied && Platform.isAndroid) ||
  //     permission == PermissionStatus.permanentlyDenied) {
  //   return permission;
  // } else {
  //   PermissionStatus permissionStatus = await Permission.photos.request();
  //   return permissionStatus;
  // }
}

Future<PermissionStatus> checkstoragePermission() async {
  PermissionStatus permission = await Permission.storage.status;
  if (permission == PermissionStatus.permanentlyDenied) {
    return permission;
  } else {
    PermissionStatus permissionStatus = await Permission.storage.request();
    return permissionStatus;
  }

  // if ((permission == PermissionStatus.denied && Platform.isAndroid) ||
  //     permission == PermissionStatus.permanentlyDenied) {
  //   return permission;
  // } else {
  //   PermissionStatus permissionStatus = await Permission.storage.request();
  //   return permissionStatus;
  // }
}

showPermissionDialog({dynamic context, String text, Function onTap}) {
  return showConfirmDialog(
    context: context,
    content: Column(
      children: [
        TextCustom(
          title: text,
          textAlign: TextAlign.center,
          color: AppTheme.colorText,
          fontSize: 20,
          fontWeight: FontWeight.w500,
        ),
      ],
    ),
    onTapYes: () {
      openAppSettings();
    },
    confirmText: 'aSetting'.tr,
    declineText: 'exit'.tr,
  );
}
