final url = "https://cms.247delivery.com.vn";

const String PRODUCT_CATEGORY = '/api/products/list-product-category';
const String BANNER_LIST = '/api/comon/list-banner';
const String PROMOTION = '/api/comon/list-promotion-news';
const String PRODUCT_LIST_BY_GROUP_CATEGORY =
    '/api/products/list-product-by-group-category';
const String PRODUCT_LIST_BY_CATEGORY =
    '/api/products/list-product-by-categoryid';
const String PRODUCT_DETAILS = '/api/products/detail-product';
const String TINH_THANH = '/api/Comon/list-province';
const String QUAN_HUYEN = '/api/Comon/list-district';
const String XA_PHUONG = '/api/Comon/list-wards';
const String ADDRESS = '/api/AccountUseAppMobile/list-delivery-address';
const String ADD_ADDRESS = '/api/AccountUseAppMobile/register-delivery-address';
const String ADD_ORDERS = '/api/Order/post';
const String PRODUCT_STATUS = '/api/AccountUseAppMobile/list-order';

const String RESGISTER = '/api/AccountUseAppMobile/register';
const String LOGIN = '/api/AccountUseAppMobile/login';
