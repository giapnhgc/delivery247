import 'dart:async';
import 'dart:convert';
import 'dart:isolate';
import 'package:connectivity/connectivity.dart';
import 'package:crypto/crypto.dart';
import 'package:delivery247/authentication/authentication_controller.dart';
import 'package:delivery247/components/loading.dart';
import 'package:delivery247/services/constant.dart';
import 'package:device_info/device_info.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:get/get.dart';
import 'package:get_version/get_version.dart';
import 'package:http/http.dart' as http;

import 'dart:io';

import 'package:intl/intl.dart';

class HttpClient {
  String fcmToken = 'null';
  String maNguoiSD = 'giapnhgc@gmail.com';
  String loginToken = 'null';
  String maTvv = '';
  String maDtac = '';
  String version = '';
  String iosVersion = '';

  String deviceID = 'null';
  String platform;
  String date;
  String vercode;
  String pmCheck = '247Delivery';

  String signature;
  String authority;

  ReceivePort recievePort;
  Isolate isoLate;
  // SqliteModule sqliteModule;

  static HttpClient _instance = new HttpClient.internal();

  HttpClient.internal();
  factory HttpClient() => _instance;

  Isolate isolate;
  ReceivePort receivePort;

  final AuthenticationController _authenticationController = Get.find();
  void startIsolate() async {
    // sqliteModule = SqliteModule.instance;
    receivePort = new ReceivePort();
    isolate = await Isolate.spawn(entryPointSendPort, receivePort.sendPort);
    try {
      receivePort.listen(handleMessage);
    } catch (e) {
      print(e);
    }
  }

  static void entryPointSendPort(SendPort sendPort) async {}

  void handleMessage(dynamic data) async {
    ConnectivityResult networkResult =
        await (Connectivity().checkConnectivity());
    String networkText;
    switch (networkResult) {
      case ConnectivityResult.mobile:
        networkText = 'mobile';
        break;
      case ConnectivityResult.wifi:
        networkText = 'wifi';
        break;
      case ConnectivityResult.none:
      default:
        networkText = 'none';
        break;
    }
    var _log = {
      'body_request': data['body_request'],
      'body_response': data['body_response'],
      'network_type': networkText,
      'url_api': data['url_api'],
      'ngay_client': date,
      'muc_dich': 'save log'
    };

    // int logCount = await sqliteModule.countTraceLog();

    // if (logCount == 5) {
    //   List listLog = await sqliteModule.selectTraceLog();

    //   var _listLog = [];
    //   for (var item in listLog) {
    //     _listLog.add(jsonDecode(item['log']));
    //   }
    //   debugPrint("LOG: " + _listLog.toString());
    // } else {
    //   sqliteModule.insertTraceLog(jsonEncode(_log));
    // }
  }

  void onLoadDevice() async {
    // startIsolate();
    // NotificationSettings settings =
    //     await FirebaseMessaging.instance.requestPermission(
    //   alert: true,
    //   announcement: false,
    //   badge: true,
    //   carPlay: false,
    //   criticalAlert: false,
    //   provisional: false,
    //   sound: true,
    // );

    // fcmToken = await FirebaseMessaging.instance.getToken();
    DeviceInfoPlugin deviceInfo = new DeviceInfoPlugin();
    if (Platform.isIOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      deviceID = iosDeviceInfo.identifierForVendor;
      iosVersion = iosDeviceInfo.systemVersion;
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      deviceID = androidDeviceInfo.androidId;
    }
    platform = Platform.isAndroid ? 'android' : 'ios';
    date = DateFormat('dd-MM-yyyy').format(new DateTime.now());
    vercode = await GetVersion.projectVersion;

    String signTemp = 'device_id=' +
        deviceID +
        '&platform=' +
        platform +
        '&date=' +
        date +
        '&pm_check=' +
        pmCheck +
        '&vercode=' +
        vercode;

    var utf8Temp = utf8.encode(signTemp);

    signature = sha256.convert(utf8Temp).toString();
    authority = maNguoiSD + '-' + loginToken + '-' + deviceID + '-' + fcmToken;
  }

  void setUser(data) async {
    // maNguoiSD = data['ma'];
    maTvv = data['ma_tvv'] != null ? data['ma_tvv'] : '';
    // maDtac =  data['dvi'] != null ? data['dvi'] : '';
  }

  Future<dynamic> getMethod(
      BuildContext context, TickerProvider ticker, String url,
      {Map<String, String> headers}) async {
    BuildContext _loadingContext;

    try {
      if (context != null && ticker != null)
        showLoading(
          context,
          ticker,
          onCallBackContext: (loadingContext) =>
              _loadingContext = loadingContext,
        );

      var response = await http
          .get(Uri.parse(url), headers: headers)
          .timeout(Duration(seconds: REQUEST_TIME_OUT));
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception('Error while fetching data');
      }
      dynamic bodyJson = jsonDecode(res);
      if (_loadingContext != null) {
        // hideLoading(_loadingContext);
      }
      return bodyJson;
    } catch (e) {
      // if (_loadingContext != null) hideLoading(_loadingContext);
      return null;
    }
  }

  Future<dynamic> postMethod(
      BuildContext context, TickerProvider ticker, String url,
      {Map body, String function, String action}) async {
    BuildContext _loadingContext;

    Map<String, String> defaultHeader = {
      'Content-Type': 'application/json',
      'Signature': signature,
      'Authority': authority
    };
    Map<String, dynamic> defaultBody = {
      'device_id': deviceID,
      'platform': platform,
      'date': date,
      'pm_check': pmCheck,
      'vercode': vercode,
      'player_id': fcmToken,
      'ma_thiet_bi': deviceID,
      'action': action
    };

    body.forEach((key, value) {
      defaultBody[key] = value;
    });

    try {
      int requestTimeOut = REQUEST_TIME_OUT;

      if (context != null && ticker != null) {
        showLoading(
          context,
          ticker,
          onCallBackContext: (loadingContext) =>
              _loadingContext = loadingContext,
        );
      }

      print(jsonEncode(body));

      var response = await http
          .post(Uri.parse(url),
              headers: defaultHeader, body: jsonEncode(defaultBody))
          .timeout(Duration(seconds: requestTimeOut));

      final String res = response.body;

      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception('Error while fetching data');
      }

      dynamic bodyJson = jsonDecode(res);
      // if (_loadingContext != null) hideLoading(_loadingContext);

      // receivePort.sendPort.send({
      //   'body_request': body,
      //   'body_response': response.body,
      //   'url_api': url,
      // });

      return {
        'status': Status.SUCCESS,
        'data': bodyJson,
      };
    } catch (e) {
      // receivePort.sendPort.send({
      //   'body_request': body,
      //   'body_response': e,
      //   'url_api': url,
      // });

      // if (_loadingContext != null) hideLoading(_loadingContext);
      if (e is TimeoutException) {
        return {
          'status': Status.FAIL,
          'errorCode': ErrorCode.TIMEOUT,
          'message': 'mNetworkLow'.tr
        };
      } else {
        return {
          'status': Status.FAIL,
          'errorCode': ErrorCode.SYS_ERROR,
          'message': 'mApiError'.tr
        };
      }
    }
  }

  Future<dynamic> postMultipartMethod(
      BuildContext context, TickerProvider ticker, String url,
      {Map header, Map fields, Map files}) async {
    var request = http.MultipartRequest('POST', Uri.parse(url));
    BuildContext _loadingContext;

    try {
      if (context != null && ticker != null)
        showLoading(
          context,
          ticker,
          onCallBackContext: (loadingContext) =>
              _loadingContext = loadingContext,
        );
      Map<String, String> headers;

      if (header != null) {
        headers = header;
      } else {
        headers = {
          'Content-Type': 'multipart/form-data',
          'Accept': 'application/json'
        };
      }

      request.headers.addAll(headers);

      fields.forEach((key, value) {
        request.fields[key] = value;
      });

      files.forEach((key, value) async {
        ImageProperties properties =
            await FlutterNativeImage.getImageProperties(value);
        File compressedFile = await FlutterNativeImage.compressImage(value,
            quality: 80,
            targetWidth: 1000,
            targetHeight:
                (properties.height * 1000 / properties.width).round());

        request.files.add(http.MultipartFile.fromBytes(
            key, compressedFile.readAsBytesSync(),
            filename: value.split('/').last));
      });

      final response =
          await request.send().timeout(Duration(seconds: REQUEST_TIME_OUT));
      final respStr = await response.stream.bytesToString();

      // if (_loadingContext != null) hideLoading(_loadingContext);

      if (respStr != '{}') {
        return {
          'status': Status.SUCCESS,
          'data': jsonDecode(respStr),
        };
      } else {
        return {
          'status': Status.FAIL,
          'errorCode': ErrorCode.NO_DATA,
          'message': 'mWrongPhoto'.tr
        };
      }
    } catch (e) {
      // if (_loadingContext != null) hideLoading(_loadingContext);
      if (e is TimeoutException) {
        return {
          'status': Status.FAIL,
          'errorCode': ErrorCode.TIMEOUT,
          'message': 'mLoadFail'.tr
        };
      } else {
        return {
          'status': Status.FAIL,
          'errorCode': ErrorCode.SYS_ERROR,
          'message': 'mApiError'.tr
        };
      }
    }
  }
}
