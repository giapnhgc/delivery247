import 'dart:convert';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:delivery247/components/loading/loading_controller.dart';
import 'package:delivery247/components/loading/loading_state.dart';
import 'package:delivery247/services/http/api_url.dart';
import 'package:device_info/device_info.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:get_version/get_version.dart';
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';

class GetXHttpProvider extends GetConnect {
  final LoadingController _loadingController = Get.find();

  final Duration timeout = Duration(seconds: 15);

  Map<String, String> headerSignature = {};

  String deviceID = '';
  String platform = '';
  String date = DateFormat('yyyy-MM-dd').format(DateTime.now());
  String pm = '247Delivery';
  String vercode = 'vercode';
  String iosVersion = '';

  String fcmToken = 'null';

  @override
  void onInit() async {
    // fcmToken = await FirebaseMessaging.instance.getToken();
    DeviceInfoPlugin deviceInfo = new DeviceInfoPlugin();
    if (Platform.isIOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      deviceID = iosDeviceInfo.identifierForVendor;
      iosVersion = iosDeviceInfo.systemVersion;
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      deviceID = androidDeviceInfo.androidId;
    }
    platform = Platform.isAndroid ? 'android' : 'ios';
    date = DateFormat('dd-MM-yyyy').format(new DateTime.now());
    vercode = await GetVersion.projectVersion;

    String signTemp =
        'device_id=$deviceID&platform=$platform&date=$date&pm_check=$pm&vercode=$vercode';
    var utf8Temp = utf8.encode(signTemp);
    String signature = sha256.convert(utf8Temp).toString();
    String authority = 'a@gmail.com-$deviceID-a@gmail.com';
    headerSignature['Authority'] = authority;
    headerSignature['Signature'] = signature;
    headerSignature['Content-Type'] = 'application/json';
  }

//1
  Future<dynamic> productCategory() async {
    Response response =
        await get(url + PRODUCT_CATEGORY, headers: headerSignature);
    return handleResponse(response);
  }

//2
  Future<dynamic> bannerList() async {
    Response response = await get(url + BANNER_LIST, headers: headerSignature);
    return handleResponse(response);
  }

//3
  Future<dynamic> promotion(int pageIndex, int pageSize) async {
    var params = '?pageIndex=' +
        pageIndex.toString() +
        '&pageSize=' +
        pageSize.toString();
    Response response =
        await get(url + PROMOTION + params, headers: headerSignature);
    return handleResponse(response);
  }

  //4
  Future<dynamic> productListByGroupCategory() async {
    Response response = await get(url + PRODUCT_LIST_BY_GROUP_CATEGORY,
        headers: headerSignature);
    return handleResponse(response);
  }

//5
  Future<dynamic> productListByCategory(
      String catId, int pageIndex, int pageSize) async {
    var params = '?category_id=' +
        catId +
        '&pageIndex=' +
        pageIndex.toString() +
        '&pageSize=' +
        pageSize.toString();
    Response response = await get(url + PRODUCT_LIST_BY_CATEGORY + params,
        headers: headerSignature);
    return handleResponse(response);
  }

//6
  Future<dynamic> productDetailsz(String id) async {
    var params = '?id=' + id;
    Response response =
        await get(url + PRODUCT_DETAILS + params, headers: headerSignature);
    return handleResponse(response);
  }

//7
  Future<dynamic> getTinhThanh() async {
    Response response = await get(url + TINH_THANH, headers: headerSignature);
    return handleResponse(response);
  }

//8
  Future<dynamic> getQuanHuyen(String id) async {
    var params = '?province_id=' + id;
    Response response =
        await get(url + QUAN_HUYEN + params, headers: headerSignature);
    return handleResponse(response);
  }

//9
  Future<dynamic> getXaPhuong(String id) async {
    var params = '?district_id=' + id;
    Response response =
        await get(url + XA_PHUONG + params, headers: headerSignature);
    return handleResponse(response);
  }

  // 10
  Future<dynamic> login(body) async {
    Response response = await post(url + LOGIN, body, headers: headerSignature);
    return handleResponse(response);
  }

  // 11
  Future<dynamic> register(body) async {
    Response response =
        await post(url + RESGISTER, body, headers: headerSignature);
    return handleResponse(response);
  }

  //12
  Future<dynamic> getListAddress(String userId) async {
    var params = '?id=' + userId;
    Response response =
        await get(url + ADDRESS + params, headers: headerSignature);
    return handleResponse(response);
  }

  Future<dynamic> productStatus(String userId) async {
    // var params = '?id=389472695990882304';
    var params = '?id=' + userId;
    Response response =
        await get(url + PRODUCT_STATUS + params, headers: headerSignature);
    return handleResponse(response);
  }

  // 13
  Future<dynamic> addAddress(body) async {
    Response response =
        await post(url + ADD_ADDRESS, body, headers: headerSignature);
    return handleResponse(response);
  }

  // 14
  Future<dynamic> addOrders(body) async {
    Response response =
        await post(url + ADD_ORDERS, body, headers: headerSignature);
    return handleResponse(response);
  }

  //Xử lý response
  handleResponse(Response response, {bool hideLoading = true}) async {
    if (_loadingController.state is Loading && hideLoading) {
      await _loadingController.hideLoading();
      Get.back();
    }

    switch (response.statusCode) {
      case 200:
        return response.body;
      case 401:
      case 404:
        Toast.show('Lỗi truyền dữ liệu', Get.context);
        await Future.delayed(Duration(seconds: 2));
        return null;
      case 500:
        Toast.show('Không thể kết nối đến server', Get.context);
        await Future.delayed(Duration(seconds: 2));
        return null;
      default:
        if (response.statusText.contains('TimeoutException')) {
          Toast.show(
              'Kết nối tới server quá lâu, vui lòng thử lại', Get.context);
          await Future.delayed(Duration(seconds: 2));
        }
        return null;
    }
  }
}
