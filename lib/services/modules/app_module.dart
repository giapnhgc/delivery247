import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:delivery247/components/text_custom.dart';
import 'package:delivery247/models/hex_color.dart';
import 'package:delivery247/services/app_theme.dart';
import 'package:delivery247/services/constant.dart';
import 'package:delivery247/services/share_preferent_module.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:photo_view/photo_view.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:flutter_svg/svg.dart';

showYesNoDialog(
  BuildContext context, {
  @required Widget child,
  Function onSubmit,
  String confirmText = 'Có',
  String declineText = 'Không',
  bool barrierDismissible = true,
}) {
  showDialog(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (BuildContext dialogContext) {
      return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Container(
                    width: 296,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.all(16),
                    child: Column(
                      children: [
                        child,
                        SizedBox(
                          height: 16,
                        ),
                        Container(
                          height: 48,
                          child: TextButton(
                            style: AppTheme.textButtonPrimary,
                            child: TextCustom(
                              title: confirmText,
                              style: AppTheme.textBtnLight,
                            ),
                            onPressed: () {
                              onSubmit(1);
                              Navigator.pop(dialogContext);
                            },
                          ),
                          width: double.infinity,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 48,
                          child: TextButton(
                            style: AppTheme.textButtonSecondary,
                            child: TextCustom(
                              title: declineText,
                              style: AppTheme.textBtnYellow,
                            ),
                            onPressed: () => Navigator.pop(dialogContext),
                          ),
                          width: double.infinity,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}

showConfirmPaymentDialog(
  BuildContext context, {
  Function onSubmit,
  String urlDieuKhoan,
}) {
  showDialog(
    context: context,
    builder: (BuildContext dialogContext) {
      return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Container(
                    width: 296,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.all(16),
                    child: Column(
                      children: [
                        Container(
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/chia_se.png',
                                fit: BoxFit.contain,
                              ),
                              Container(
                                child: Column(
                                  children: [
                                    InkWell(
                                      child: RichText(
                                        text: TextSpan(children: <TextSpan>[
                                          TextSpan(
                                            text: 'Cam ket',
                                            style: AppTheme.textStyle,
                                          ),
                                          TextSpan(
                                            text: 'Dieu khoan 247 Delivery',
                                            style: TextStyle(
                                              color: AppTheme.colorPrimary,
                                              fontSize: 16,
                                            ),
                                          ),
                                        ]),
                                      ),
                                      onTap: () {
                                        openFilePdf(urlDieuKhoan);
                                      },
                                    ),
                                    SizedBox(height: 4),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Container(
                          height: 48,
                          child: TextButton(
                            style: AppTheme.textButtonPrimary,
                            child: TextCustom(
                              title: 'Có',
                              style: AppTheme.textBtnLight,
                            ),
                            onPressed: () {
                              Navigator.pop(dialogContext);
                              onSubmit(1);
                            },
                          ),
                          width: double.infinity,
                        ),
                        SizedBox(height: 8),
                        Container(
                          height: 48,
                          child: TextButton(
                            style: AppTheme.textButtonSecondary,
                            child: TextCustom(
                              title: 'Không',
                              style: AppTheme.textBtnBlue,
                            ),
                            onPressed: () => Navigator.pop(dialogContext),
                          ),
                          width: double.infinity,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}

showConfirmCartDialog(
  BuildContext context, {
  Function onSubmit,
}) {
  showDialog(
    context: context,
    builder: (BuildContext dialogContext) {
      return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Container(
                    width: 296,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.all(16),
                    child: Column(
                      children: [
                        Container(
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/chia_se.png',
                                fit: BoxFit.contain,
                              ),
                              Container(
                                child: Column(
                                  children: [
                                    TextCustom(
                                      title:
                                          'Bạn có chắc chắn muốn thanh toán đơn hàng này không?',
                                      style: AppTheme.textStyle,
                                    ),
                                    SizedBox(height: 4),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 16),
                        Container(
                          height: 48,
                          child: TextButton(
                            style: AppTheme.textButtonPrimary,
                            child: TextCustom(
                              title: 'Có',
                              style: AppTheme.textBtnLight,
                            ),
                            onPressed: () {
                              Navigator.pop(dialogContext);
                              onSubmit();
                            },
                          ),
                          width: double.infinity,
                        ),
                        SizedBox(height: 8),
                        Container(
                          height: 48,
                          child: TextButton(
                            style: AppTheme.textButtonSecondary,
                            child: TextCustom(
                              title: 'Không',
                              style: AppTheme.textBtnBlue,
                            ),
                            onPressed: () => Navigator.pop(dialogContext),
                          ),
                          width: double.infinity,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}

showVersionInfoDialog({BuildContext context, String version}) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Container(
          child: Image.asset('assets/images/done.png'),
        ),
        content: IntrinsicHeight(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              TextCustom(
                title: 'Phien ban: $version',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
              TextCustom(
                title: '© Bản quyền thuộc về 247 Delivery',
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
        actions: [
          TextButton(
            child: TextCustom(
              title: "Đóng",
              style: AppTheme.textBtnDanger,
            ),
            onPressed: () async {
              Navigator.pop(context, null);
            },
          ),
        ],
      );
    },
  );
}

showPhotoDialog({
  BuildContext context,
  String rootLink,
  ImageType type,
  bool showButton = false,
  Function onButtonPress,
  String buttonText = '',
  Function onDelete,
}) {
  ImageProvider<Object> image;
  switch (type) {
    case ImageType.base64:
      Uint8List bytes = base64.decode(rootLink);
      image = MemoryImage(bytes);
      break;
    case ImageType.file:
      image = FileImage(File(rootLink));
      break;
    case ImageType.network:
      image = NetworkImage(rootLink);
      break;
    case ImageType.asset:
      image = AssetImage(rootLink);
      break;
    default:
      break;
  }
  showDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return Container(
        padding: EdgeInsets.symmetric(
          horizontal: 40,
          vertical: 105,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Stack(
            children: [
              PhotoView(
                imageProvider: image,
              ),
              Positioned(
                top: 0,
                left: -10,
                child: TextButton(
                  child: SvgPicture.asset(
                    'assets/svgs/icon_close.svg',
                    fit: BoxFit.contain,
                    color: Colors.white,
                    height: 12,
                    width: 12,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              showButton
                  ? Positioned(
                      bottom: 16,
                      left: 16,
                      right: 16,
                      child: Container(
                        height: 48,
                        child: TextButton(
                          style: AppTheme.textButtonPrimary,
                          child: TextCustom(
                            title: buttonText,
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                            color: Colors.white,
                          ),
                          onPressed: onButtonPress,
                        ),
                      ),
                    )
                  : SizedBox(),
            ],
          ),
        ),
      );
    },
  );
}

showConfirmDialog(
    {BuildContext context,
    Function onTapYes,
    Function onTapNo,
    String confirmText,
    String declineText,
    double width,
    bool barrierDismissible = true,
    bool showNoButton = true,
    Widget content}) {
  showDialog(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (BuildContext dialogContext) {
      return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
            child: SingleChildScrollView(
              child: Container(
                width: width ?? 296,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.white,
                ),
                padding: EdgeInsets.all(16),
                child: Column(
                  children: [
                    content,
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      height: 48,
                      child: TextButton(
                          style: AppTheme.textButtonPrimary,
                          child: TextCustom(
                            title: confirmText ?? 'Có',
                            style: AppTheme.textBtnLight,
                          ),
                          onPressed: () {
                            Navigator.pop(dialogContext);
                            if (onTapYes != null) onTapYes();
                          }),
                      width: double.infinity,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    showNoButton
                        ? Container(
                            height: 48,
                            child: TextButton(
                              style: AppTheme.textButtonSecondary,
                              child: TextCustom(
                                title: declineText ?? 'Không',
                                style: AppTheme.textBtnBlue,
                              ),
                              onPressed: () {
                                Navigator.pop(dialogContext);
                                if (onTapNo != null) onTapNo();
                              },
                            ),
                            width: double.infinity,
                          )
                        : SizedBox()
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    },
  );
}

showActionSheetBar(
    {BuildContext context, List data, String label, Function onSubmit}) {
  showModalBottomSheet<void>(
    isScrollControlled: true,
    isDismissible: true,
    context: context,
    builder: (BuildContext barContext) {
      //mỗi item của list là 60px, nếu max height thì sẽ - đi 60px
      double _height = (data.length.toDouble() * 60) + 50;
      if (_height > (MediaQuery.of(context).size.height - 60)) {
        _height = MediaQuery.of(context).size.height - 60;
      }
      return SafeArea(
          bottom: true,
          child: Container(
            height: _height,
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              ),
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Container(
                      height: 52,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            width: 1,
                            color: AppTheme.colorDisable,
                          ),
                        ),
                      ),
                      alignment: Alignment.center,
                      child: TextCustom(
                        title: label,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Colors.grey,
                      ),
                    ),
                    Expanded(
                      child: ListView.builder(
                        itemCount: data.length,
                        itemBuilder: (BuildContext _, int index) {
                          return Container(
                            height: 56,
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            child: InkWell(
                              onTap: () {
                                Navigator.pop(barContext);
                                onSubmit(data[index]);
                              },
                              child: Container(
                                height: double.infinity,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  border: Border(
                                    bottom: BorderSide(
                                        color: HexColor('#E7E7E7'), width: 1),
                                  ),
                                ),
                                child: TextCustom(
                                  title: data[index]['title'],
                                  color: AppTheme.colorText,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ));
    },
  );
}

showModal(BuildContext context,
    {Widget child, Function onSubmit, isDismiss = false}) {
  showModalBottomSheet<void>(
    isScrollControlled: true,
    isDismissible: isDismiss,
    context: context,
    builder: (BuildContext modalContext) {
      return SafeArea(
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
          child: child,
        ),
      );
    },
  );
}

double tinhKhoangCach(mLocation, location) {
  if (location['lat'] != null) {
    double lat;
    double lng;
    if (location['lat'] is String) {
      lat = double.parse(location['lat']);
    } else {
      lat = location['lat'];
    }
    if (location['lng'] is String) {
      lng = double.parse(location['lng']);
    } else {
      lng = location['lng'];
    }
    double R = 6371;
    double dLat = (lat - mLocation.latitude) * (pi / 180);
    double dLon = (lng - mLocation.longitude) * (pi / 180);
    double la1ToRad = mLocation.latitude * (pi / 180);
    double la2ToRad = lat * (pi / 180);
    double a = sin(dLat / 2) * sin(dLat / 2) +
        cos(la1ToRad) * cos(la2ToRad) * sin(dLon / 2) * sin(dLon / 2);
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));
    double d = R * c;
    return double.parse((d).toStringAsFixed(1));
  } else
    return null;
}

tinhTuoi(birth) {
  int presentYear = DateTime.now().year;
  int relationYear;
  if (birth is DateTime)
    relationYear = birth.year;
  else
    relationYear = DateFormat('dd/MM/yyyy').parse(birth).year;

  return presentYear - relationYear;
}

String convertJobID(soID) {
  var soIDStr = '';
  if (soID is int)
    soIDStr = soID.toString();
  else if (soID is double)
    soIDStr = soID.toInt().toString();
  else
    soIDStr = soID;
  var phan1 = soIDStr.substring(2, 8);
  var phan2 = int.parse(soIDStr.substring(8, soIDStr.length));
  return phan1 + phan2.toString();
}

// Hàm đổi số kiểu (20201220) sang (21-12-2020)
String doubleToDateYYYYMMDD(value) {
  String _value;
  if (value is double)
    _value = value.toString();
  else
    _value = value;

  String yyyy = _value.substring(0, 4);
  String mm = _value.substring(4, 6);
  String dd = _value.substring(6, 8);
  return dd + '/' + mm + '/' + yyyy;
}

// Hàm đổi số kiểu (21122020) sang (21-12-2020)
String doubleToDateDDMMYYYY(value) {
  String _value;
  if (value is double)
    _value = value.toString();
  else
    _value = value;

  String dd = _value.substring(0, 2);
  String mm = _value.substring(2, 4);
  String yyyy = _value.substring(4, 8);
  return dd + '/' + mm + '/' + yyyy;
}

openFilePdf(url) async {
  if (await canLaunch(url))
    await launch(url);
  else
    throw "Could not launch $url";
}

bool checkStringNull(value) {
  return value != null && value != '';
}

GridType detectGridType(BuildContext context) {
  MediaQueryData mediaQueryData = MediaQuery.of(context);
  double width = mediaQueryData.size.width;

  if (width < 576) {
    return GridType.xs;
  } else if (width < 768) {
    return GridType.sm;
  } else if (width < 992) {
    return GridType.md;
  } else if (width < 1200) {
    return GridType.lg;
  } else {
    // width >= 1200
    return GridType.xl;
  }
}

pushPage(context, page, {routePage, onPop, animation = true}) async {
  var _pop;
  if (animation) {
    _pop = await Navigator.push(
      context,
      PageRouteBuilder(
          pageBuilder: (_, __, ___) => page,
          transitionDuration: Duration(milliseconds: 200), // thêm delay times
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return FadeTransition(
              opacity: animation,
              child: child,
            );
          },
          settings: RouteSettings(
            name: routePage,
            arguments: Map(),
          )),
    );
  } else {
    _pop = await Navigator.push(
      context,
      PageRouteBuilder(
          pageBuilder: (_, __, ___) => page,
          transitionDuration: Duration(seconds: 0),
          settings: RouteSettings(
            name: routePage,
            arguments: Map(),
          )),
    );
  }

  if (onPop != null) onPop(_pop);
}

//** Đổi kiểu ngày tháng dạng double sang DateTime */
DateTime convertDateTypeDouble(double value) {
  String valueString = value.toString();
  if (valueString.length > 8) {
    String yyyy = valueString.substring(0, 4);
    String mm = valueString.substring(5, 6);
    String dd = valueString.substring(7, 8);
    return DateFormat('dd/MM/yyyy').parse(dd + '/' + mm + '/' + yyyy);
  } else
    return null;
}

getDateTimeNow() {
  return DateFormat('dd/MM/yyyy').format(DateTime.now());
}

getDateBeforeFromToday(dateNumber) {
  return DateFormat('dd/MM/yyyy')
      .format(DateTime.now().subtract(Duration(days: dateNumber)));
}

getDateAfterFromToday(dateNumber) {
  return DateFormat('dd/MM/yyyy')
      .format(DateTime.now().add(Duration(days: dateNumber)));
}

/* Check null các ô input, chuyền vào mảng các value,
* true - là tất cả mảng đều k null và rỗng
* false - là chỉ cần 1 value null hoặc rỗng
*/

//check cmt, phone, email nếu nhập thì check validate, nếu k nhập thì pass bình thường
bool checkValidate(cmt, phone, email) {
  bool result = true;
  if (cmt != '') {
    result = !checkValidateCmt(cmt);
    if (!result) return result;
  }
  if (phone != '') {
    result = !checkValidatePhone(phone);
    if (!result) return result;
  }
  if (email != '') {
    result = !checkValidateEmail(email);
    if (!result) return result;
  }
  return result;
}

bool checkNullOrUndifine(arrayValue) {
  for (var item in arrayValue) {
    if (item is DateTime) {
      if (item == null) return false;
    }
    if (item is String) {
      if (item == null || item.trim() == '') {
        return false;
      }
    } else {
      if (item == null) return false;
    }
  }
  return true;
}

/* Hàm trả về rỗng nếu thuộc tính bị null hoặc undefine */
String getNullValue(value) {
  if (value == null)
    return '';
  else
    return value;
}

/* Validate phone number*/
validateFormData(String data, ValidateType type, {String fieldValidate}) {
  switch (type) {
    case ValidateType.textInfo:
      if (data.trim().length == 0)
        throw ({"message": 'Vui lòng nhập ' + fieldValidate + '!'});
      break;
    case ValidateType.phone:
      if (data.trim().length == 0)
        throw ({"message": 'Vui lòng nhập số điện thoại!'});
      if (data.trim().length < 9 || data.trim().length > 12)
        throw ({"message": 'Vui lòng nhập chính xác số điện thoại!'});
      break;
    case ValidateType.id:
      if (data.trim().length == 0)
        throw ({"message": 'Vui lòng nhập số chứng minh thư!'});
      if (data.trim().length < 8 || data.trim().length > 12)
        throw ({"message": 'Vui lòng nhập chính xác số chứng minh thư!'});
      break;
    case ValidateType.email:
      if (data.trim().length == 0) throw ({"message": 'Vui lòng nhập email!'});
      bool emailValid = RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(data);
      if (!emailValid)
        throw ({"message": 'Vui lòng nhập chính xác địa chỉ email!'});
      break;
    case ValidateType.passwordOld:
      if (data.trim().length == 0)
        throw ({"message": 'Vui lòng nhập mật khẩu cũ!'});
      break;
    case ValidateType.password:
      if (data.trim().length == 0)
        throw ({"message": 'Vui lòng nhập mật khẩu!'});
      bool passValid =
          RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$').hasMatch(data);
      if (!passValid)
        throw ({
          "message":
              'Mật khẩu cần tối thiểu 6 ký tự, gồm ít nhất 1 số, 1 chữ cái viết thường và 1 chữ cái viết hoa!'
        });
      break;
    case ValidateType.rePassword:
      if (data.trim().length == 0)
        throw ({"message": 'Vui lòng nhập mật khẩu xác thực!'});
      bool passValid =
          RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$').hasMatch(data);
      if (!passValid)
        throw ({
          "message":
              'Mật khẩu xác thực cần tối thiểu 6 ký tự, gồm ít nhất 1 số, 1 chữ cái viết thường và 1 chữ cái viết hoa!'
        });
      break;
    default:
      break;
  }
}

//kiểm tra điều kiện nhập cmt 8 đến 13 ký tự
bool checkValidateCmt(value) {
  String pattern = r'^(?:[+0]9)?[a-zA-Z0-9\.\-\/]{9,13}$';
  RegExp regExp = new RegExp(pattern);

  return !regExp.hasMatch(value);
}

//kiểm tra điều kiện nhập mã số thuế 10 đến 14 ký tự
bool checkValidateMst(value) {
  String pattern = r'^(?:[+0]9)?[0-9\.\-\/]{10,14}$';
  RegExp regExp = new RegExp(pattern);

  return !regExp.hasMatch(value);
}

//kiểm tra điều kiện nhập số điện thoại 10 đến 11 ký tự
bool checkValidatePhone(value) {
  String pattern = r'^(?:[+0]9)?[0-9]{10,11}$';
  RegExp regExp = new RegExp(pattern);

  return !regExp.hasMatch(value);
}

//kiểm tra điều kiện nhập email
bool checkValidateEmail(value) {
  String pattern =
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
  RegExp regExp = new RegExp(pattern);

  return !regExp.hasMatch(value);
}

//input chỉ được nhập số
TextInputFormatter allowFillInt() {
  return FilteringTextInputFormatter.allow(RegExp(r'[0-9]'));
}

//input chỉ được nhập số thập phân
TextInputFormatter allowFillDouble() {
  return FilteringTextInputFormatter.allow(RegExp(r'^(\d+)?\.?\d*'));
}

//input chỉ nhập số, chữ k có ký tự đặc biệt
TextInputFormatter allowFillNormalChar() {
  return FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9 ]'));
}

//input chỉ nhập số, chữ k có ký tự đặc biệt
TextInputFormatter allowFillEmail() {
  return FilteringTextInputFormatter.allow(RegExp(r'^(\n*)?\@?\w*.?\w*.?\w*'));
}

getLocation() async {
  try {
    var locationStore = await SPreferentModule().getItem(StorageKey.LOCATION);
    if (locationStore == null) {
      Map locationStore = {
        'latitude': '21.0345665',
        'longitude': '105.827421',
      };
      Location location = new Location();
      bool _serviceEnabled = false;
      PermissionStatus _permissionGranted;
      LocationData _locationData;
      bool acceptPermission = true;
      _serviceEnabled = await location.serviceEnabled();
      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {
          acceptPermission = false;
        }
      }
      _permissionGranted = await location.hasPermission();
      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();
        if (_permissionGranted != PermissionStatus.granted) {
          acceptPermission = false;
        }
      }
      if (acceptPermission) {
        _locationData = await location.getLocation();
        locationStore = {
          'latitude': _locationData.latitude.toString(),
          'longitude': _locationData.longitude.toString(),
        };
        SPreferentModule().setItem(StorageKey.LOCATION, locationStore);
      }

      return locationStore;
    }
    return locationStore;
  } catch (e) {
    print(e);
  }
}

ceilUpthousand(double number) {
  return number;
}

ceilUpPercent(double number) {
  return ((number * 10).ceil() / 10).toDouble();
}

String generateRandomString(int len) {
  var r = Random();
  const _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  return List.generate(len, (index) => _chars[r.nextInt(_chars.length)]).join();
}

/// Hàm parse chuỗi base64 thành file và trả về path
Future<String> generateAndGetPath(String _base64,
    {String fileType = '.pdf'}) async {
  final path = join(
    (await getTemporaryDirectory()).path,
    '${DateTime.now().toUtc().millisecondsSinceEpoch}.pdf',
  );
  if (_base64 != null) {
    Uint8List bytes = base64.decode(_base64);
    final file = File(path);
    file.writeAsBytesSync(bytes);
  }

  return path;
}

/// Hàm trả về path
Future<String> generatePath() async {
  final path = join(
    (await getTemporaryDirectory()).path,
    '${DateTime.now().toUtc().millisecondsSinceEpoch}.jpg',
  );
  return path;
}

///Hàm format và parse number
String formatCurrency(number) {
  String _format =
      NumberFormat.currency(locale: 'vi_VN', symbol: 'đ').format(number);
  return _format.replaceAll('.', ',');
}

String formatNumber(number) {
  String _format = NumberFormat().format(number);
  return _format.replaceAll('.', ',');
}

double parseCurrency(value) {
  var _value = value.replaceAll(',', '.').replaceAll('đ', '');
  try {
    double result = NumberFormat().parse(_value.trim());
    return result;
  } catch (e) {
    return 0.0;
  }
}

Map parseQrCodeToObj(value) {
  var _listObj = value.split('|');
  if (_listObj.length == 7) {
    try {
      var _obj = {
        'name': _listObj[2],
        'id': _listObj[0],
        'birth': doubleToDateDDMMYYYY(_listObj[3]),
        'address': _listObj[5],
        'gender': _listObj[4],
        'timeCreate': doubleToDateDDMMYYYY(_listObj[6])
      };
      return _obj;
    } catch (e) {
      return null;
    }
  } else
    return null;
}

String dateConvertFromStringYYYYMMDD(value) {
  //String value input có dinh dang yyyymmdd sẽ convert ra String định dang dd/mm/yyyy
  if (value != null) {
    String year = value.substring(0, 4);
    String month = value.substring(4, 6);
    String day = value.substring(6, 8);
    return day + '/' + month + '/' + year;
  } else
    return '';
}

Future<void> makePhoneCall(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

showToastCus(String msg) {
  return ScaffoldMessenger.of(Get.context).showSnackBar(SnackBar(
    content: Text(msg),
  ));
}

countCartNumber() async {
  var cartProducts = await SPreferentModule().getItem(StorageKey.CART_PROTDUCT);
  if (checkNullOrUndifine([cartProducts])) {
    return cartProducts.length;
  } else {
    return 0;
  }
  // int cartNumber = 0;
  // for (var i = 0; i < cartProducts.length; i++) {
  //   // Quantity
  //   cartNumber += cartProducts[i]['Quantity'];
  // }
  // print(cartProducts.length);
  // return cartNumber;
}

addToCart(Map listProductDetails) async {
  List cartProducts =
      await SPreferentModule().getItem(StorageKey.CART_PROTDUCT);
  bool isMatchProduct = false;
  if (checkNullOrUndifine([cartProducts])) {
    for (var i = 0; i < cartProducts.length; i++) {
      if (cartProducts[i]['ProductId'] == listProductDetails['Id']) {
        cartProducts[i]['Quantity']++;
        isMatchProduct = true;
      }
    }
  } else {
    cartProducts = [];
  }

  if (!isMatchProduct) {
    Map product = {
      "ProductId": listProductDetails['Id'],
      "ProductName": listProductDetails['Name'],
      "Quantity": 1,
      "Price": listProductDetails['Price'],
      "ImageLink": listProductDetails['Avatar'],
    };
    cartProducts.add(product);
  }

  SPreferentModule().setItem(StorageKey.CART_PROTDUCT, cartProducts);
  // cartInfo.setCartProduct(product);
  showToastCus('Đã thêm vào giỏ hàng');

  return await countCartNumber();
}


backToHome(Widget page) {
  Get.offUntil(
    GetPageRoute(page: () => page),
    (route) {
      if (route is GetPageRoute) return route.routeName == '/MyTabView';
      return false;
    },
  );
}
