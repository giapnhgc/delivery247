import 'package:delivery247/models/hex_color.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui show Shadow, FontFeature;

@immutable
class MyTextStyle extends TextStyle {
  MyTextStyle bold() {
    return copyWith1(fontWeight: FontWeight.w700);
  }

  MyTextStyle medium() {
    return copyWith1(fontWeight: FontWeight.w500);
  }

  MyTextStyle white() {
    return copyWith1(
      color: AppTheme.colorWhite,
    );
  }

  MyTextStyle black() {
    return copyWith1(
      color: AppTheme.colorBlack,
    );
  }

  MyTextStyle grey() {
    return copyWith1(
      color: AppTheme.colorGrey,
    );
  }

  MyTextStyle lightGrey() {
    return copyWith1(color: AppTheme.colorBorderDisable);
  }

  MyTextStyle red() {
    return copyWith1(
      color: AppTheme.colorRed,
    );
  }

  MyTextStyle primary() {
    return copyWith1(
      color: AppTheme.colorPrimary,
    );
  }

  MyTextStyle primary500() {
    return copyWith1(
      color: AppTheme.colorBlueDark,
    );
  }

  MyTextStyle secondary() {
    return copyWith1(
      color: AppTheme.colorSecondary,
    );
  }

  MyTextStyle copyWith1({
    bool inherit,
    Color color,
    Color backgroundColor,
    String fontFamily,
    List<String> fontFamilyFallback,
    double fontSize,
    FontWeight fontWeight,
    FontStyle fontStyle,
    double letterSpacing,
    double wordSpacing,
    TextBaseline textBaseline,
    double height,
    Locale locale,
    Paint foreground,
    Paint background,
    List<ui.Shadow> shadows,
    List<ui.FontFeature> fontFeatures,
    TextDecoration decoration,
    Color decorationColor,
    TextDecorationStyle decorationStyle,
    double decorationThickness,
    String debugLabel,
  }) {
    assert(color == null || foreground == null);
    assert(backgroundColor == null || background == null);
    String newDebugLabel;
    assert(() {
      if (this.debugLabel != null)
        newDebugLabel = debugLabel ?? '(${this.debugLabel}).copyWith1';
      return true;
    }());
    return MyTextStyle(
      inherit: inherit ?? this.inherit,
      color: this.foreground == null && foreground == null
          ? color ?? this.color
          : null,
      backgroundColor: this.background == null && background == null
          ? backgroundColor ?? this.backgroundColor
          : null,
      fontFamily: fontFamily ?? this.fontFamily,
      fontFamilyFallback: fontFamilyFallback ?? this.fontFamilyFallback,
      fontSize: fontSize ?? this.fontSize,
      fontWeight: fontWeight ?? this.fontWeight,
      fontStyle: fontStyle ?? this.fontStyle,
      letterSpacing: letterSpacing ?? this.letterSpacing,
      wordSpacing: wordSpacing ?? this.wordSpacing,
      textBaseline: textBaseline ?? this.textBaseline,
      height: height ?? this.height,
      locale: locale ?? this.locale,
      foreground: foreground ?? this.foreground,
      background: background ?? this.background,
      shadows: shadows ?? this.shadows,
      fontFeatures: fontFeatures ?? this.fontFeatures,
      decoration: decoration ?? this.decoration,
      decorationColor: decorationColor ?? this.decorationColor,
      decorationStyle: decorationStyle ?? this.decorationStyle,
      decorationThickness: decorationThickness ?? this.decorationThickness,
      debugLabel: newDebugLabel,
    );
  }

  MyTextStyle({
    inherit = true,
    fontSize,
    color,
    fontWeight,
    backgroundColor,
    fontFamily,
    fontFamilyFallback,
    fontStyle,
    letterSpacing,
    wordSpacing,
    textBaseline,
    height,
    locale,
    foreground,
    background,
    shadows,
    fontFeatures,
    decoration,
    decorationColor,
    decorationStyle,
    decorationThickness,
    debugLabel,
  }) : super(
          inherit: inherit,
          fontSize: fontSize,
          color: color,
          fontWeight: fontWeight,
          backgroundColor: backgroundColor,
          fontFamily: fontFamily,
          fontFamilyFallback: fontFamilyFallback,
          fontStyle: fontStyle,
          letterSpacing: letterSpacing,
          wordSpacing: wordSpacing,
          textBaseline: textBaseline,
          height: height,
          locale: locale,
          foreground: foreground,
          background: background,
          shadows: shadows,
          fontFeatures: fontFeatures,
          decoration: decoration,
          decorationColor: decorationColor,
          decorationStyle: decorationStyle,
          decorationThickness: decorationThickness,
          debugLabel: debugLabel,
        );
}

class AppTheme {
  AppTheme._();

  static const Color notWhite = Color(0xFFEDF0F2);
  static const Color nearlyWhite = Color(0xFFFEFEFE);
  static const Color white = Color(0xFFFFFFFF);
  static const Color nearlyBlack = Color(0xFF213333);
  static const Color grey = Color(0xFF3A5160);
  static const Color dark_grey = Color(0xFF313A44);

  static const Color darkText = Color(0xFF253840);
  static const Color darkerText = Color(0xFF17262A);
  static const Color lightText = Color(0xFF4A6572);
  static const Color deactivatedText = Color(0xFF767676);
  static const Color dismissibleBackground = Color(0xFF364A54);
  static const Color chipBackground = Color(0xFFEEF1F3);
  static const Color spacer = Color(0xFFF2F2F2);
  static const Color tileBorderColor = Color(0xFFF4F5F6);

  static const String fontName = 'WorkSans';

  static final Color borderLineColor = HexColor('#DBE0E6');
  static final Color backgroundColor = HexColor('#F4F5F6');

  static final Color colorPalePinkLight = HexColor('#FDEBEA');
  static final Color colorGreenLight = HexColor('#E6FDFA');
  static final Color colorWhite = HexColor('#FFFFFF');
  static final Color colorBlack = HexColor('#000000');
  static final Color colorOrange = HexColor('#FF6D00');
  static final Color colorBlue = HexColor('#0B7EFB');
  static final Color colorYellow = HexColor('#FDC224');
  static final Color colorSuccess = HexColor('#3ECC72');
  static final Color colorDanger = HexColor('#E53935');
  static final Color colorBlueLight = HexColor('#E6F2FF');
  static final Color colorBlueDark = HexColor('#3382BF');
  static final Color colorDisable = HexColor('#E3E3E3');
  static final Color colorTextDisable = HexColor('#646464');
  static final Color colorBorder = HexColor('#596481');
  static final Color colorBorder1 = HexColor('#DBE0E6');
  static final Color colorBorder2 = HexColor('#ACACAC');
  static final Color colorBorderDisable = HexColor('#8592AB');
  static final Color colorRed = HexColor('#DD005D');
  static final Color colorPrimary = HexColor('#FFD700');
  static final Color colorSecondary = HexColor('#0062AF');
  static final Color colorText = HexColor('#2E3A5B');
  static final Color colorGrey = HexColor('#596481');
  static final Color colorBlueWhite = HexColor('#D9F2FD');
  static final Color colorRedWhite = HexColor('#FCE6EF');
  static final Color colorYellowWhite = HexColor('#fff4d6');
  static final Color colorRedWhite1 = HexColor('#FDF2F7');
  static final Color colorOrangeWhite = HexColor('#FFF7F2');

  static final Color backgroundSuccessColor = HexColor('#EBFAF1');
  static final Color backgroundErrorColor = HexColor('#FFEDED');
  static final Color backgroundBlueColor = HexColor('#E6F2FF');

  static final double fontSizeText = 16;
  static final double fontSizeTextBig = 18;
  static final double fontSizeSubText = 14;
  static final double fontSize12 = 12;

  static final Border borderBottomLine = Border(
    bottom: BorderSide(
      width: 1,
      color: borderLineColor,
    ),
  );

  static final Border borderTopLine = Border(
    top: BorderSide(
      width: 1,
      color: borderLineColor,
    ),
  );

  static final TextStyle headerTitle = TextStyle(
    // text on header
    fontWeight: FontWeight.w500,
    fontSize: 16,
    color: Colors.white,
  );

  static final MyTextStyle textStyle12 = MyTextStyle(
    fontSize: 12.0,
    color: colorText,
    fontWeight: FontWeight.w400,
  );

  static final MyTextStyle textStyle14 = MyTextStyle(
    fontSize: 14.0,
    color: colorText,
    fontWeight: FontWeight.w400,
  );

  static final MyTextStyle textStyle16 = MyTextStyle(
    fontSize: 16.0,
    color: colorText,
    fontWeight: FontWeight.w400,
  );

  static final MyTextStyle textStyle18 = MyTextStyle(
    fontSize: 18.0,
    color: colorText,
    fontWeight: FontWeight.w400,
  );

  static final MyTextStyle textStyle20 = MyTextStyle(
    fontSize: 20.0,
    color: colorText,
    fontWeight: FontWeight.w400,
  );

  static final TextStyle textStyleLarge = TextStyle(
    fontSize: 20,
    color: colorText,
  );

  //style font chữ bình thường
  static final TextStyle textStyle = TextStyle(
    fontSize: fontSizeText,
    color: colorBorder,
  );

  //style font chữ lỗi validate
  static final TextStyle textErrorValidateStyle = TextStyle(
    fontSize: fontSize12,
    color: colorBorder,
  );

  static final TextStyle textStyleBig = TextStyle(
    fontSize: fontSizeTextBig,
    color: colorBorder,
  );

  static final TextStyle textStyleDark = TextStyle(
    fontSize: fontSizeText,
    color: colorText,
  );

  static final TextStyle textBtnLight = TextStyle(
    fontSize: fontSizeText,
    color: Colors.white,
    fontWeight: FontWeight.w600,
  );

  static final TextStyle textBtnBlue = TextStyle(
    fontSize: fontSizeText,
    color: colorBlue,
    fontWeight: FontWeight.w600,
  );
  static final TextStyle textBtnYellow = TextStyle(
    fontSize: fontSizeText,
    color: colorYellow,
    fontWeight: FontWeight.w600,
  );

  static final TextStyle textBtnBlueSecondary = TextStyle(
    fontSize: fontSizeText,
    color: colorSecondary,
    fontWeight: FontWeight.w600,
  );

  static final TextStyle textBtnRed = TextStyle(
    fontSize: fontSizeText,
    color: colorRed,
    fontWeight: FontWeight.w600,
  );

  static final TextStyle textBtnDanger = TextStyle(
    fontSize: fontSizeText,
    color: colorDanger,
    fontWeight: FontWeight.w600,
  );

  static final TextStyle textActionHeader = TextStyle(
    fontSize: fontSizeText,
    fontWeight: FontWeight.w500,
    color: HexColor('#000000'),
  );

  static final TextStyle borderRadius = TextStyle(
    fontSize: fontSizeText,
    color: colorBorder,
  );

  static final TextStyle textStyleSub = TextStyle(
    fontSize: fontSizeSubText,
    color: colorBorder,
  );

  static final TextStyle textStyleBold = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: fontSizeText,
    color: colorBorder,
  );

  static final TextStyle textStyleRed = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: fontSizeText,
    color: colorRed,
  );

  static final boxShadow = [
    BoxShadow(
      offset: Offset(0.0, -1.0),
      color: Colors.black.withOpacity(.1),
      blurRadius: 8.0,
    ),
  ];

  static final OutlineInputBorder outlineBorder = OutlineInputBorder(
    borderSide: BorderSide(color: borderLineColor),
    borderRadius: BorderRadius.circular(8),
  );

  //style text button, chỉ border radius và padding 0
  static final ButtonStyle textButtonStyle = ButtonStyle(
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
    padding: MaterialStateProperty.all(EdgeInsets.all(0)),
    overlayColor:
        MaterialStateProperty.all<Color>(Color.fromRGBO(255, 255, 255, 0.45)),
  );

  //style button phụ, nền vang nhạt, text vang
  static final ButtonStyle textButtonSecondary = ButtonStyle(
    backgroundColor: MaterialStateProperty.all<Color>(colorYellowWhite),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
    padding: MaterialStateProperty.all(EdgeInsets.all(0)),
    overlayColor:
        MaterialStateProperty.all<Color>(Color.fromRGBO(255, 255, 255, 0.45)),
  );

  //style button chính, nền vang
  static final ButtonStyle textButtonPrimary = ButtonStyle(
    backgroundColor: MaterialStateProperty.all<Color>(colorYellow),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
    padding: MaterialStateProperty.all(EdgeInsets.all(0)),
    overlayColor:
        MaterialStateProperty.all<Color>(Color.fromRGBO(255, 255, 255, 0.45)),
  );
  //style button chính, nền đỏ
  static final ButtonStyle textButtonPrimaryRedPadding10 = ButtonStyle(
    backgroundColor: MaterialStateProperty.all<Color>(colorRed),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
    padding: MaterialStateProperty.all(EdgeInsets.all(10)),
    overlayColor:
        MaterialStateProperty.all<Color>(Color.fromRGBO(255, 255, 255, 0.45)),
  );
  static final ButtonStyle textButtonPrimaryRed = ButtonStyle(
    backgroundColor: MaterialStateProperty.all<Color>(colorRed),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
    padding: MaterialStateProperty.all(EdgeInsets.all(0)),
    overlayColor:
        MaterialStateProperty.all<Color>(Color.fromRGBO(255, 255, 255, 0.45)),
  );
  //style button phụ, nền do nhạt, text do
  static final ButtonStyle textButtonSecondaryRed = ButtonStyle(
    backgroundColor: MaterialStateProperty.all<Color>(colorRedWhite),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
    padding: MaterialStateProperty.all(EdgeInsets.all(0)),
    overlayColor:
        MaterialStateProperty.all<Color>(Color.fromRGBO(255, 255, 255, 0.45)),
  );

  static final ButtonStyle roundOulineButton = ButtonStyle(
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
        side: BorderSide(color: AppTheme.borderLineColor),
      ),
    ),
    padding: MaterialStateProperty.all(
      EdgeInsets.all(15),
    ),
    backgroundColor: MaterialStateProperty.all(Colors.white),
  );

  static final ButtonStyle roundUnactiveButton = ButtonStyle(
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
    ),
    padding: MaterialStateProperty.all(
      EdgeInsets.all(15),
    ),
    backgroundColor: MaterialStateProperty.all(backgroundColor),
  );

  static final ButtonStyle buttonPrimary = ButtonStyle(
    backgroundColor: MaterialStateProperty.all<Color>(colorPrimary),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))),
    padding: MaterialStateProperty.all(EdgeInsets.all(0)),
    overlayColor:
        MaterialStateProperty.all<Color>(Color.fromRGBO(255, 255, 255, 0.45)),
  );

  static final ButtonStyle buttonPrimaryOutline = ButtonStyle(
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
        side: BorderSide(color: AppTheme.colorPrimary),
      ),
    ),
    padding: MaterialStateProperty.all(EdgeInsets.all(0)),
    overlayColor:
        MaterialStateProperty.all<Color>(Color.fromRGBO(255, 255, 255, 0.45)),
  );

  static final ButtonStyle buttonSecondary = ButtonStyle(
    backgroundColor: MaterialStateProperty.all<Color>(colorSecondary),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))),
    padding: MaterialStateProperty.all(EdgeInsets.all(0)),
    overlayColor:
        MaterialStateProperty.all<Color>(Color.fromRGBO(255, 255, 255, 0.45)),
  );

  static final ButtonStyle buttonRed = ButtonStyle(
    backgroundColor: MaterialStateProperty.all<Color>(colorRedWhite),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))),
    padding: MaterialStateProperty.all(EdgeInsets.all(0)),
    overlayColor:
        MaterialStateProperty.all<Color>(Color.fromRGBO(255, 255, 255, 0.45)),
  );

  static final ButtonStyle buttonNoPadding = ButtonStyle(
    padding: MaterialStateProperty.all(EdgeInsets.all(0)),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))),
  );
}
