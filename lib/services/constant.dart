//**Status for api */
class Status {
  static const SUCCESS = 1;
  static const FAIL = 0;
}

class ErrorCode {
  static const TIMEOUT = 1;
  static const SYS_ERROR = 2;
  static const SERVER_MESSAGE = 3;
  static const NO_DATA = 4;
}

//**Storage Key for share preferent */
class StorageKey {
  static const LOGIN_INFO = 'login_info';
  static const LOGIN = 'login';
  static const LOCATION = 'location';
  static const CART_PROTDUCT = 'cartroduct';
  static const ADDRESS = 'address';
  
}

const ITEM_PER_PAGE = 10;
const REQUEST_TIME_OUT = 12;
const NULL_STRING = 'chua_co_thong_tin';

class LoaiHinhKh {
  static const CA_NHAN = 'CN';
  static const DOANH_NGHIEP = 'DN';
}

enum GridType { xs, sm, md, lg, xl }

enum ImageType { base64, file, asset, network, error, pdf }

enum HinhThucChup { chupAnh, chonAnhLib }

enum BoiThuongType { conNguoi, xe, xeMay }

enum LuaChonTienIchMm { xemgcn, ycBoiThuong }

enum ActionButton { add, edit, delete, view, glary, camera }

enum DanhMucAnh { trai, phai, truoc, sau, kinh, guong }

enum ValidateType {
  phone,
  password,
  passwordOld,
  rePassword,
  email,
  id,
  textInfo,
  maSoThue,
  normal
}

const DEFAULT_STRING = 'Chưa cập nhật';
const DATA_ERROR = 'Dữ liệu không hợp lệ!';

class LoginType {
  static const GOOGLE = 'GOOGLE';
  static const APPLE = 'APPLE';
}

class CardType {
  static const CMND = 'CMND';
  static const CCCD = 'CCCD';
  static const CCCD_MOI = 'CCCD_MOI';
  static const HO_CHIEU = 'HO_CHIEU';
}
