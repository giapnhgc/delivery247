# delivery247

A new Flutter project.

## Getting Started
#Author giapnhgc@gmail.com


Phiên bản sử dụng: flutter 2.2.3
### Fix file input_decorator.dart
- Tìm file này tại path như bên dưới trong thư mục flutter: 
```
flutter\packages\flutter\lib\src\material\input_decorator.dart
```
- Zip backup lại file trước khi thực hiện bước tiếp theo. 
- Copy&replace file này từ thư mục plugins đã có sẵn file zip, giải nén và replace vào thư mục trên.
- Nhớ xóa file input_decorator.dart tại mục plugins sau khi copy.

Cấu hình android: Đã cấu hình sẵn trên git, chỉ cần pull về (Android Manifest, build gradle đã cấu hình sẵn)